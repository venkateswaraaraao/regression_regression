package com.afqa.testsuite_RS;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.testng.annotations.Test;

import com.afqa.page.Store_Home_Page;
import com.afqa.page.Store_Login_Page;
import com.afqa.page.Store_Payment_Page;
import com.afqa.resultsets.Constants;

public class Store_005_MinDueAmount_ACH extends Constants{
	
static Logger Log = Logger.getLogger(Store_005_MinDueAmount_ACH.class.getName());
	
	@Test
	public void Store_MinDuePay_ACH() throws Exception
	{
		PropertyConfigurator.configure(logPath);
	
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get(StoreURL());
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	
		WebDriverWait wait=new WebDriverWait(driver,100);
		
		Store_Login_Page loginPage=PageFactory.initElements(driver, Store_Login_Page.class);
		Store_Home_Page homePage=PageFactory.initElements(driver, Store_Home_Page.class);
		Store_Payment_Page payPage=PageFactory.initElements(driver, Store_Payment_Page.class);
		
		loginPage.StoreLogin();
		
		String CxID=dbLOC.getlOCIDForPayment();
		String uEmail=dbCx.getCustomerEmail(CxID);
		
		int emailUpdate=dbCx.updateEmailforPayments(uEmail);
		Log.info(emailUpdate);
		
		homePage.pickCxbyID(driver, CxID, wait);
		
		Sleeper.sleepTightInSeconds(2);
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupDialogMask")));
		
		payPage.clickMoreBtn();
		
		payPage.PaymentBtn(wait);
		
		payPage.clickMinPayDueCash();
		
		payPage.processPaymentForCash(wait);
		
	}
}
