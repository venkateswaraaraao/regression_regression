package com.afqa.testsuite_RS;

import java.awt.AWTException;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.ss.usermodel.Row;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;
import org.testng.annotations.Test;

import com.afqa.page.Online_Registration_Page;
import com.afqa.page.Online_Video_Page;
import com.afqa.page.Yodlee_Page;
import com.afqa.resultsets.Constants;
import com.afqa.resultsets.DBRS_Customer;

public class Online_001_Registartion_Card_EBV extends Constants
{
	static Logger Log = Logger.getLogger(Online_001_Registartion_Card_EBV.class.getName());
	
	/*
	 * Card - Yes; Account type - Checking; EBV
	 */
	
	@Test
	public void OnlineRegEBV_Card() throws IOException, InterruptedException, ClassNotFoundException, SQLException, AWTException, ParseException
	{
		PropertyConfigurator.configure(logPath);
		
		String RunMode=eo.getCellData(Config, 0, 1, 3);
		if(!RunMode.equals("Yes"))
		{
			throw new SkipException("Skipping Online_CR_001 testcase");
		}
		else
		{
			System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
			driver=new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.get(CxRegisterURL());
			
			WebDriverWait wait=new WebDriverWait(driver, 160);
			
			Log.info("Online Registration started");
			
			int sRow=(int) eo.getCellValue(Config, 0, 1, 1);
			int eRow=(int) eo.getCellValue(Config, 0, 1, 2);
			
			Log.info(sRow+","+eRow);
			
			//First Row Index is always starts from 2
			try
			{
				for(int i=sRow;i<eRow;i++)	
				{
					Row r=eo.getRowData(path0,0,i);
		       	
					Online_Registration_Page RegPage =PageFactory.initElements(driver, Online_Registration_Page.class);
					Online_Video_Page VideoPage=PageFactory.initElements(driver, Online_Video_Page.class);
					
					Yodlee_Page yodlee=PageFactory.initElements(driver, Yodlee_Page.class);
					DBRS_Customer dbResCustomer=PageFactory.initElements(driver, DBRS_Customer.class);
					
					//***********************************
					//CREATE YOUR ACCOUNT
					//***********************************
					Log.info("----------------------");
					Log.info("Iteraion No: "+(i-1));
					Log.info("----------------------");
					
					//Enter Email Address
					RegPage.enterEmail();
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
					RegPage.enterConfirmEmail();
					String NewEMail=RegPage.ConfirmEmail.getAttribute("value");
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		
					//Enter Phone Number
					RegPage.enterPhNumber(wait, driver);
					
					//Enter password, DOB, FirstName and LastName
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
					Sleeper.sleepTightInSeconds(2);
					RegPage.enterGenDetails(wait);
					
					//Enter SSN, if already exist enter new SSN
					RegPage.enterSSN();
					
					//Enter Security Details
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
					RegPage.enterSecurityDetails();
					JavascriptExecutor jse=(JavascriptExecutor) driver;
					jse.executeScript("window.scrollBy(0,100)", "");
					
					//Wisconsin State additional fields
					//String State=r.getCell(0).getStringCellValue();
					
					RegPage.FirstPageState(r, wait);
		
					//Check Military and Consent
					Sleeper.sleepTightInSeconds(1);
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
					RegPage.checkMilitary(wait);
					RegPage.checkConsent();
					RegPage.communucationPolicyCheck();
					jse.executeScript("window.scrollBy(0,500)", "");
					
					//Click on Next Button in Registration first page
					RegPage.clickAccCreateNXTBtn();
					
					Log.info("Registration First Page Completed");
					Sleeper.sleepTightInSeconds(6);
					
					//Get Statuses after Account Creation in Registration first page
					String CxID=dbCx.getCustomerID(NewEMail);
					Log.info(CxID);
			
					//Visibility of Registration second page Validation
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("AddressLine1")));
					Log.info("Registration Second page displayed");
					Log.info("***************************************");
					
					//***********************************
					//ENTER PERSONAL DETAILS
					//***********************************
		
					//Enter Address details
					RegPage.NewAddressDetails(r, wait);
					
					//Enter Identity details
					RegPage.IdentityDetails(wait, driver);
					Log.info("Address and Identity details are entered");
					
					//Fill Income details in Registration second page
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
					RegPage.enterIncomeDetails(r,wait,driver);
					Log.info("Income details entered");
					
					//Fill Bank details in Registration second page
					jse.executeScript("window.scrollBy(0,500)", "");
					RegPage.selectCheckingACType();
					
					RegPage.BankDetails(r,wait);
					Log.info("Bank details entered");
					
					//Select how to verify account by instant electronic verification
					RegPage.VerifyAccountYodlee();
					
					RegPage.CardDetails(wait, driver, r);
					Log.info("Card details entered");
					Sleeper.sleepTightInSeconds(2);
					jse.executeScript("window.scrollBy(0,500)", "");
					RegPage.PersonalPageNextBtn();
					
					//***************************************
					//	VERIFY CUSTOMER EMAIL
					//***************************************
					dbCx.IsVerified(CxID);
					Log.info("Email verified successfully");
					Log.info("***************************************");
					
					Sleeper.sleepTightInSeconds(6);
				
					//***********************************
					//	PERFORM YODLEE
					//***********************************
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
					Sleeper.sleepTightInSeconds(5);
					
					boolean EBVPopUp=driver.findElements(By.className("EBVPopUp")).size()>0;
					
					if(EBVPopUp==true)
					{
						wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("EBVPopUp")));
						Sleeper.sleepTightInSeconds(20);
						Log.info("EBV Popup opened");
						yodlee.performYodleeSuccess();
						Sleeper.sleepTightInSeconds(60);
						
						//Wait until Video page display 
						wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\'sticky-wrapper\']/header/div[3]/a[2]/img")));
						Sleeper.sleepTightInSeconds(2);
						Log.info("Video page is displayed");
						
						//***********************************
						//	VIDEO PAGE
						//***********************************
			
						VideoPage.VideoPageCheck(wait);
						Sleeper.sleepTightInSeconds(2);
						
						driver.findElement(By.className("getCashNowNewbtn")).click();
						ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
					    driver.close();
						driver.switchTo().window(tabs2.get(1));
						
						Sleeper.sleepTightInSeconds(10);
					}
					else
					{
						String currentURL=driver.getCurrentUrl();
						if(currentURL.contains("ReasonForNoAA"))
						{
			    			Log.info("NOAA page displayed");
						}
					}
					Sleeper.sleepTightInSeconds(2);
					
					//Get Customer status After Registration validation in DB
					Log.info("****************************");
					Log.info("Customer Table DB Validations After Cx Registration");
					Log.info("****************************");
					
					dbResCustomer.getStatuses(CxID);
					Log.info("Expected: CxStatusCodeID: 20, Applic.Status: 0, Agent.Status: 0, Verif.Status: 30");
					
					//Get CustomerCard status After Registration validation in DB
					Log.info("****************************");
					Log.info("CustomerCard Table DB Validations After Cx Registration");
					Log.info("****************************");
					
					dbResCustomer.getCustomerCardStatus(CxID);
					Log.info("Expected: CustomerCardStatus: 0");
					
					//Get Canarifi status After Registration validation in DB
					Log.info("****************************");
					Log.info("Canarifi Table DB Validations After Cx Registration");
					Log.info("****************************");
					
					dbResCustomer.getCanarifiStatusForEBV(CxID);
					Log.info("Expected: CanarifiStatus: 80, RequestStatus: 2");
				}
			}
			catch(Exception e)
			{
				String ErrorMsg=e.toString();
				Log.info(ErrorMsg);
				
				if(ErrorMsg.contains("Cannot Focus"))
				{
					Log.info("TestCase Failed due to Cannot Focus Element");
				}
				if(ErrorMsg.contains("Timed out"))
				{
					Log.info("TestCase Failed due to Time Out");
				}
				if(ErrorMsg.contains("unknown error"))
				{
					Log.info("TestCase Failed due to unknown error");
				}
				if(ErrorMsg.contains("no such element"))
				{
					Log.info("TestCase Failed due to no such element exception");
				}
			}
		}
		//driver.close();
	}
}












