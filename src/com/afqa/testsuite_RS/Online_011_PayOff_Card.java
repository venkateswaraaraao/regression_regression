package com.afqa.testsuite_RS;

import java.awt.AWTException;
import java.io.IOException;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.afqa.page.Online_CxHome_Menu;
import com.afqa.page.Online_CxMakePayment_Page;
import com.afqa.page.Online_Login_Page;
import com.afqa.resultsets.Constants;

public class Online_011_PayOff_Card extends Constants{
	
	static Logger Log = Logger.getLogger("Online_Payment_AddCard");

	/*
	 * This testcase validates Customer Payment by adding new card
	 */
	
	@Test
	public void Online_PayOffAmt_Card() throws IOException, AWTException, ClassNotFoundException, SQLException
	{
		PropertyConfigurator.configure(logPath);
		
		//Browser launch and navigate to URL
		PropertyConfigurator.configure(logPath);
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");	
		ChromeOptions options = new ChromeOptions();
		options.addArguments("disable-infobars");
		
		driver = new ChromeDriver(options);
		driver.get(CustomerURL());
		driver.manage().window().maximize();
		
		WebDriverWait wait=new WebDriverWait(driver, 45);
		Log.info("Online Customer Payment by adding new card");
		
		Online_Login_Page OnlineLogin=PageFactory.initElements(driver, Online_Login_Page.class);
		Online_CxHome_Menu CxMenu=PageFactory.initElements(driver, Online_CxHome_Menu.class);
		Online_CxMakePayment_Page CxPayment=PageFactory.initElements(driver, Online_CxMakePayment_Page.class);
		
		int CxID=(int) eo.getCellValue(path4, 0, 6, 1);
		Log.info(CxID);
				
		String uEmail=dbCx.getCustEmail(CxID);
		OnlineLogin.onlineLogin(uEmail);
				
		String CardNumber="5200827832667243";
		try
		{
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("OK")));
			Sleeper.sleepTightInSeconds(1);
			driver.findElement(By.linkText("OK")).click();
		}
		catch(Exception ex){}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("MakePayment")));
		CxMenu.MakePayment();
		
		CxPayment.clickPayOff();
		CxPayment.paymentByAddCard(wait, CardNumber);
	}
}
