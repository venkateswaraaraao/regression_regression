package com.afqa.testsuite_RS;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.afqa.page.Online_CxHome_Menu;
import com.afqa.page.Online_CxReqCashAdvance_Page;
import com.afqa.page.Online_Login_Page;
import com.afqa.resultsets.Constants;

public class Online_005_LOC_WithDrawal extends Constants
{
	static Logger Log = Logger.getLogger("OnlineWithDrawal");
	@Test
	public void OnlineAdvance() throws Exception
	{
		PropertyConfigurator.configure(logPath);
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(CustomerURL());
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebDriverWait wait=new WebDriverWait(driver,60);
		
		Online_Login_Page OnlineLogin=PageFactory.initElements(driver, Online_Login_Page.class);
		Online_CxHome_Menu CxMenu=PageFactory.initElements(driver, Online_CxHome_Menu.class);
		Online_CxReqCashAdvance_Page CxWithDrawal=PageFactory.initElements(driver, Online_CxReqCashAdvance_Page.class);
		
		String CxID=dbLOC.getCxIDForWithDrawal();
		Log.info(CxID);
		
		String uEmail=dbCx.getCustomerEmail(CxID);
		
		OnlineLogin.onlineLogin(uEmail);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("requestAdvanceBtn")));
		Sleeper.sleepTightInSeconds(2);
		CxMenu.RequestCashAdvance();
		
		CxWithDrawal.RequestAdvance(wait);
		
		//CxWithDrawal.OneToTwoBusinessFund(wait);
		//CxWithDrawal.WithdrawalSubmit(wait);
		
		CxWithDrawal.getMyCash(wait);
		Sleeper.sleepTightInSeconds(5);
		String SecurityCode=otpVerification.SecurityCode(CxID);
		CxWithDrawal.OTPVerification(SecurityCode,wait);
		CxWithDrawal.CxLogout();
		
	}
}
