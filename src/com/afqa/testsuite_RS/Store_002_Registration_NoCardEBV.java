package com.afqa.testsuite_RS;

import java.awt.AWTException;
import java.io.IOException;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.afqa.page.Store_CxRegister_Page;
import com.afqa.page.Store_CxSummary_Page;
import com.afqa.page.Store_Home_Page;
import com.afqa.page.Store_Login_Page;
import com.afqa.page.Yodlee_Page;
import com.afqa.resultsets.Constants;
import com.afqa.resultsets.DBRS_Customer;

public class Store_002_Registration_NoCardEBV extends Constants{
	
static Logger Log = Logger.getLogger(Store_001_Registration_Card_EBV.class.getName());
	
	/*
	 *  Store Registration With NoCard -- EBV
	 */
	
	@Test 
	public void StoreCxRegEBV_NoCard() throws IOException, ClassNotFoundException, SQLException, AWTException
	{
		PropertyConfigurator.configure(logPath);
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("disable-infobars");
		driver=new ChromeDriver(options);
		driver.get(StoreURL());
		driver.manage().window().maximize();
		
		WebDriverWait wait=new WebDriverWait(driver, 160);
		
		Store_Login_Page StoreLoginP=PageFactory.initElements(driver, Store_Login_Page.class);
		Store_CxRegister_Page StoreRegPage=PageFactory.initElements(driver, Store_CxRegister_Page.class);
		Store_Home_Page storeHome=PageFactory.initElements(driver, Store_Home_Page.class);
		Store_CxSummary_Page StoreSummary=PageFactory.initElements(driver, Store_CxSummary_Page.class);
		Yodlee_Page yodlee=PageFactory.initElements(driver, Yodlee_Page.class);
		
		DBRS_Customer dbResCustomer=PageFactory.initElements(driver, DBRS_Customer.class);
		
		StoreLoginP.StoreLogin();
		
		try
		{
			for (int i=9;i<10;i++)
			{	
				Row r=eo.getRowData(path0, 2, i);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='MainHdr_Registratoin']/img")));
				storeHome.OpenCxRegister(wait);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("PersonalDetails_SSN")));
				
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
				Sleeper.sleepTightInSeconds(2);
				StoreRegPage.enterValidPhone();
				String CxPhone=StoreRegPage.stPhone();
				Log.info("PhoneNumber: "+CxPhone);
				
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
				StoreRegPage.enterName(driver);
				
				StoreRegPage.enterValidSSN(wait);
				
				StoreRegPage.enterValidPhotoID(wait);
				
				StoreRegPage.enterAddress(wait);
				
				StoreRegPage.enterValidEmail(wait,r);
				Sleeper.sleepTightInSeconds(2);
				
				String Email=StoreRegPage.CxEmail.getAttribute("value");
				Log.info(Email);
				
				StoreRegPage.selectReceiveMail(wait);
				
				Sleeper.sleepTightInSeconds(2);
				StoreRegPage.enterDOB(wait);
				
				StoreRegPage.BankDetails(r, driver, wait);
				
				StoreRegPage.IncomeDetails(r, wait);
				StoreRegPage.FrequencyDetails(r, wait);
				
				StoreRegPage.RefDetailsToEnd();
				Sleeper.sleepTightInSeconds(5);
				
				StoreRegPage.VerifyNow(wait);
				Sleeper.sleepTightInSeconds(20);
				
				yodlee.performYodleeSuccess();
				Sleeper.sleepTightInSeconds(15);
				
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("maskPopup")));
				Sleeper.sleepTightInSeconds(2);
				StoreRegPage.ReadyToApprove();
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("OK")));
				Sleeper.sleepTightInSeconds(3);
				StoreRegPage.RegisterConfirmSuccess();
				Sleeper.sleepTightInSeconds(5);
				
				//Navigation Check After Customer Successful Registration
				Log.info("-----------------------------------------------------------------------");
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='MainHdr_Registratoin']/img")));
				StoreSummary.CheckCxSumamry(wait);
				Log.info("-----------------------------------------------------------------------");
				
				String CxID=dbCx.getCustomerID(Email);
				Log.info("CustomerID: "+CxID);
				
				Sleeper.sleepTightInSeconds(2);
				
				String CustomerStatus=dbCx.getCustomerStatusID(CxID);
				Log.info("Customer Status is "+CustomerStatus);
				
				Sleeper.sleepTightInSeconds(2);
				
				//Get Customer status After Registration validation in DB
				Log.info("****************************");
				Log.info("Customer Table DB Validations After Cx Registration");
				Log.info("****************************");
				
				dbResCustomer.getStatuses(CxID);
				Log.info("Expected: CxStatusCodeID: 20, Applic.Status: 0, Agent.Status: 0, Verif.Status: 30");
				
				//Get Canarifi status After Registration validation in DB
				Log.info("****************************");
				Log.info("Canarifi Table DB Validations After Cx Registration");
				Log.info("****************************");
				
				dbResCustomer.getCanarifiStatusForEBV(CxID);
				Log.info("Expected: CanarifiStatus: 80, RequestStatus: 2");
			}
		}
		catch(Exception e)
		{
			String ErrorMsg=e.toString();
			Log.info(ErrorMsg);
			
			if(ErrorMsg.contains("Cannot Focus"))
			{
				Log.info("TestCase Failed due to Cannot Focus Element");
			}
			if(ErrorMsg.contains("Timed out"))
			{
				Log.info("TestCase Failed due to Time Out");
			}
			if(ErrorMsg.contains("unknown error"))
			{
				Log.info("TestCase Failed due to unknown error");
			}
			if(ErrorMsg.contains("no such element"))
			{
				Log.info("TestCase Failed due to no such element exception");
			}
		}
	}
}
