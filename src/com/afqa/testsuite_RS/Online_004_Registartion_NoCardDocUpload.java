package com.afqa.testsuite_RS;

import java.awt.AWTException;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.ss.usermodel.Row;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;
import org.testng.annotations.Test;

import com.afqa.page.Online_DocUpload_Page;
import com.afqa.page.Online_Registration_Page;
import com.afqa.page.Online_Video_Page;
import com.afqa.resultsets.Constants;
import com.afqa.resultsets.DBRS_Customer;

public class Online_004_Registartion_NoCardDocUpload extends Constants
{
	static Logger Log = Logger.getLogger(Online_004_Registartion_NoCardDocUpload.class.getName());
	
	/*
	 * Card - No; Account type - Checking; Documents Upload 
	 */
	
	@Test
	public void OnlineRegDocUpload_NoCard() throws IOException, InterruptedException, ClassNotFoundException, SQLException, AWTException, ParseException
	{
		PropertyConfigurator.configure(logPath);
		
		String RunMode=eo.getCellData(Config, 0, 1, 3);
		if(!RunMode.equals("Yes"))
		{
			throw new SkipException("Skipping Online_CR_006 testcase");
		}
		else
		{
			System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
			driver=new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.get(CxRegisterURL());
			
			WebDriverWait wait=new WebDriverWait(driver, 120);
			
			Log.info("Online Registration started");
			
			int sRow=(int) eo.getCellValue(Config, 0, 6, 1);
			int eRow=(int) eo.getCellValue(Config, 0, 6, 2);
			
			Log.info(sRow+","+eRow);
			try
			{
				//First Row Index is always starts from 2
				 for(int i=sRow;i<eRow;i++)	
			     {
			       	Row r=eo.getRowData(path0,0,i);
					
					Online_Registration_Page RegPage =PageFactory.initElements(driver, Online_Registration_Page.class);
					Online_Video_Page VideoPage=PageFactory.initElements(driver, Online_Video_Page.class);
					Online_DocUpload_Page DocUpload=PageFactory.initElements(driver, Online_DocUpload_Page.class);
					DBRS_Customer dbResCustomer=PageFactory.initElements(driver, DBRS_Customer.class);
	
					//***********************************
					//CREATE YOUR ACCOUNT
					//***********************************
		
					Log.info("----------------------");
					Log.info("Iteraion No: "+(i-1));
					Log.info("----------------------");
		
					//Enter Email Address
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Email")));
					RegPage.enterEmail();
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
					RegPage.enterConfirmEmail();
					String NewEMail=RegPage.ConfirmEmail.getAttribute("value");
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		
					//Enter Phone Number
					RegPage.enterPhNumber(wait, driver);
					
					//Enter password, DOB, FirstName and LastName
					RegPage.enterGenDetails(wait);
					
					//Enter SSN, if already exist enter new SSN
					RegPage.enterSSN();
					
					//Enter Security Details
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
					RegPage.enterSecurityDetails();
					JavascriptExecutor jse=(JavascriptExecutor) driver;
					jse.executeScript("window.scrollBy(0,100)", "");
					
					//Wisconsin State additional fields
					RegPage.FirstPageState(r, wait);
		
					//Check Military and Consent
					Sleeper.sleepTightInSeconds(1);
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
					RegPage.checkMilitary(wait);
					RegPage.checkConsent();
					RegPage.communucationPolicyCheck();
					jse.executeScript("window.scrollBy(0,500)", "");
					
					//Click on Next Button in Registration first page
					RegPage.clickAccCreateNXTBtn();
					
					Log.info("Registration First Page Completed");
					Sleeper.sleepTightInSeconds(6);
					
					String CxID=dbCx.getCustomerID(NewEMail);
		
					//Visibility of Registration second page Validation
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("AddressLine1")));
					Log.info("Registration Second page displayed");
					
					//***********************************
					//ENTER PERSONAL DETAILS
					//***********************************
		
					//Enter Address details
					RegPage.NewAddressDetails(r, wait);
					
					//Enter Identity details
					RegPage.IdentityDetails(wait, driver);
					Log.info("Address and Identity details are entered");
								
					//Fill Income details in Registration second page
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
					RegPage.enterIncomeDetails(r,wait,driver);
					Log.info("Income details entered");
					
					//Fill Bank details in Registration second page
					jse.executeScript("window.scrollBy(0,500)", "");
					RegPage.selectCheckingACType();
					
					RegPage.BankDetails(r, wait);
					Log.info("Bank details entered");
					
					//Select how to verify account by i will manually uplaod my documents
					RegPage.VerifyAccountUploadDoc();
					
					jse.executeScript("window.scrollBy(0,400)", "");
					RegPage.PersonalPageNextBtn();
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("popupDialogHdr")));
					Sleeper.sleepTightInSeconds(2);
	
					String NoCardBtn=RegPage.ProceedNoCardBtn.getText();
	
					if(NoCardBtn.contains("Debit Card"))
					{
						Log.info(NoCardBtn);
						RegPage.ProceedNoCardBtn.click();
						Log.info("Card details skipped");
					}
					//***************************************
					//	VERIFY CUSTOMER EMAIL
					//***************************************
					dbCx.IsVerified(CxID);
					
					Log.info("-----------------------------------------------------------------------");
					Log.info("EmailID set to IsVerified=1 in Database : Email verified successfully");
					Log.info("-----------------------------------------------------------------------");
					
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("popupDialogHdr")));
					Sleeper.sleepTightInSeconds(5);
					
					RegPage.DocsUploadPopUp();
					Sleeper.sleepTightInSeconds(3);
					
					//***********************************
					//	DOCUMENT DETAILS
					//***********************************
					
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
					Sleeper.sleepTightInSeconds(15);
					
					boolean showAppAmouontBtn=driver.findElements(By.className("ebvInstantButton")).size()>0;
					
					if(showAppAmouontBtn==true)
					{
						Sleeper.sleepTightInSeconds(2);
						wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Mdl_Documents_0__Location")));
						Sleeper.sleepTightInSeconds(2);
						
						DocUpload.docUpload(r,driver);
						
						Sleeper.sleepTightInSeconds(2);
						DocUpload.docFinish(driver);
					
						//***********************************
						//	UPLOAD DOCUMENT VALIDATION
						//***********************************
						//Wait until Video page display 
						
						wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='sticky-wrapper']/header/div[3]/a[2]/img")));
						Sleeper.sleepTightInSeconds(2);
						
						Log.info("Video page is displayed");
						Log.info("-----------------------------------------------------------------------");
						
						//***********************************
						//	VIDEO PAGE
						//***********************************
						VideoPage.VideoPageCheck(wait);
						Sleeper.sleepTightInSeconds(2);
						driver.findElement(By.className("getCashNowNewbtn")).click();
						ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
					    driver.close();
						driver.switchTo().window(tabs2.get(1));
						
						Sleeper.sleepTightInSeconds(10);
					}
					else
					{
						String currentURL=driver.getCurrentUrl();
						if(currentURL.contains("ReasonForNoAA"))
						{
			    			Log.info("NOAA page displayed");
						}
					}
					
					String CustomerStatus=dbCx.getCustomerStatusID(CxID);
					Log.info("Customer Status is "+CustomerStatus);
					
					//Get Customer status After Registration validation in DB
					Log.info("****************************");
					Log.info("Customer Table DB Validations After Cx Registration");
					Log.info("****************************");
					
					dbResCustomer.getStatuses(CxID);
					Log.info("Expected: CxStatusCodeID: 20, Applic.Status: 0, Agent.Status: 0, Verif.Status: 30");
					
					//***********************************
					//	UPLOAD DOCUMENT VALIDATION
					//***********************************
					try
					{
						List<String> docPath=(List<String>) dbCxDocs.getDocPath(CxID);
						List<String> docType=(List<String>) dbCxDocs.getDocTypeID(CxID);
						int noOfPaths=docPath.size();
						Log.info("Documents Upload Validation");
						Log.info("-----------------------------------------------------------------------");
						for(int l=0;l<noOfPaths;l++)
						{
							String PathName=docPath.get(l);
							String Doctype=docType.get(l);
							Log.info("DocumentTypeID: "+Doctype+", and DocFilePath: "+PathName);
						}
						Log.info("-----------------------------------------------------------------------");
					}
					catch(Exception e)
					{
						Log.info("No documents saved in the database");
						Log.info("-----------------------------------------------------------------------");
					}
							
					int LtDocument=dbCxDocs.getLTDocumentTypeID(CxID);
					Log.info("LTDocumentTypeID is : "+LtDocument);
				}
			}
			catch(Exception e)
			{
				String ErrorMsg=e.toString();
				Log.info(ErrorMsg);
				
				if(ErrorMsg.contains("Cannot Focus"))
				{
					Log.info("TestCase Failed due to Cannot Focus Element");
				}
				if(ErrorMsg.contains("unknown error"))
				{
					Log.info("TestCase Failed due to unknown error");
				}
				if(ErrorMsg.contains("no such element"))
				{
					Log.info("TestCase Failed due to no such element exception");
				}
			}
		}
	}
}
