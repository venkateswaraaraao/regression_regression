package com.afqa.testsuite_RS;

import java.awt.AWTException;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.afqa.page.Admin_Login_Page;
import com.afqa.page.Admin_Verification_Page;
import com.afqa.resultsets.Constants;

import bsh.ParseException;

public class Admin_001_Verify extends Constants{
	
static Logger Log = Logger.getLogger(Admin_001_Verify.class.getName());
	
	@Test
	public void AdminVerification() throws IOException, InterruptedException, ClassNotFoundException, SQLException, AWTException, ParseException
	{
		PropertyConfigurator.configure(logPath);
		
		/*DesiredCapabilities capability = DesiredCapabilities.chrome();
		capability.setBrowserName("chrome");
		capability.setPlatform(Platform.LINUX);
		RemoteWebDriver driver = new RemoteWebDriver(new URL(nodeURL), capability);*/
		
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(AdminURL());
		
		WebDriverWait wait=new WebDriverWait(driver, 140);
		
		Admin_Login_Page AdminLPage=PageFactory.initElements(driver, Admin_Login_Page.class);
		Admin_Verification_Page AdminVerifyPage=PageFactory.initElements(driver, Admin_Verification_Page.class);
		try
		{
		
			String CxStatusCodeID="160";
			
			String CxID=dbCx.getCx4Verification(CxStatusCodeID);
			Log.info("CustomerID is: "+CxID);
			
			String OutletID=dbCx.getOutletID(CxID);
			Log.info("OutletId is:"+OutletID);
			
			Sleeper.sleepTightInSeconds(2);
	
			String EmailID=dbCx.getCustomerEmail(CxID);
			Log.info(EmailID);
			
			//***************************************
			//	VERIFY CUSTOMER EMAIL
			//***************************************
			dbCx.IsVerified(CxID);
			
			Log.info("Email verified successfully");
			Log.info("***************************************");
			
			Sleeper.sleepTightInSeconds(1);
			
			String CxRiskRate=dbCxRiskLog.getRiskRating(CxID);
			Log.info("Customer Risk Rating is "+CxRiskRate);
			
			driver.get(AdminURL());
	        Sleeper.sleepTightInSeconds(2);
	        
	        AdminLPage.AdminLogin();
	        Sleeper.sleepTightInSeconds(3);
	        
	        AdminVerifyPage.pickNupdateCustomer(CxID, wait,CxRiskRate,driver);
	        
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("VerficationDetails_StatusID")));
	        Sleeper.sleepTightInSeconds(10);
	        
	        AdminVerifyPage.UpdateVerificationStatusAccept(driver);
	        
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("popupDialogCont")));
			AdminVerifyPage.readANDclosePopup();
			Sleeper.sleepTightInSeconds(5);
			
			String CxStatusID=dbCx.getCustomerStatusID(CxID);
			String VerifyStatus=dbCx.getVerificationStatus(CxID);
			Log.info("Actuals: CxStatusCodeID: "+CxStatusID+", Verification.Status: "+VerifyStatus);
			Log.info("Expected: CxStatusCodeID: 10,  Verif.Status: 40");
		}
		catch(Exception e)
		{
			String ErrorMsg=e.toString();
			Log.info(ErrorMsg);
			
			if(ErrorMsg.contains("Cannot Focus"))
			{
				Log.info("TestCase Failed due to Cannot Focus Element");
			}
			else if(ErrorMsg.contains("Time Out"))
			{
				Log.info("TestCase Failed due to Time Out");
			}
			else if(ErrorMsg.contains("no such element"))
			{
				Log.info("TestCase Failed due to No Such Element Exception");
			}
		}
    }
}
