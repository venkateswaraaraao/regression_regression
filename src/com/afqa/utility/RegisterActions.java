package com.afqa.utility;

public class RegisterActions {

	public String ReadState(String stateCode)
	{
		String State=null;
		if(stateCode.equals("74"))
		{
			State="Tennessee";
		}
		if(stateCode.equals("81"))
		{
			State="Missouri";
		}
		if(stateCode.equals("85"))
		{
			State="Utah";
		}
		if(stateCode.equals("86"))
		{
			State="Kansas";
		}
		if(stateCode.equals("87"))
		{
			State="Idaho";
		}
		if(stateCode.equals("90"))
		{
			State="Alabama";
		}
		if(stateCode.equals("93"))
		{
			State="Virginia";
		}
		if(stateCode.equals("105"))
		{
			State="California";
		}
		if(stateCode.equals("102"))
		{
			State="North Dakota";
		}
		if(stateCode.equals("101"))
		{
			State="Delaware";
		}
		if(stateCode.equals("125"))
		{
			State="Wisconsin";
		}
		System.out.println(State);
		return State;	
	}

	public void selectState()
	{
		System.out.println("Enter 74 for TN");
		System.out.println("Enter 81 for MS");
		System.out.println("Enter 85 for UT");
		System.out.println("Enter 86 for KS");
		System.out.println("Enter 87 for ID");
		System.out.println("Enter 90 for AL");
		System.out.println("Enter 93 for VA");
		System.out.println("Enter 105 for CA");
		System.out.println("Enter 102 for ND");
		System.out.println("Enter 101 for DE");
		System.out.println("Enter 125 for WI");
	}
	
	public int selectSheet(String ouletID)
	{
		int sheet=0;
		switch(ouletID) 
		{
			case "74" : sheet=0; break;
			case "81" : sheet=1; break;
			case "85" : sheet=2; break;
			case "86" : sheet=3; break;
			case "87" : sheet=4; break;
			case "90" : sheet=5; break;
			case "93" : sheet=6; break;
			case "105" : sheet=7; break;
			case "102" : sheet=8; break;
			case "101" : sheet=9; break;
			case "125" : sheet=10; break;
		}
		return sheet;
	}
	
	public String UpdateCxStatus(String CxStatus)
	{
		String CustStatus=null;
		if(CxStatus.equals("1"))
		{
			CustStatus="Accepted";
		}
		if(CxStatus.equals("2"))
		{
			CustStatus="Not Accepted";
		}
		if(CxStatus.equals("3"))
		{
			CustStatus="Request To Supervisor";
		}
		if(CxStatus.equals("4"))
		{
			CustStatus="Need More Info";
		}
		System.out.println(CustStatus);
		return CustStatus;
	}
	
	public void selectStatus()
	{
		System.out.println("Enter 1 for Accept");
		System.out.println("Enter 2 for Not Accept");
		System.out.println("Enter 3 for Req To Supervisor");
		System.out.println("Enter 4 for Need More Info");
	}
}
