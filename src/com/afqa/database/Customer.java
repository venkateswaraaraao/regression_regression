package com.afqa.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

public class Customer extends DBConnection{

	static Logger Log = Logger.getLogger(Customer.class.getName());

	public String getCustomerID(String NewEMail) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxID="Select CustomerID from customer where Email='"+NewEMail+"'";
		int iCustID = 0;
		ResultSet rs=stmt.executeQuery(Query_GetCxID);
		while(rs.next())
		{
			iCustID=rs.getInt("CustomerID");
		}
		String CustID=String.valueOf(iCustID);

		closeDB(rs, stmt, con);
		
		return CustID;
	}
	
	public String getOutletID(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetOutletID="Select OutletID from customer where CustomerID='"+CxID+"'";
		int iOutletID = 0;
		ResultSet rs=stmt.executeQuery(Query_GetOutletID);
		
		while(rs.next())
		{
			iOutletID=rs.getInt("OutletID");
		}
		String OutletID=String.valueOf(iOutletID);

		closeDB(rs, stmt, con);
		
		return OutletID;
	}
	
	public String getCompanyID(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCompanyID="Select CompanyID from customer where CustomerID='"+CxID+"'";
		int iCompanyID = 0;
		ResultSet rs=stmt.executeQuery(Query_GetCompanyID);
		
		while(rs.next())
		{
			iCompanyID=rs.getInt("CompanyID");
		}
		String CompanyID=String.valueOf(iCompanyID);

		closeDB(rs, stmt, con);
		
		return CompanyID;
	}
	
	public String getCustomerIDByName(String FName) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxStatus="Select top 1 CustomerID from customer where FirstName='"+FName+"' order by InsertedDate desc";
		int iCustID = 0;
		ResultSet rs=stmt.executeQuery(Query_GetCxStatus);
		while(rs.next())
		{
			iCustID=rs.getInt("CustomerID");
		}
		String CustID=String.valueOf(iCustID);

		closeDB(rs, stmt, con);
		
		return CustID;
	}
	
	public String getCustomerSSN(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxSSN="Select SSN from customer where CustomerID='"+CxID+"'";
		int iCustSSN = 0;
		ResultSet rs=stmt.executeQuery(Query_GetCxSSN);
		
		while(rs.next())
		{
			iCustSSN=rs.getInt("SSN");
		}
		String CustSSN=String.valueOf(iCustSSN);

		closeDB(rs, stmt, con);
		
		return CustSSN;
	}
	
	public String getCustomerPhNum(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxphNum="Select SSN from customer where CustomerID='"+CxID+"'";
		int iCustPH = 0;
		ResultSet rs=stmt.executeQuery(Query_GetCxphNum);
		
		while(rs.next())
		{
			iCustPH=rs.getInt("SSN");
		}
		String CustPhNum=String.valueOf(iCustPH);

		closeDB(rs, stmt, con);
		
		return CustPhNum;
	}
	
	public String getCustomerEmail(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxEmail="Select Email from customer where CustomerID='"+CxID+"'";
		String CustEmail = null;
		ResultSet rs=stmt.executeQuery(Query_GetCxEmail);
		
		while(rs.next())
		{
			CustEmail=rs.getString("Email");
		}

		closeDB(rs, stmt, con);
		return CustEmail;
	}
	
	public String getFirstName(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxFName="Select FirstName from customer where CustomerID='"+CxID+"'";
		String CustFName = null;
		ResultSet rs=stmt.executeQuery(Query_GetCxFName);
		
		while(rs.next())
		{
			CustFName=rs.getString("FirstName");
		}

		closeDB(rs, stmt, con);
		return CustFName;
	}

	public String getLastName(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxLName="Select LastName from customer where CustomerID='"+CxID+"'";
		String CustLName = null;
		ResultSet rs=stmt.executeQuery(Query_GetCxLName);
		
		while(rs.next())
		{
			CustLName=rs.getString("LastName");
		}

		closeDB(rs, stmt, con);
		return CustLName;
	}

	public String getCustomerDOB(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxDOB="Select DOB from customer where CustomerID='"+CxID+"'";
		Date iCustDOB = null;
		ResultSet rs=stmt.executeQuery(Query_GetCxDOB);
		
		while(rs.next())
		{
			iCustDOB=rs.getDate("DOB");
		}
		DateFormat dateFormat = new SimpleDateFormat("MMddyyyy");
		String jCustDOB=dateFormat.format(iCustDOB);

		closeDB(rs, stmt, con);
		
		return jCustDOB;
	}
	
	public String getCustomerStatusID(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxStatus="Select CustomerStatusCodeID from customer where CustomerID='"+CxID+"'";
		int iCustStatusID = 0;
		ResultSet rs=stmt.executeQuery(Query_GetCxStatus);
		
		while(rs.next())
		{
			iCustStatusID=rs.getInt("CustomerStatusCodeID");
		}
		String CustStatusID=String.valueOf(iCustStatusID);

		closeDB(rs, stmt, con);
		
		return CustStatusID;
	}
	
	public String getStatus(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetStatus="Select Status from customer where CustomerID="+CxID;
		int iStatus=0;
		ResultSet rs=stmt.executeQuery(Query_GetStatus);
		while(rs.next())
		{
			iStatus=rs.getInt("Status");
		}
		String Status=String.valueOf(iStatus);

		closeDB(rs, stmt, con);
		
		return Status;
	}
	
	public String getSupervisorReqStatus(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetSprVsrReqStatus="Select SuperVisorRequestStatus from customer where CustomerID="+CxID;
		int iSprVsrReqStatus=0;
		ResultSet rs=stmt.executeQuery(Query_GetSprVsrReqStatus);
		while(rs.next())
		{
			iSprVsrReqStatus=rs.getInt("SuperVisorRequestStatus");
		}
		String SprVsrReqStatus=String.valueOf(iSprVsrReqStatus);

		closeDB(rs, stmt, con);
		
		return SprVsrReqStatus;
	}
	
	public String getApplicationStatus(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetAppStatus="Select ApplicationStatus from customer where CustomerID="+CxID;
		int iAppStatus=0;
		ResultSet rs=stmt.executeQuery(Query_GetAppStatus);
		while(rs.next())
		{
			iAppStatus=rs.getInt("ApplicationStatus");
		}
		String AppStatus=String.valueOf(iAppStatus);

		closeDB(rs, stmt, con);
		
		return AppStatus;
	}
	
	public String getAgentStatus(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetAgentStatus="Select AgentStatus from customer where CustomerID="+CxID;
		int iAgentStatus=0;
		ResultSet rs=stmt.executeQuery(Query_GetAgentStatus);
		while(rs.next())
		{
			iAgentStatus=rs.getInt("AgentStatus");
		}
		String AgentStatus=String.valueOf(iAgentStatus);

		closeDB(rs, stmt, con);
		
		return AgentStatus;
	}
	
	public String getVerificationStatus(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxVerifyStatus="Select VerificationStatus from customer where CustomerID="+CxID;
		int iVerifyStatus=0;
		ResultSet rs=stmt.executeQuery(Query_GetCxVerifyStatus);
		while(rs.next())
		{
			iVerifyStatus=rs.getInt("VerificationStatus");
		}
		String VerifyStatus=String.valueOf(iVerifyStatus);

		closeDB(rs, stmt, con);
		
		return VerifyStatus;
	}
	
	public String getLexisNexisStatus(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxLNStatus="Select ApplicationStatus from customer where CustomerID="+CxID;
		String LNStatus="";
		ResultSet rs=stmt.executeQuery(Query_GetCxLNStatus);
		while(rs.next())
		{
			LNStatus=rs.getString("ApplicationStatus");
		}

		closeDB(rs, stmt, con);
		
		return LNStatus;
	}
	
	public String getLoginID(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxStatus="Select ApplicationStatus from customer where CustomerID="+CxID;
		String LNStatus="";
		ResultSet rs=stmt.executeQuery(Query_GetCxStatus);
		while(rs.next())
		{
			LNStatus=rs.getString("ApplicationStatus");
		}

		closeDB(rs, stmt, con);
		
		return LNStatus;
	}
	
	public int IsVerified(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_UpdateIsVerified="update customer set IsVerified=1 where CustomerID="+CxID;
		int rs=stmt.executeUpdate(Query_UpdateIsVerified);
		
		stmt.close();
		con.close();
		
		return rs;
	}
	
	public String getIsDirectMailer(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetIsDirectMailer="select IsDirectMailer from customer where CustomerID="+CxID;
		int iIsDirectMailer=0;
		ResultSet rs=stmt.executeQuery(Query_GetIsDirectMailer);
		while(rs.next())
		{
			iIsDirectMailer=rs.getInt("IsDirectMailer");
		}

		String IsDirectMailer=String.valueOf(iIsDirectMailer);

		closeDB(rs, stmt, con);
		
		return IsDirectMailer;
	}
	
	public String getCustomerIDforBankRupt(String NewEMail) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxID="Select CustomerID from customer where Email='"+NewEMail+"'";
		int iCustID = 0;
		ResultSet rs=stmt.executeQuery(Query_GetCxID);
		while(rs.next())
		{
			iCustID=rs.getInt("CustomerID");
		}
		String CustID=String.valueOf(iCustID);

		closeDB(rs, stmt, con);
		
		return CustID;
	}
	public String getCustEmail(int CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxEmail="Select Email from customer where CustomerID='"+CxID+"'";
		String CustEmail = null;
		ResultSet rs=stmt.executeQuery(Query_GetCxEmail);
		
		while(rs.next())
		{
			CustEmail=rs.getString("Email");
		}

		closeDB(rs, stmt, con);
		return CustEmail;
	}
	
	public int updateEmail() throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
	
		String uEmail=pageCon.PartName()+"_"+pageCon.randNum()+"@"+pageCon.todayDate()+".com";
		Log.info(uEmail);
		
		String Query_UpdateCustomerEMail="update customer set Email='"+uEmail+"'where Email='auto.emailsfinext@gmail.com'";
		int rs=stmt.executeUpdate(Query_UpdateCustomerEMail);
		Log.info("Email has been updated in Customer table");

		String Query_UpdateCustomerLoginEMail="update customerLogin set LoginID='"+uEmail+"' where LoginID='auto.emailsfinext@gmail.com'";
		stmt.executeUpdate(Query_UpdateCustomerLoginEMail);
		Log.info("Email has been updated in CustomerLogin table");
		
		stmt.close();
		con.close();
		
		return rs;
	}
	
	public int updateEmailForSendToStore() throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
	
		String uEmail=pageCon.PartName()+"_"+pageCon.randNum()+"@"+pageCon.todayDate()+".com";
		Log.info(uEmail);
		
		String Query_UpdateCustomerEMail="update customer set Email='"+uEmail+"'where Email='finextqa.auto@gmail.com'";
		int rs=stmt.executeUpdate(Query_UpdateCustomerEMail);
		Log.info("Email has been updated in Customer table");

		String Query_UpdateCustomerLoginEMail="update customerLogin set LoginID='"+uEmail+"' where LoginID='finextqa.auto@gmail.com'";
		stmt.executeUpdate(Query_UpdateCustomerLoginEMail);
		Log.info("Email has been updated in CustomerLogin table");
		
		stmt.close();
		con.close();
		
		return rs;
	}
	
	public int updateWithDrawalEmail() throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
	
		String uEmail=pageCon.PartName()+"_"+pageCon.randNum()+"@"+pageCon.todayDate()+".com";
		Log.info(uEmail);
		
		String Query_UpdateCustomerEMail="update customer set Email='"+uEmail+"'where Email='automationfinext@gmail.com'";
		int rs=stmt.executeUpdate(Query_UpdateCustomerEMail);
		Log.info("Email has been updated in Customer table");

		String Query_UpdateCustomerLoginEMail="update customerLogin set LoginID='"+uEmail+"' where LoginID='automationfinext@gmail.com'";
		stmt.executeUpdate(Query_UpdateCustomerLoginEMail);
		Log.info("Email has been updated in CustomerLogin table");
		
		stmt.close();
		con.close();
		
		return rs;
	}
	
	public int updateDeclineEmail() throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
	
		String uEmail=pageCon.PartName()+"_"+pageCon.randNum()+"@"+pageCon.todayDate()+".com";
		Log.info(uEmail);
		
		String Query_UpdateCustomerEMail="update customer set Email='"+uEmail+"'where Email='automationfinext@gmail.com'";
		int rs=stmt.executeUpdate(Query_UpdateCustomerEMail);
		Log.info("Email has been updated in Customer table");

		String Query_UpdateCustomerLoginEMail="update customerLogin set LoginID='"+uEmail+"' where LoginID='automationfinext@gmail.com'";
		stmt.executeUpdate(Query_UpdateCustomerLoginEMail);
		Log.info("Email has been updated in CustomerLogin table");
		
		stmt.close();
		con.close();
		
		return rs;
	}

	public int updateEmailforPayments(String uEmail) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
			
		String Query_UpdateCustomerEMail="update customer set Email='automationfinext@gmail.com' where Email='"+uEmail+"'";
		int rs=stmt.executeUpdate(Query_UpdateCustomerEMail);
		Log.info("Email has been updated in Customer table");

		String Query_UpdateCustomerLoginEMail="update customerLogin set LoginID='automationfinext@gmail.com' where LoginID='"+uEmail+"'";
		stmt.executeUpdate(Query_UpdateCustomerLoginEMail);
		Log.info("Email has been updated in CustomerLogin table");
		
		stmt.close();
		con.close();
		
		return rs;
	}
	
	public int updateEmailforWithDrawl(String uEmail) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
			
		String Query_UpdateCustomerEMail="update customer set Email='automationfinext@gmail.com' where Email='"+uEmail+"'";
		int rs=stmt.executeUpdate(Query_UpdateCustomerEMail);
		Log.info("Email has been updated in Customer table");

		String Query_UpdateCustomerLoginEMail="update customerLogin set LoginID='automationfinext@gmail.com' where LoginID='"+uEmail+"'";
		stmt.executeUpdate(Query_UpdateCustomerLoginEMail);
		Log.info("Email has been updated in CustomerLogin table");
		
		stmt.close();
		con.close();
		
		return rs;
	}
	
	public int updateEmail(String NewEMail) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
			
		String Query_UpdateCustomerEMail="update customer set Email='parise.maheswari@gmail.com' where Email='"+NewEMail+"'";
		int rs=stmt.executeUpdate(Query_UpdateCustomerEMail);
		Log.info("Email has been updated in Customer table");

		String Query_UpdateCustomerLoginEMail="update customerLogin set LoginID='parise.maheswari@gmail.com' where LoginID='"+NewEMail+"'";
		stmt.executeUpdate(Query_UpdateCustomerLoginEMail);
		Log.info("Email has been updated in CustomerLogin table");
		
		stmt.close();
		con.close();
		
		return rs;
	}

	public int updateEmailforPasswordReset(String uEmail) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
			
		String Query_UpdateCustomerEMail="update customer set Email='automationfinext@gmail.com' where Email='"+uEmail+"'";
		int rs=stmt.executeUpdate(Query_UpdateCustomerEMail);
		Log.info("Email has been updated in Customer table");

		String Query_UpdateCustomerLoginEMail="update customerLogin set LoginID='automationfinext@gmail.com' where LoginID='"+uEmail+"'";
		stmt.executeUpdate(Query_UpdateCustomerLoginEMail);
		Log.info("Email has been updated in CustomerLogin table");
		
		stmt.close();
		con.close();
		
		return rs;
	}
	
	public int updateEmailforSendToStore(String uEmail) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
			
		String Query_UpdateCustomerEMail="update customer set Email='automationfinext@gmail.com' where Email='"+uEmail+"'";
		int rs=stmt.executeUpdate(Query_UpdateCustomerEMail);
		Log.info("Email has been updated in Customer table");

		String Query_UpdateCustomerLoginEMail="update customerLogin set LoginID='automationfinext@gmail.com' where LoginID='"+uEmail+"'";
		stmt.executeUpdate(Query_UpdateCustomerLoginEMail);
		Log.info("Email has been updated in CustomerLogin table");
		
		stmt.close();
		con.close();
		
		return rs;
	}
	
	public String getCx4Verification(String CxStatusCodeID, String OutletID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCx4Verify="Select top 1 CustomerID from Customer where CustomerStatusCodeID="+CxStatusCodeID+" and VerificationStatus=30 and OutletID="+OutletID+" order by 1 desc";
		String CxByStatus ="";
		ResultSet rs=stmt.executeQuery(Query_GetCx4Verify);
		
		while(rs.next())
		{
			CxByStatus=rs.getString("CustomerID");
		}

		closeDB(rs, stmt, con);
		return CxByStatus;
	}
	
	public String getCxAfterAccCreation(String CxStatusCodeID, String OutletID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCx4onlineQue="Select top 1 CustomerID from Customer where CustomerStatusCodeID="+CxStatusCodeID+" and OutletID="+OutletID+" order by 1 desc";
		String CxByStatus ="";
		ResultSet rs=stmt.executeQuery(Query_GetCx4onlineQue);
		
		while(rs.next())
		{
			CxByStatus=rs.getString("CustomerID");
		}

		closeDB(rs, stmt, con);
		return CxByStatus;
	}
	
	public String getCxByStatus(String CxStatusCodeID, String OutletID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxByStatus="Select top 1 customerid from customer where customerstatuscodeid="+CxStatusCodeID+" and OutletID="+OutletID+" order by 1 desc";
		String CxByStatus = "";
		ResultSet rs=stmt.executeQuery(Query_GetCxByStatus);
		
		while(rs.next())
		{
			CxByStatus=rs.getString("CustomerID");
		}

		closeDB(rs, stmt, con);
		return CxByStatus;
	}

	public String getCxByAppStatus(String AppStatus, String OutletID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxByAppStatus="Select top 1 customerid from customer where applicationstatus="+AppStatus+" and OutletID="+OutletID+" order by 1 desc";
		String CxByAppStatus = "";
		ResultSet rs=stmt.executeQuery(Query_GetCxByAppStatus);
		
		while(rs.next())
		{
			CxByAppStatus=rs.getString("CustomerID");
		}

		closeDB(rs, stmt, con);
		return CxByAppStatus;
	}
	
	public String getCxAfterAccCreation() throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCx4onlineQue="select top 1 c.customerid from customer c join customerapplication ca on c.customerid=ca.customerid where c.customerstatuscodeid=120 and c.agentstatus=20 and ca.IsWorkCompleted=1 and ca.IsBankDetailsCompleted=0 order by 1 desc";
		String CxByStatus ="";
		ResultSet rs=stmt.executeQuery(Query_GetCx4onlineQue);
		
		while(rs.next())
		{
			CxByStatus=rs.getString("CustomerID");
		}

		closeDB(rs, stmt, con);
		return CxByStatus;
	}
	
	public String getCx4Verification(String CxStatusCodeID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCx4Verify="Select top 1 CustomerID from Customer where CustomerStatusCodeID="+CxStatusCodeID+" and VerificationStatus=30 order by 1 desc";
		String CxByStatus ="";
		ResultSet rs=stmt.executeQuery(Query_GetCx4Verify);
		
		while(rs.next())
		{
			CxByStatus=rs.getString("CustomerID");
		}

		closeDB(rs, stmt, con);
		return CxByStatus;
	}

}
