package com.afqa.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CustomerDocument extends DBConnection{
	
	public List<String> getDocPath(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_getDocPath="Select DocFilePath from CustomerDocument where CustomerID="+CxID;
		List<String> DocPaths=new ArrayList<>();
		int i=0;
		ResultSet rs=stmt.executeQuery(Query_getDocPath);
		while(rs.next())
		{
			String DocPath=rs.getString("DocFilePath");
			DocPaths.add(i, DocPath);
			i++;
		}
		
		closeDB(rs, stmt, con);
		
		return DocPaths;
	}
	
	public List<String> getDocTypeID(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_getDocType="Select LTDocumentTypeID from CustomerDocument where CustomerID="+CxID;
		List<String> DocTypes=new ArrayList<>();
		int i=0;
		ResultSet rs=stmt.executeQuery(Query_getDocType);
		while(rs.next())
		{
			String DocType=rs.getString("LTDocumentTypeID");
			DocTypes.add(i, DocType);
			i++;
		}

		closeDB(rs, stmt, con);
		
		return DocTypes;
	}
	
	public int getLTDocumentTypeID(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetLTDocTypeID="Select LTDocumentTypeID from CustomerDocument where CustomerID="+CxID;
		int LTDocTypeID = 0 ;
		ResultSet rs=stmt.executeQuery(Query_GetLTDocTypeID);
		while(rs.next())
		{
			LTDocTypeID=rs.getInt("LTDocumentTypeID");
		}
				
		closeDB(rs, stmt, con);
		
		return LTDocTypeID;
	}
}
