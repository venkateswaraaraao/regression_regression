package com.afqa.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class CanariFi extends DBConnection
{
	public String getCanariFiID(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCanariFIID="Select top 1 CanariFiID from CanariFi where CustomerID="+CxID+" order by InsertedDate desc";
		int iCanFiID = 0;
		ResultSet rs=stmt.executeQuery(Query_GetCanariFIID);
		while(rs.next())
		{
			iCanFiID=rs.getInt("CanariFiID");
		}
		String CanFiID=String.valueOf(iCanFiID);

		closeDB(rs, stmt, con);
		
		return CanFiID;
	}
	
	public String getCustomerID(String NewEMail) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxStatus="Select CustomerID from CanariFi where Email='"+NewEMail+"'";
		int iCustID = 0;
		ResultSet rs=stmt.executeQuery(Query_GetCxStatus);
		while(rs.next())
		{
			iCustID=rs.getInt("CustomerID");
		}
		String CustID=String.valueOf(iCustID);

		closeDB(rs, stmt, con);
		
		return CustID;
	}
	
	public String getRequestStatus(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetReqStatus="Select RequestStatus from CanariFi where CustomerID="+CxID;
		int iReqStatus = 0;
		ResultSet rs=stmt.executeQuery(Query_GetReqStatus);
		while(rs.next())
		{
			iReqStatus=rs.getInt("RequestStatus");
		}
		String RequestStatus=String.valueOf(iReqStatus);

		closeDB(rs, stmt, con);
		
		return RequestStatus;
	}
	
	public String getCanariFiStatus(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCanariFi="Select top 1 CanariFiStatus from CanariFi where CustomerID="+CxID+" order by InsertedDate desc";
		int iCanariFi = 0;
		ResultSet rs=stmt.executeQuery(Query_GetCanariFi);
		while(rs.next())
		{
			iCanariFi=rs.getInt("CanariFiStatus");
		}
		String CanariFiStatus=String.valueOf(iCanariFi);

		closeDB(rs, stmt, con);
		
		return CanariFiStatus;
	}
	
	public String getTransactionCount(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetTransactionCount="Select top 1 TransactionCount from CanariFi where CustomerID="+CxID+" order by InsertedDate desc";
		int iTransactionCount = 0;
		ResultSet rs=stmt.executeQuery(Query_GetTransactionCount);
		while(rs.next())
		{
			iTransactionCount=rs.getInt("TransactionCount");
		}
		String TransactionCount=String.valueOf(iTransactionCount);

		closeDB(rs, stmt, con);
		
		return TransactionCount;
	}
	
	public int updateTransCount(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_UpdateTransCount="update CanariFi set TransactionCount=1 where CustomerID="+CxID;
		int rs=stmt.executeUpdate(Query_UpdateTransCount);
		
		stmt.close();
		con.close();
		
		return rs;
	}
}
