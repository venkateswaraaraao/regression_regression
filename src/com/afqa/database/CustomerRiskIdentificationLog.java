package com.afqa.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class CustomerRiskIdentificationLog extends DBConnection{
	
	public String getRiskRating(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetRiskRating="select top 1 * from CustomerRiskIdentificationLog where CustomerID="+CxID+" Order by 1 desc";
		String CxRiskRating="";
		ResultSet rs=stmt.executeQuery(Query_GetRiskRating);
		while(rs.next())
		{
			CxRiskRating=rs.getString("RiskRating");
		}

		closeDB(rs, stmt, con);
		
		return CxRiskRating;
	}
}
