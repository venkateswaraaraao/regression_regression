package com.afqa.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class LOC extends DBConnection{
	
	public String getLOCID(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetLOCID="Select LOCID from LOC where CustomerID="+CxID;
		String LOCID="";
		ResultSet rs=stmt.executeQuery(Query_GetLOCID);
		while(rs.next())
		{
			LOCID=rs.getString("LOCID");
		}

		closeDB(rs, stmt, con);
		
		return LOCID;
	}
	
	public String getLOCProductID(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetLOCProductID="Select LOCProductID from LOC where CustomerID="+CxID;
		String LOCProductID="";
		ResultSet rs=stmt.executeQuery(Query_GetLOCProductID);
		while(rs.next())
		{
			LOCProductID=rs.getString("LOCProductID");
		}

		closeDB(rs, stmt, con);
		
		return LOCProductID;
	}
	
	public String getOutletID(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetOutletID="Select OutletID from LOC where CustomerID="+CxID;
		String OutletID="";
		ResultSet rs=stmt.executeQuery(Query_GetOutletID);
		while(rs.next())
		{
			OutletID=rs.getString("OutletID");
		}

		closeDB(rs, stmt, con);
		
		return OutletID;
	}
	
	public String getLOCInitialLimit(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetLOCInitialLimit="Select LOCInitialLimit from LOC where CustomerID="+CxID;
		String LOCInitialLimit="";
		ResultSet rs=stmt.executeQuery(Query_GetLOCInitialLimit);
		while(rs.next())
		{
			LOCInitialLimit=rs.getString("LOCInitialLimit");
		}

		closeDB(rs, stmt, con);
		
		return LOCInitialLimit;
	}
	
	public String getLOCCurrentLimit(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetLOCCurrentLimit="Select LOCCurrentLimit from LOC where CustomerID="+CxID;
		String LOCCurrentLimit="";
		ResultSet rs=stmt.executeQuery(Query_GetLOCCurrentLimit);
		while(rs.next())
		{
			LOCCurrentLimit=rs.getString("LOCCurrentLimit");
		}

		closeDB(rs, stmt, con);
		
		return LOCCurrentLimit;
	}
	
	public String getCxAvailableCredit(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxAvailableCredit="Select AvailableCredit from LOC where CustomerID="+CxID;
		String AvailableCredit="";
		ResultSet rs=stmt.executeQuery(Query_GetCxAvailableCredit);
		while(rs.next())
		{
			AvailableCredit=rs.getString("AvailableCredit");
		}

		closeDB(rs, stmt, con);
		
		return AvailableCredit;
	}
	
	public String getCurrentLOCPrinciple(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCurLOCPrinciple="Select CurrentLOCPrinciple from LOC where CustomerID="+CxID;
		String CurLOCPrinciple="";
		ResultSet rs=stmt.executeQuery(Query_GetCurLOCPrinciple);
		while(rs.next())
		{
			CurLOCPrinciple=rs.getString("CurrentLOCPrinciple");
		}

		closeDB(rs, stmt, con);
		
		return CurLOCPrinciple;
	}
	
	public String getLOCStatusCodeID(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetLOCStatusCodeID="Select LOCStatusCodeID from LOC where CustomerID="+CxID;
		String LOCStatusCodeID="";
		ResultSet rs=stmt.executeQuery(Query_GetLOCStatusCodeID);
		while(rs.next())
		{
			LOCStatusCodeID=rs.getString("LOCStatusCodeID");
		}

		closeDB(rs, stmt, con);
		
		return LOCStatusCodeID;
	}
	
	public String getLOCPerformanceID(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetLOCPerformanceID="Select LOCPerformanceID from LOC where CustomerID="+CxID;
		String LOCPerformanceID="";
		ResultSet rs=stmt.executeQuery(Query_GetLOCPerformanceID);
		while(rs.next())
		{
			LOCPerformanceID=rs.getString("LOCPerformanceID");
		}

		closeDB(rs, stmt, con);
		
		return LOCPerformanceID;
	}
	
	public String getLOCAPR(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetLOCAPR="Select APR from LOC where CustomerID="+CxID;
		String LOCAPR="";
		ResultSet rs=stmt.executeQuery(Query_GetLOCAPR);
		while(rs.next())
		{
			LOCAPR=rs.getString("APR");
		}

		closeDB(rs, stmt, con);
		
		return LOCAPR;
	}
	
	public String getLTTimeZoneID(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetLTTimeZoneID="Select LTTimeZoneID from LOC where CustomerID="+CxID;
		String LTTimeZoneID="";
		ResultSet rs=stmt.executeQuery(Query_GetLTTimeZoneID);
		while(rs.next())
		{
			LTTimeZoneID=rs.getString("LTTimeZoneID");
		}

		closeDB(rs, stmt, con);
		
		return LTTimeZoneID;
	}
	
	public String getOriginatedOutletID(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetOriginatedOutletID="Select OriginatedOutletID from LOC where CustomerID="+CxID;
		String OriginatedOutletID="";
		ResultSet rs=stmt.executeQuery(Query_GetOriginatedOutletID);
		while(rs.next())
		{
			OriginatedOutletID=rs.getString("OriginatedOutletID");
		}

		closeDB(rs, stmt, con);
		
		return OriginatedOutletID;
	}
	
	public String getInitialUWAccountTypeID(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetInitialUWAccountTypeID="Select InitialUWAccountTypeID from LOC where CustomerID="+CxID;
		String InitialUWAccountTypeID="";
		ResultSet rs=stmt.executeQuery(Query_GetInitialUWAccountTypeID);
		while(rs.next())
		{
			InitialUWAccountTypeID=rs.getString("InitialUWAccountTypeID");
		}

		closeDB(rs, stmt, con);
		
		return InitialUWAccountTypeID;
	}
	
	public String getCurrentUWAccountTypeID(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCurrentUWAccountTypeID="Select CurrentUWAccountTypeID from LOC where CustomerID="+CxID;
		String CurrentUWAccountTypeID="";
		ResultSet rs=stmt.executeQuery(Query_GetCurrentUWAccountTypeID);
		while(rs.next())
		{
			CurrentUWAccountTypeID=rs.getString("CurrentUWAccountTypeID");
		}

		closeDB(rs, stmt, con);
		
		return CurrentUWAccountTypeID;
	}
	
	public String getCustomerIDToUpdateStatus() throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxID="Select top 1 CustomerID from Loc where locstatuscodeid=10 order by newid()";
		int iCustID = 0;
		ResultSet rs=stmt.executeQuery(Query_GetCxID);
		while(rs.next())
		{
			iCustID=rs.getInt("CustomerID");
		}
		String CustID=String.valueOf(iCustID);

		closeDB(rs, stmt, con);
		
		return CustID;
	}
	
	public String getlOCIDForPayment() throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetLOCID="select Customerid, * from customercard where status=0 and customerid in (select customerid from LOC where locstatuscodeid=20 and minpaymentdue>=30 and outletid=74)";
		int iLOCID = 0;
		ResultSet rs=stmt.executeQuery(Query_GetLOCID);
		while(rs.next())
		{
			iLOCID=rs.getInt("CustomerID");
		}
		String LOCID=String.valueOf(iLOCID);

		closeDB(rs, stmt, con);
		
		return LOCID;
	}

	public String getCustomerIDForPayment() throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCustomerID="select distinct cc.Customerid from CustomerCard cc join loc l on Cc.CustomerID=l.customerID join locstatement ls on l.LOCID=ls.LOCID Where cc.status=0 and l.locstatuscodeid=20 and l.locperformanceid =30 and MinPaymentDue>50 and datediff(d,ls.NextPaymentDueDate,getdate()) <=0 order by 1 desc";
		int iLOCID = 0;
		ResultSet rs=stmt.executeQuery(Query_GetCustomerID);
		while(rs.next())
		{
			iLOCID=rs.getInt("CustomerID");
		}
		String LOCID=String.valueOf(iLOCID);

		closeDB(rs, stmt, con);
		
		return LOCID;
	}
	
	public String getCustomerIDForMinPayment() throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCustomerID="select top 1 * from loc l join customercard cc on l.customerid=cc.customerid join locstatement ls on l.locid=ls.locid where l.locstatuscodeid=20 and l.outletid=86 and cc.status=0 and ls.StatementNumber>1 and ls.IsPaid=0 order by  1desc";
		int Customerid = 0;
		ResultSet rs=stmt.executeQuery(Query_GetCustomerID);
		while(rs.next())
		{
			Customerid=rs.getInt("CustomerID");
		}
		String CxID=String.valueOf(Customerid);

		closeDB(rs, stmt, con);
		
		return CxID;
	}
	
	public String getCxIDForWithDrawal() throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetLOCID="select top 1 c.customerid from customer c join customercard cc on c.customerid=cc.customerid join loc l on c.customerid=l.customerid where c.isverified=1 and cc.status=0 and l.isautopayaceepted=1 and l.LOCStatuscodeid=20 and l.AvailableCredit>200 and l.locperformanceid = 10 order by 1 desc";
		int iLOCID = 0;
		ResultSet rs=stmt.executeQuery(Query_GetLOCID);
		while(rs.next())
		{
			iLOCID=rs.getInt("CustomerID");
		}
		String LOCID=String.valueOf(iLOCID);

		closeDB(rs, stmt, con);
		
		return LOCID;
	}
	
	public String getCustomerIDToResetPassword() throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxID="Select top 1 CustomerID from Loc where locstatuscodeid=10 order by newid()";
		int iCustID = 0;
		ResultSet rs=stmt.executeQuery(Query_GetCxID);
		while(rs.next())
		{
			iCustID=rs.getInt("CustomerID");
		}
		String CustID=String.valueOf(iCustID);

		closeDB(rs, stmt, con);
		
		return CustID;
	}
}
