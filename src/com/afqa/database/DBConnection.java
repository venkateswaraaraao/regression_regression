package com.afqa.database;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.afqa.page.Constants_Page;

public class DBConnection{
	
	public String dbURL() throws ClassNotFoundException, SQLException, IOException
	{
		
		FileInputStream f=new FileInputStream("E:\\WS_AFQA_Regression\\FINEXT\\src\\com\\afqa\\configuration\\Config.xlsx");
		XSSFWorkbook wb=new XSSFWorkbook(f);
		XSSFSheet ws=wb.getSheetAt(2);
		int selectDB=(int) ws.getRow(8).getCell(1).getNumericCellValue();
		
		String DBserver = "";
		String uName = "";
		String pWord = "";
		String DBname = "";
		
		if(selectDB==1)
		{
			DBserver = "192.168.113.201";
			uName = "ucshekar";
			pWord = "Password@123";
			DBname = "finext";			
		}
			
		if(selectDB==2)
		{
			DBserver = "AF-NP1-SQL-01";
			uName = "appuser";
			pWord = "FINEXT!23 ";
			DBname = "finext_UAT";
		}
		
		if(selectDB==3)
		{
			DBserver = "172.16.17.6";
			uName = "appuser";
			pWord = "FINEXT!23";
			DBname = "finext";
		}
		
		if(selectDB==4)
		{
			DBserver = "40.122.113.154";
			uName = "appuser";
			pWord = "FINEXT!23";
			DBname = "finext";
		}
		if(selectDB==5)
		{
			DBserver = "40.122.207.216";
			uName = "appuser";
			pWord = "FINEXT!23";
			DBname = "finext";
		}
		if(selectDB==6)
		{
			DBserver = "168.61.180.56";
			uName = "appuser";
			pWord = "FINEXT!23";
			DBname = "finext";
		}
		if(selectDB==7)
		{
			DBserver = "137.116.47.107";
			uName = "appuser";
			pWord = "FINEXT!23";
			DBname = "finext_uat";
		}
		
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String jdbcUrl = "jdbc:sqlserver://"+DBserver+";user="+uName+";password="+pWord+";databaseName="+DBname;
		return jdbcUrl;
	}	
	
	public void closeDB(ResultSet rs, Statement stmt, Connection con) throws SQLException
	{
		rs.close();
		stmt.close();
		con.close();
	}	
	
	Constants_Page pageCon=new Constants_Page();
}
