package com.afqa.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

public class CustomerCard extends DBConnection{

	static Logger Log=Logger.getLogger(CustomerCard.class.getName());

	public int getCustomerCardID(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxCardID="Select CustomerCardID from CustomerCard where CustomerID="+CxID;
		int iCustomerCardID=0;
		ResultSet rs=stmt.executeQuery(Query_GetCxCardID);
		while(rs.next())
		{
			iCustomerCardID=rs.getInt("CustomerCardID");
		}

		closeDB(rs, stmt, con);
		
		return iCustomerCardID;
	}
	
	public int getCxCardLastDigits(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxCard4Digits="Select CCLastFourDigits from CustomerCard where CustomerID="+CxID;
		int iCxLast4Digits=0;
		ResultSet rs=stmt.executeQuery(Query_GetCxCard4Digits);
		while(rs.next())
		{
			iCxLast4Digits=rs.getInt("CCLastFourDigits");
		}

		closeDB(rs, stmt, con);
		
		return iCxLast4Digits;
	}
	
	public String getCxNameOnCard(String CxID,String CardLstDigits) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_CXnameOnCard="select nameonthedebitcard from customercard where customerid="+CxID+" and CCLastFourDigits="+CardLstDigits;
		ResultSet rs=stmt.executeQuery(Query_CXnameOnCard);

		String NameOnCard="";
		while(rs.next())
		{
			NameOnCard=rs.getString("NameOnTheDebitCard");
		}

		closeDB(rs, stmt, con);
		
		return NameOnCard;
	}
	
	public String getCardExpiryDate(String CxID, String CardLstDigits) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxEmail="select debitCardExpiryDt from customercard where customerid="+CxID+" and CCLastFourDigits="+CardLstDigits;
		String CardExpiry = "";
		ResultSet rs=stmt.executeQuery(Query_GetCxEmail);
		
		while(rs.next())
		{
			CardExpiry=rs.getString("debitCardExpiryDt");
			Log.info(CardExpiry);
		}

		closeDB(rs, stmt, con);
		return CardExpiry;
	}
	public String getCxCardStatus(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_GetCxCardStatus="Select status from CustomerCard where CustomerID="+CxID;
		int icardStatus=0;
		ResultSet rs=stmt.executeQuery(Query_GetCxCardStatus);
		while(rs.next())
		{
			icardStatus=rs.getInt("status");
		}

		closeDB(rs, stmt, con);
		
		String cardStatus=String.valueOf(icardStatus);

		return cardStatus;
	}
	
	public String getCxLPPPaymentMethodToken(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_CXLppPaymentToken="select LppPaymentMethodToken from customercard where customerid="+CxID;
		ResultSet rs=stmt.executeQuery(Query_CXLppPaymentToken);

		String LppPaymentToken="";
		while(rs.next())
		{
			LppPaymentToken=rs.getString("LppPaymentMethodToken");
		}

		closeDB(rs, stmt, con);
		
		return LppPaymentToken;
	}
	
	public String getCxRepayToken(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_CXRepayToken="select RepayCardToken from customercard where customerid="+CxID;
		ResultSet rs=stmt.executeQuery(Query_CXRepayToken);

		String RepayToken="";
		while(rs.next())
		{
			RepayToken=rs.getString("RepayCardToken");
		}

		closeDB(rs, stmt, con);
		
		return RepayToken;
	}
	
	public String getCxIngoTokenCode(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String jdbcUrl=dbURL();
		Connection con=DriverManager.getConnection(jdbcUrl);
		Statement stmt=con.createStatement();
		
		String Query_CXRepayToken="select IngoTokenCode from customercard where customerid="+CxID;
		ResultSet rs=stmt.executeQuery(Query_CXRepayToken);

		String IngoToken="";
		while(rs.next())
		{
			IngoToken=rs.getString("IngoTokenCode");
		}

		closeDB(rs, stmt, con);
		
		return IngoToken;
	}
}
