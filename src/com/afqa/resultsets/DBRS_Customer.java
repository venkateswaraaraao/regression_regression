package com.afqa.resultsets;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.log4j.Logger;

public class DBRS_Customer extends Constants
{
	static Logger Log = Logger.getLogger(DBRS_Customer.class.getName());
	
	
	public void getStatuses(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String CxStatusID=dbCx.getCustomerStatusID(CxID);
		String AppStatus=dbCx.getApplicationStatus(CxID);
		String AgentStatus=dbCx.getAgentStatus(CxID);
		String VerifyStatus=dbCx.getVerificationStatus(CxID);
		
		Log.info("Actuals: CxStatusCodeID: "+CxStatusID+", Applic.Status: "+AppStatus+", Agent.Status: "+AgentStatus+", Verif.Status: "+VerifyStatus);
	}

	public void VerifyEmail(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		dbCx.IsVerified(CxID);
	}
	
	public void getCustomerCardStatus(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String CustomerCardStatus=dbCxCard.getCxCardStatus(CxID);
		String LppPaymentToken=dbCxCard.getCxLPPPaymentMethodToken(CxID);
		String RepayToken=dbCxCard.getCxRepayToken(CxID);
		String IngoCardToken=dbCxCard.getCxIngoTokenCode(CxID);
		
		Log.info("Actuals: CustomerCardStatus: "+CustomerCardStatus+", LppPaymentToken: "+LppPaymentToken+", RepayToken: "+RepayToken+", IngoCardToken: "+IngoCardToken);
		
		if(LppPaymentToken==null && RepayToken!=null && IngoCardToken==null)
		{
			Log.info("RepayToken is inserted: "+RepayToken);
		}
		if(LppPaymentToken!=null && RepayToken==null && IngoCardToken==null)
		{
			Log.info("LppPaymentToken is inserted: "+LppPaymentToken);
		}
		if(LppPaymentToken==null && RepayToken==null && IngoCardToken!=null)
		{
			Log.info("IngoCardToken is inserted: "+IngoCardToken);
		}
		if(LppPaymentToken==null && RepayToken==null && IngoCardToken==null)
		{
			Log.info("Any One of the Value should not be null");
		}
	}
	
	public void getCanarifiStatusForEBV(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String CanarifiStatus=dbCanariFi.getCanariFiStatus(CxID);
		String RequestStatus=dbCanariFi.getRequestStatus(CxID);
		
		Log.info("Actuals: CanarifiStatus: "+CanarifiStatus+", RequestStatus: "+RequestStatus);
	}

	public void BeforeVerification(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String CxStatusID=dbCx.getCustomerStatusID(CxID);
		String AppStatus=dbCx.getApplicationStatus(CxID);
		String AgentStatus=dbCx.getAgentStatus(CxID);
		String VerifStatus=dbCx.getVerificationStatus(CxID);
		
		Log.info(CxStatusID+", "+AppStatus+", "+AgentStatus+", "+VerifStatus);
		
		String[] ActStatus={CxStatusID,AppStatus,AgentStatus,VerifStatus};
		String[] ExpStatus={"160","290","60","30"};
		String[] DataSet={"Customer","Application","Agent","Verification"};

		for(int j=0;j<ExpStatus.length;j++)
		{
			if(ActStatus[j].equals(ExpStatus[j]))
			{
				Log.info("(Expected:"+ExpStatus[j]+", Actual:"+ActStatus[j]+") "+DataSet[j]+" is matched");
			}
			else
			{
				Log.info("(Expected:"+ExpStatus[j]+", Actual:"+ActStatus[j]+") "+DataSet[j]+" is not matched");
			}
		}
	}
	
	public void AfterVerification(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String CxStatusID=dbCx.getCustomerStatusID(CxID);
		String AppStatus=dbCx.getApplicationStatus(CxID);
		String AgentStatus=dbCx.getAgentStatus(CxID);
		String VerifStatus=dbCx.getVerificationStatus(CxID);
		
		Log.info(CxStatusID+", "+AppStatus+", "+AgentStatus+", "+VerifStatus);
		
		String[] ActStatus={CxStatusID,AppStatus,AgentStatus,VerifStatus};
		String[] ExpStatus={"10","320","120","40"};
		String[] Dataset={"Customer","Application","Agent","Verification"};

		for(int j=0;j<ExpStatus.length;j++)
		{
			if(ActStatus[j].equals(ExpStatus[j]))
			{
				Log.info("(Expected:"+ExpStatus[j]+", Actual:"+ActStatus[j]+") "+Dataset[j]+" status is matched");
			}
			else
			{
				Log.info("(Expected:"+ExpStatus[j]+", Actual:"+ActStatus[j]+") "+Dataset[j]+" status is not matched");
			}
		}
	}
	
	public void LOC_setVerifyIsAccepted(String CxID) throws ClassNotFoundException, SQLException, IOException
	{
		String LOCID=dbLOC.getLOCID(CxID);
		String LOCProductID=dbLOC.getLOCProductID(CxID);
		String OutletID=dbLOC.getOutletID(CxID);
		String InitialLimit=dbLOC.getLOCInitialLimit(CxID);
		String CurrentLimit=dbLOC.getLOCCurrentLimit(CxID);
		String AvlbleCredit=dbLOC.getCxAvailableCredit(CxID);
		String StatusCodeID=dbLOC.getLOCStatusCodeID(CxID);
		String PerformanceID=dbLOC.getLOCPerformanceID(CxID);
		String APR=dbLOC.getLOCAPR(CxID);
		String TimeZone=dbLOC.getLTTimeZoneID(CxID);
		String OriginatedOutletID=dbLOC.getOriginatedOutletID(CxID);
		String InitialUWType=dbLOC.getInitialUWAccountTypeID(CxID);
		String CurrentUWType=dbLOC.getCurrentUWAccountTypeID(CxID);
		
		Log.info("LOCID is "+LOCID);
		Log.info("LOCProductID is "+ LOCProductID);
		Log.info("OutletID is "+OutletID);
		Log.info("InitialLimit is "+InitialLimit);
		Log.info("CurrentLimit is "+CurrentLimit);
		Log.info("AvlbleCredit is "+AvlbleCredit);
		Log.info("StatusCodeID is "+StatusCodeID);
		Log.info("PerformanceID is "+PerformanceID);
		Log.info("APR is "+APR);
		Log.info("TimeZone is "+TimeZone);
		Log.info("OriginatedOutletID is "+OriginatedOutletID);
		Log.info("InitialUWTypeID is "+InitialUWType);
		Log.info("CurrentUWTypeID is "+CurrentUWType);
		
		Log.info("-------------------------------------------------");
	}
}