package com.afqa.resultsets;

import java.io.IOException;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.afqa.database.CanariFi;
import com.afqa.database.Customer;
import com.afqa.database.CustomerCard;
import com.afqa.database.CustomerDocument;
import com.afqa.database.CustomerRiskIdentificationLog;
import com.afqa.database.DBConnection;
import com.afqa.database.LOC;
import com.afqa.database.OTPVerificationLog;
import com.afqa.utility.ExcelOperations;
import com.afqa.utility.RegisterActions;
import com.afqa.utility.ScreenShots;

public class Constants {
	
	public static RemoteWebDriver driver=null;
	
	public String StoreReconOutletID="1";
	public String GridHubURL="http://10.205.5.194:4444/wd/hub";
	
	//public String nodeURL="http://localhost:4444/wd/hub";
	public String nodeURL="http://localhost:4444/wd/hub";
	
	public ExcelOperations eo=new ExcelOperations();
	public ScreenShots sc=new ScreenShots();
	public RegisterActions ra=new RegisterActions();
	
	public DBConnection dbConnect=new DBConnection();
	public Customer dbCx=new Customer();
	public CustomerCard dbCxCard=new CustomerCard();
	public CustomerDocument dbCxDocs=new CustomerDocument();
	public CanariFi dbCanariFi=new CanariFi();
	public CustomerRiskIdentificationLog dbCxRiskLog=new CustomerRiskIdentificationLog();
	
	public LOC dbLOC=new LOC();
	public OTPVerificationLog otpVerification = new OTPVerificationLog();
	
	
	
	public String projectPath="E:\\WS_AFQA_Regression\\FINEXT\\";
	public String logPath=projectPath+"log4j.properties";
	
	public String aggrement=projectPath+"src\\com\\afqa\\excel\\Alabama_Eam.xlsx";
	public String statement=projectPath+"src\\com\\afqa\\excel\\Missouri Examination.xlsx";
	public String path0=projectPath+"src\\com\\afqa\\excel\\TestData_AFQA.xlsx";
	public String path2=projectPath+"src\\com\\afqa\\excel\\TestData_DirectFlows.xlsx";
	public String path1=projectPath+"src\\com\\afqa\\excel\\Underwritng Profile.xlsx";
	public String path3=projectPath+"src\\com\\afqa\\excel\\TestData_AFQA_IL.xlsx";
	public String path4=projectPath+"\\src\\com\\afqa\\excel\\Payment.xlsx";
	public String Config=projectPath+"src\\com\\afqa\\configuration\\Config.xlsx";
	public String dataEngine=projectPath+"src\\com\\afqa\\configuration\\DataEngine.xlsx";
	
	public String defineOutletID(String State)
	{
		String OutletID=null;
		if (State.equals("Tennessee"))
		{
			OutletID="74";
		}
		if (State.equals("Missouri"))
		{
			OutletID="81";
		}
		if (State.equals("Utah"))
		{
			OutletID="85";
		}
		if (State.equals("Kansas"))
		{
			OutletID="86";
		}
		if (State.equals("Idaho"))
		{
			OutletID="87";
		}
		if (State.equals("Alabama"))
		{
			OutletID="74";
		}
		if (State.equals("Virginia"))
		{
			OutletID="74";
		}
		if (State.equals("North Dakota"))
		{
			OutletID="74";
		}
		if (State.equals("Delaware"))
		{
			OutletID="74";
		}
		if (State.equals("South Carolina"))
		{
			OutletID="74";
		}
		if (State.equals("Wisconsin"))
		{
			OutletID="74";
		}
		if (State.equals("Mississippi"))
		{
			OutletID="74";
		}
		if (State.equals("Texas"))
		{
			OutletID="74";
		}
		return OutletID;
	}

	public int startRow(int r) throws IOException
	{
		int startRow=(int) eo.getCellValue(Config, 0, r, 1);
		return startRow;
	}
	
	public int endRow(int r) throws IOException
	{
		int endRow=(int) eo.getCellValue(Config, 0, r, 2);
		return endRow;
	}
	
	public String StoreURL() throws IOException
	{
		int selectDB=(int) eo.getCellValue(Config, 2, 8, 1);
		
		String StoreURL="http://finextqa.af247.com/#x";
		
		if(selectDB==1)
		{
			StoreURL="http://finextqa.af247.com/#x";
		}
		if(selectDB==2)
		{
			StoreURL="https://finext.plutodev.net/#x";
		}
		if(selectDB==3)
		{
			StoreURL="http://172.16.17.15";
			
		}
		if(selectDB==4)
		{
			StoreURL="https://qa1-store.plutodev.net/#x";
			
		}
		if(selectDB==5)
		{
			StoreURL="http://qa2-store.plutodev.net/#x";
		}
		if(selectDB==6)
		{
			StoreURL="https://qa3-store.plutodev.net";
		}
		if(selectDB==7)
		{
			StoreURL="https://uat-store.plutodev.net/#x";
		}
		return StoreURL;
	}
	
	public String AdminURL() throws IOException
	{
		int selectDB=(int) eo.getCellValue(Config, 2, 8, 1);
		
		String AdminURL="";
		
		if(selectDB==1)
		{
			AdminURL="http://finextqa.af247.com/admin/adminhome";
		}
		if(selectDB==2)
		{
			AdminURL="https://finext.plutodev.net/admin/adminhome";
		}
		if(selectDB==3)
		{
			AdminURL="http://172.16.17.15/Admin/AdminHome";
		}
		if(selectDB==4)
		{
			AdminURL="https://qa1-store.plutodev.net/admin/adminhome";
		}
		if(selectDB==5)
		{
			AdminURL="http://qa2-store.plutodev.net/admin/adminhome";
		}
		if(selectDB==6)
		{
			AdminURL="https://qa3-store.plutodev.net/admin/adminhome#x";
		}
		if(selectDB==7)
		{
			AdminURL=" https://uat-store.plutodev.net/Admin/AdminHome#x";
		}
		return AdminURL;
	}
	
	public String CustomerURL() throws IOException
	{
		int selectDB=(int) eo.getCellValue(Config, 2, 8, 1);
		
		String CustURL="";
		
		if(selectDB==1)
		{
			CustURL="http://finextqa.af247.com/web";
		}
		if(selectDB==2)
		{
			CustURL="https://online.plutodev.net/#x";
		}
		if(selectDB==3)
		{
			CustURL="http://172.16.17.16";
		}
		if(selectDB==4)
		{
			CustURL="https://qa1-online.plutodev.net/#x";
		}
		if(selectDB==5)
		{
			CustURL="http://qa2-online.plutodev.net/#x";
		}
		if(selectDB==6)
		{
			CustURL="http://qa3-online.plutodev.net/#x";
		}
		if(selectDB==7)
		{
			CustURL="https://uat-online.plutodev.net/#x";
		}
		return CustURL;
	}

	public String CxRegisterURL() throws IOException
	{
		int selectDB=(int) eo.getCellValue(Config, 2, 8, 1);
		
		String CxRegisterURL="";
		
		if(selectDB==1)
		{
			CxRegisterURL="http://finextqa.af247.com/web/OnlineRegistration/RegisterCustomer/AccountCreation?IsFirstReq=True&CampaignID=#x";
		}
		if(selectDB==2)
		{
			CxRegisterURL="https://online.plutodev.net/OnlineRegistration/RegisterCustomer/AccountCreation?IsFirstReq=True&CampaignID=#x";
		}
		if(selectDB==3)
		{
			CxRegisterURL="http://172.16.17.16/Partner/LeadCampaign?URL=http://172.16.17.16/OnlineRegistration/RegisterCustomer/AccountCreation?IsFirstReq=True&CampaignID=";
		}
		if(selectDB==4)
		{
			CxRegisterURL="http://qa1-online.plutodev.net/OnlineRegistration/RegisterCustomer/AccountCreation?IsFirstReq=True&CampaignID=#x";
		}
		if(selectDB==5)
		{
			CxRegisterURL="http://qa2-online.plutodev.net/OnlineRegistration/RegisterCustomer/AccountCreation?IsFirstReq=True&CampaignID=#x";
		}
		if(selectDB==6)
		{
			CxRegisterURL="http://qa3-online.plutodev.net/OnlineRegistration/RegisterCustomer/AccountCreation?IsFirstReq=True&CampaignID=#x";
		}
		if(selectDB==7)
		{
			CxRegisterURL="https://uat-online.plutodev.net/OnlineRegistration/RegisterCustomer/AccountCreation?IsFirstReq=True&CampaignID=#x";
		}
		return CxRegisterURL;
	}
	
}
