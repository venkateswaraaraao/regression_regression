package com.afqa.page;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Online_Login_Page{

	static Logger Log = Logger.getLogger("Online_Login_Page");
	
	@FindBy(id="Username")
	WebElement emailID;
	@FindBy(id="Password")
	WebElement pWord;
	@FindBy(className="loginButton")
	WebElement loginBtn;
	@FindBy(linkText="Submit")
	WebElement submitBtn;
	@FindBy(linkText="Create an Account.")
	WebElement CreateAccLink;
	@FindBy(id="RegisterSSN")
	WebElement SSNtxt;
	@FindBy(id="DOB")
	WebElement DOBtxt;
	@FindBy(className="ForgotPasswordSSN")
	WebElement forgotPwdSSN;
	@FindBy(className="ForgotPasswordDOB")
	WebElement forgotPwdDOB;
	@FindBy(id="globalPopup")
	WebElement PopUp;
	//@FindBy(xpath="//*[contains(@fnhref,'/web/CustomerLogin/ForgotPassword/')]")
	@FindBy(partialLinkText="Forgot your Email or Password")
	WebElement ForgotPWDLink;
	@FindBy(id="SecurityAnswer")
	WebElement securityAns;
	@FindBy(linkText="Click here to try logging in again.")
	WebElement AFLoginLink;
	@FindBy(linkText="OK")
	WebElement OKBtn;
	@FindBy(linkText="Create Account")
	WebElement CreateAccBTN;
	@FindBy(linkText="GET CASH NOW")
	WebElement DirectRegLink;
	@FindBy(linkText="Decline")
	WebElement declineBtn;
	@FindBy(linkText="Finish at an Advance Financial store")
	WebElement finishAtStoreBtn;
	
	public void onlineLogin(String uEmail)
	{
		emailID.click();
		Sleeper.sleepTightInSeconds(1);
		emailID.sendKeys(uEmail);
		pWord.sendKeys("password");
		Sleeper.sleepTightInSeconds(2);
		loginBtn.click();
	}
	
	public void ToRegister(WebDriverWait wait) throws InterruptedException
	{
		CreateAccLink.click();
		Sleeper.sleepTightInSeconds(1);
		SSNtxt.click();
		SSNtxt.sendKeys("956824455");
		DOBtxt.click();
		DOBtxt.sendKeys("12121975");
		Sleeper.sleepTightInSeconds(1);
		
		DOBtxt.sendKeys(Keys.TAB);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(2);
		CreateAccBTN.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("OK")));		
		OKBtn.click();
		Sleeper.sleepTightInSeconds(1);
		CreateAccBTN.click();
	}
	
	public void createOnlineAcc(WebDriverWait wait, String stSSN, String stDOB)
	{
		CreateAccLink.click();
		Sleeper.sleepTightInSeconds(5);
		SSNtxt.click();
		Sleeper.sleepTightInSeconds(1);
		SSNtxt.sendKeys(stSSN);
		
		DOBtxt.click();
		DOBtxt.sendKeys(stDOB);
		Sleeper.sleepTightInSeconds(1);
		
		DOBtxt.sendKeys(Keys.TAB);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(2);
		CreateAccBTN.click();
	}
	
	public void GetCash()
	{
		DirectRegLink.click();
	}
	
	public void ForgotPassword(String NewEMail,String CxSecAns,WebDriverWait wait)
	{
		ForgotPWDLink.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Username")));
		Sleeper.sleepTightInSeconds(2);
		
		emailID.click();
		Sleeper.sleepTightInSeconds(2);
		emailID.sendKeys(NewEMail);
		Sleeper.sleepTightInSeconds(1);
		
		loginBtn.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("SecurityAnswer")));
		
		securityAns.click();
		securityAns.sendKeys(CxSecAns);
		Sleeper.sleepTightInSeconds(2);
		
		submitBtn.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("OK")));
		OKBtn.click();
	}
}
