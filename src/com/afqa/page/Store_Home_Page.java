package com.afqa.page;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Store_Home_Page {
	
	static Logger Log = Logger.getLogger("Store Home Page");
	
	@FindBy(className="logo")
	WebElement AFLogo;
	@FindBy(id="MainHdr_Home")
	WebElement HomeImg;
	@FindBy(xpath="html/body/div[1]/div[1]/div/ul/li[2]/a")
	WebElement MOSetUpImg;
	@FindBy(xpath="//*[@id='body']/div/div[7]/div/div[2]/div/div/ul/li[1]/a/img")
	WebElement AddMOIcon;
	@FindBy(css="a.TellerAssignLink[title='Teller Not Assigned']")
	WebElement TellerAssignLink;
	@FindBy(css="a.TellerAssignLink[title='Teller Assigned']")
	WebElement TellerAssignedLink;
	@FindBy(id="miscTransactionsHomepageLink")
	WebElement ViewMiscTransImg;
	@FindBy(id="StartDate")
	WebElement ViewMisc_StartDate;
	@FindBy(xpath="html/body/div[1]/div[1]/div/ul/li[7]/a/img")
	WebElement MiscTransImg;
	@FindBy(css="a.safeAssignLink[title='Safe Not Assigned']")
	WebElement SafeAssignImg;
	@FindBy(css="a.safeAssignLink[title='Safe Assigned']")
	WebElement SafeAssignedImg;
	@FindBy(xpath="//*[@id='MainHdr_Registratoin']/img")
	WebElement NewCxLink;
	@FindBy(xpath="html/body/div[1]/div[2]/a[1]/img")
	WebElement ReportsImg;
	@FindBy(id="MainHdr_Registratoin")
	WebElement CxRegisterImg;
	@FindBy(xpath="html/body/div[1]/div[2]/span[2]/a/img")
	WebElement CurrentVerificationImg;
	@FindBy(xpath="html/body/div[1]/div[2]/span[3]/a/img")
	WebElement ChangePWDImg;
	@FindBy(name="currentPassword")
	WebElement CurrentPWD;
	@FindBy(xpath="html/body/div[1]/div[2]/div[2]/a")
	WebElement LogoutImg;
	@FindBy(className="txtSearchOutlet")
	WebElement CxSearch;
	@FindBy(xpath="//*[@id='body']/div/div[7]/div/div[1]/a")
	WebElement CxSearchBtn;
	@FindBy(className="customerSearchResultsSection")
	WebElement CxSearchResultBlock;
	@FindBy(xpath="//*[@id='body']/div/div[7]/div/div[3]/div/div/div[1]/div/table/tbody/tr/td[6]/a")
	WebElement searchResultLink;
	@FindBy(xpath="//*[@id='body']/div/div[7]/table/tbody/tr/td[2]/div/table/tbody/tr[3]/td[1]/a")
	WebElement PickCxInList;
	@FindBy(css="img[title='Bankrupt']")
	WebElement bankruptBtn;
	@FindBy(id="BankruptcyType")
	WebElement bnkruptcytype;
	@FindBy(id="BankruptcyDate")
	WebElement bankruptcyDate;
	@FindBy(id="FileNumberValue")
	WebElement fileNbr;
	@FindBy(id="AttorneyName")
	WebElement attorneyName;
	@FindBy(id="AttorneyPhoneNumber")
	WebElement attorneyPh;
	@FindBy(className="formsubmit")
	WebElement bankruptcySubmit;
	@FindBy(css="img[title='Deceased']")
	WebElement deceasedBtn;
	@FindBy(id="button")
	WebElement SubmitBtn;
	@FindBy(className="popupDialogYes")
	WebElement okBtn;
	@FindBy(id="DateOfDeath")
	WebElement DOD;
	@FindBy(id="ReportedBy")
	WebElement reportedBy;
	@FindBy(id="RelationShiptoCustomer")
	WebElement relation2Cx;
	@FindBy(name="Note")
	WebElement Notes;
	@FindBy(xpath="//*[@id='LoginBody']/table/tbody/tr/td/div[8]/a/img")
	WebElement decStoreReq;
	@FindBy(linkText="Approved")
	WebElement approveBtn;
	@FindBy(css="img[title='Blocked']")
	WebElement blockedBtn;
	@FindBy(id="ReasonForBlock")
	WebElement reasonForBlock;
	@FindBy(id="Notes")
	WebElement notesForBlock;
	@FindBy(id="button")
	WebElement processBtn;
	
	public void storeHomeLinks(WebDriver driver,WebDriverWait wait) throws InterruptedException
	{
		MOSetUpImg.click();
		if(driver.findElements(By.xpath("//*[@id='body']/div/div[7]/div/div[2]/div/div/ul/li[1]/a/img")).size()>0)
		{
			Log.info("Set up MO page opened");
		}
		else
		{
			Log.info("Set up MO page not opened");
		}
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(2);
		HomeImg.click();
		ViewMiscTransImg.click();
		if(driver.findElements(By.id("StartDate")).size()>0)
		{
			Log.info("View Misc Transaction page is opened");
		}
		else
		{
			Log.info("View Misc Transaction page is not opened");
		}
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		AFLogo.click();
		MiscTransImg.click();
		if(driver.findElements(By.id("Disbursementselection")).size()>0)
		{
			Log.info("Misc Transaction page is opened");
		}
		else
		{
			Log.info("Misc Transaction page is not opened");
		}
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		HomeImg.click();
		ReportsImg.click();
		if(driver.findElements(By.xpath("//*[@id='body']/div/div[7]/div/div[2]/div/div/ul/li[1]/a/img")).size()>0)
		{
			Log.info("Store Reports page is opened");
		}
		else
		{
			Log.info("Store Reports page is not opened");
		}
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		HomeImg.click();
		CxRegisterImg.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		if(driver.findElements(By.id("PersonalDetails_SSN")).size()>0)
		{
			Log.info("New Customer registration page is opened");
		}
		else
		{
			Log.info("New Customer registration page is not opened");
		}
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		HomeImg.click();
		CurrentVerificationImg.click();
		if(driver.findElements(By.xpath("//*[@id='BodyDiv']/div/table/tbody/tr/td[1]/a/img")).size()>0)
		{
			Log.info("Current verifications page is opened");
		}
		else
		{
			Log.info("Current verifications page is not opened");
		}
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		HomeImg.click();
		ChangePWDImg.click();
		if(driver.findElements(By.name("currentPassword")).size()>0)
		{
			Log.info("Change Password page is opened");
		}
		else
		{
			Log.info("Change Password page is not opened");
		}
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		HomeImg.click();
		LogoutImg.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		if(driver.findElements(By.id("Username")).size()>0)
		{
			Log.info("Logged out done successfully");
		}
		else
		{
			Log.info("Logout failed");
		}
	}
	
	public void OpenCxRegister(WebDriverWait wait)
	{
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(2);
		NewCxLink.click();
	}
	
	public void PickCx()
	{
		PickCxInList.click();
	}
	
	public void pickCxbyID(WebDriver driver, String CxID, WebDriverWait wait)
	{
		CxSearch.sendKeys(CxID);
		Log.info("***** Customer ID is *****"+CxID);
		CxSearchBtn.click();
		Sleeper.sleepTightInSeconds(2);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("outletCustomerSearchProgress")));
		Sleeper.sleepTightInSeconds(1);
		driver.findElement(By.linkText(CxID)).click();
	}
	
	public void pickCx(WebDriverWait wait, WebDriver driver, String CxID)
	{
		CxSearch.sendKeys(CxID);
		Log.info("***** Customer ID is *****"+CxID);
		CxSearchBtn.click();
		Sleeper.sleepTightInSeconds(2);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("outletCustomerSearchProgress")));
		Sleeper.sleepTightInSeconds(1);
	}
	
	public void Bankrupt(WebDriverWait wait)
	{
		bankruptBtn.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("BankruptcyType")));
		
		new Select(bnkruptcytype).selectByIndex(1);
		Sleeper.sleepTightInSeconds(2);
		
		bankruptcyDate.click();
		bankruptcyDate.sendKeys(Keys.BACK_SPACE);
		bankruptcyDate.sendKeys(Keys.BACK_SPACE);
		bankruptcyDate.sendKeys(Keys.BACK_SPACE);
		bankruptcyDate.sendKeys(Keys.BACK_SPACE);
		bankruptcyDate.sendKeys(Keys.BACK_SPACE);
		bankruptcyDate.sendKeys(Keys.BACK_SPACE);
		bankruptcyDate.sendKeys(Keys.BACK_SPACE);
		bankruptcyDate.sendKeys(Keys.BACK_SPACE);
		bankruptcyDate.sendKeys("07092018");
		Sleeper.sleepTightInSeconds(1);
		
		bankruptcyDate.sendKeys(Keys.TAB);
		fileNbr.sendKeys("1323654");
		Sleeper.sleepTightInSeconds(1);
		
		attorneyName.sendKeys("Vendetta");
		Sleeper.sleepTightInSeconds(1);
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		attorneyPh.click();
		attorneyPh.sendKeys("3245678809");
		
		Sleeper.sleepTightInSeconds(1);
		bankruptcySubmit.click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("popupDialogYes")));
		okBtn.click();
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(3);
		LogoutImg.click();
	}
	
	public void clickOnDeceased() 
	{
		deceasedBtn.click();
	}
	public void Deceased(WebDriverWait wait) throws AWTException
	{	
		/*Robot rbt=new Robot();
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);*/
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("DateOfDeath")));
		DOD.sendKeys(Keys.BACK_SPACE);
		DOD.sendKeys(Keys.BACK_SPACE);
		DOD.sendKeys(Keys.BACK_SPACE);
		DOD.sendKeys(Keys.BACK_SPACE);
		DOD.sendKeys(Keys.BACK_SPACE);
		DOD.sendKeys(Keys.BACK_SPACE);
		DOD.sendKeys(Keys.BACK_SPACE);
		DOD.sendKeys(Keys.BACK_SPACE);
		Sleeper.sleepTightInSeconds(1);
		DOD.sendKeys("08072018");

		Sleeper.sleepTightInSeconds(2);
		
		Robot rbt=new Robot();
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		
		reportedBy.sendKeys("James Walton");
		
		new Select(relation2Cx).selectByIndex(1);
		Sleeper.sleepTightInSeconds(2);
		Notes.sendKeys("This is Notes block");
		SubmitBtn.click();
		Sleeper.sleepTightInSeconds(3);
		okBtn.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(3);
		LogoutImg.click();
	}
	
	public void adminApprovalForBankrupt(String CxID, WebDriver driver, WebDriverWait wait)
	{
		decStoreReq.click();
		Sleeper.sleepTightInSeconds(15);
		
		String xpOne="//*[@id='BodyDiv']/div/table/tbody/tr[3]/td/div/table/tbody/tr[";
		String xpTwo="]/td[1]";
		
		int i=2;
		for(int j=0;j<25;j++)
		{
			int k=j+(i*(j+1));
			Log.info(j);
			String XpFull=xpOne+(k)+xpTwo;
			
			String CustID=driver.findElement(By.xpath(XpFull)).getText();
			Log.info(CustID);
			
			if(CustID.equals(CxID))
			{
				driver.findElement(By.xpath(XpFull)).click();
				break;
			}
		}
	
		Sleeper.sleepTightInSeconds(5);
		approveBtn.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("popupDialogYes")));
		okBtn.click();
	}
	
	public void block(WebDriverWait wait)
	{
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("img[title='Blocked']")));
		blockedBtn.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("ReasonForBlock")));
		
		new Select(reasonForBlock).selectByIndex(2);
		
		notesForBlock.sendKeys("This is notes");
		
		processBtn.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("popupDialogYes")));
		okBtn.click();
		Sleeper.sleepTightInSeconds(5);
		LogoutImg.click();
	}
}
