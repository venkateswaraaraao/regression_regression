package com.afqa.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Online_CxLogout_Page {
	
	@FindBy(xpath="html/body/header/div[3]/a[2]/img")
	WebElement LogoutBtn;
	
	public void LogOut()
	{
		LogoutBtn.click();
	}
	
}
