package com.afqa.page;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.FindBy;

public class Yodlee_Page {
	
	static Logger Log = Logger.getLogger(Yodlee_Page.class.getName());
	
	@FindBy(tagName="iframe")
	WebElement YodleeObject;
	@FindBy(id="siteSearch")
	WebElement YodleeSearch;
	@FindBy(xpath="//*[@id='searchSites']/div[1]/div")
	WebElement YodleeSearchRes;
	@FindBy(className="EBVPopUp")
	WebElement YodleePopup;
	@FindBy(id = "LOGIN_22")
	WebElement YodleeUName;
	@FindBy(name = "PASSWORD")
	WebElement YodleePWD;
	@FindBy(name = "Re-enter_PASSWORD")
	WebElement YodleeConfPWD;
	@FindBy(id="next")
	WebElement YodleeNextBtn;
	
	public void performYodleeSuccess() throws AWTException
	{
		Robot rbt = new Robot();		
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		Sleeper.sleepTightInSeconds(1);
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		Sleeper.sleepTightInSeconds(1);
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		Sleeper.sleepTightInSeconds(1);
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		Sleeper.sleepTightInSeconds(1);
		
		/*StringSelection ss = new StringSelection("TekTest1.site16441.2");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
		
		rbt.keyPress(KeyEvent.VK_CONTROL);
		rbt.keyPress(KeyEvent.VK_V);
		rbt.keyRelease(KeyEvent.VK_CONTROL);
		rbt.keyRelease(KeyEvent.VK_V);
		Sleeper.sleepTightInSeconds(1);*/
		
		rbt.keyPress(KeyEvent.VK_CAPS_LOCK);
		rbt.keyRelease(KeyEvent.VK_CAPS_LOCK);
		rbt.keyPress(KeyEvent.VK_T);
		rbt.keyRelease(KeyEvent.VK_T);
		rbt.keyPress(KeyEvent.VK_CAPS_LOCK);
		rbt.keyRelease(KeyEvent.VK_CAPS_LOCK);
		
		Sleeper.sleepTightInSeconds(2);
		rbt.keyPress(KeyEvent.VK_E);
		rbt.keyRelease(KeyEvent.VK_E);
		rbt.keyPress(KeyEvent.VK_K);
		rbt.keyRelease(KeyEvent.VK_K);
		
		rbt.keyPress(KeyEvent.VK_CAPS_LOCK);
		rbt.keyRelease(KeyEvent.VK_CAPS_LOCK);
		rbt.keyPress(KeyEvent.VK_T);
		rbt.keyRelease(KeyEvent.VK_T);
		rbt.keyPress(KeyEvent.VK_CAPS_LOCK);
		rbt.keyRelease(KeyEvent.VK_CAPS_LOCK);
		
		
		rbt.keyPress(KeyEvent.VK_E);
		rbt.keyRelease(KeyEvent.VK_E);
		rbt.keyPress(KeyEvent.VK_S);
		rbt.keyRelease(KeyEvent.VK_S);
		rbt.keyPress(KeyEvent.VK_T);
		rbt.keyRelease(KeyEvent.VK_T);
		rbt.keyPress(KeyEvent.VK_1);
		rbt.keyRelease(KeyEvent.VK_1);
		rbt.keyPress(KeyEvent.VK_PERIOD);
		rbt.keyRelease(KeyEvent.VK_PERIOD);
		
		rbt.keyPress(KeyEvent.VK_S);
		rbt.keyRelease(KeyEvent.VK_S);
		rbt.keyPress(KeyEvent.VK_I);
		rbt.keyRelease(KeyEvent.VK_I);
		rbt.keyPress(KeyEvent.VK_T);
		rbt.keyRelease(KeyEvent.VK_T);
		rbt.keyPress(KeyEvent.VK_E);
		rbt.keyRelease(KeyEvent.VK_E);
		rbt.keyPress(KeyEvent.VK_1);
		rbt.keyRelease(KeyEvent.VK_1);
		rbt.keyPress(KeyEvent.VK_6);
		rbt.keyRelease(KeyEvent.VK_6);
		rbt.keyPress(KeyEvent.VK_4);
		rbt.keyRelease(KeyEvent.VK_4);
		rbt.keyPress(KeyEvent.VK_4);
		rbt.keyRelease(KeyEvent.VK_4);
		rbt.keyPress(KeyEvent.VK_1);
		rbt.keyRelease(KeyEvent.VK_1);
		rbt.keyPress(KeyEvent.VK_PERIOD);
		rbt.keyRelease(KeyEvent.VK_PERIOD);
		rbt.keyPress(KeyEvent.VK_2);
		rbt.keyRelease(KeyEvent.VK_2);
		Sleeper.sleepTightInSeconds(1);
		
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		rbt.keyPress(KeyEvent.VK_S);
		rbt.keyRelease(KeyEvent.VK_S);
		rbt.keyPress(KeyEvent.VK_I);
		rbt.keyRelease(KeyEvent.VK_I);
		rbt.keyPress(KeyEvent.VK_T);
		rbt.keyRelease(KeyEvent.VK_T);
		rbt.keyPress(KeyEvent.VK_E);
		rbt.keyRelease(KeyEvent.VK_E);
		rbt.keyPress(KeyEvent.VK_1);
		rbt.keyRelease(KeyEvent.VK_1);
		rbt.keyPress(KeyEvent.VK_6);
		rbt.keyRelease(KeyEvent.VK_6);
		rbt.keyPress(KeyEvent.VK_4);
		rbt.keyRelease(KeyEvent.VK_4);
		rbt.keyPress(KeyEvent.VK_4);
		rbt.keyRelease(KeyEvent.VK_4);
		rbt.keyPress(KeyEvent.VK_1);
		rbt.keyRelease(KeyEvent.VK_1);
		rbt.keyPress(KeyEvent.VK_PERIOD);
		rbt.keyRelease(KeyEvent.VK_PERIOD);
		rbt.keyPress(KeyEvent.VK_2);
		rbt.keyRelease(KeyEvent.VK_2);
		Sleeper.sleepTightInSeconds(1);
		
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		Sleeper.sleepTightInSeconds(1);
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		Sleeper.sleepTightInSeconds(1);
		rbt.keyPress(KeyEvent.VK_S);
		rbt.keyRelease(KeyEvent.VK_S);
		rbt.keyPress(KeyEvent.VK_I);
		rbt.keyRelease(KeyEvent.VK_I);
		rbt.keyPress(KeyEvent.VK_T);
		rbt.keyRelease(KeyEvent.VK_T);
		rbt.keyPress(KeyEvent.VK_E);
		rbt.keyRelease(KeyEvent.VK_E);
		rbt.keyPress(KeyEvent.VK_1);
		rbt.keyRelease(KeyEvent.VK_1);
		rbt.keyPress(KeyEvent.VK_6);
		rbt.keyRelease(KeyEvent.VK_6);
		rbt.keyPress(KeyEvent.VK_4);
		rbt.keyRelease(KeyEvent.VK_4);
		rbt.keyPress(KeyEvent.VK_4);
		rbt.keyRelease(KeyEvent.VK_4);
		rbt.keyPress(KeyEvent.VK_1);
		rbt.keyRelease(KeyEvent.VK_1);
		rbt.keyPress(KeyEvent.VK_PERIOD);
		rbt.keyRelease(KeyEvent.VK_PERIOD);
		rbt.keyPress(KeyEvent.VK_2);
		rbt.keyRelease(KeyEvent.VK_2);
		Sleeper.sleepTightInSeconds(1);
		
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		
		Sleeper.sleepTightInSeconds(1);
		rbt.keyPress(KeyEvent.VK_ENTER);
		rbt.keyRelease(KeyEvent.VK_ENTER);
	}
	
	public void performYodleeLoginFailed() throws AWTException
	{
		Robot rbt = new Robot();		
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		Sleeper.sleepTightInSeconds(1);
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		Sleeper.sleepTightInSeconds(1);
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		Sleeper.sleepTightInSeconds(1);
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		Sleeper.sleepTightInSeconds(1);
		
		StringSelection ss = new StringSelection("TekTest1.site16441.2");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
		
		rbt.keyPress(KeyEvent.VK_CONTROL);
		rbt.keyPress(KeyEvent.VK_V);
		rbt.keyRelease(KeyEvent.VK_CONTROL);
		rbt.keyRelease(KeyEvent.VK_V);
		Sleeper.sleepTightInSeconds(1);
		
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		rbt.keyPress(KeyEvent.VK_S);
		rbt.keyRelease(KeyEvent.VK_S);
		rbt.keyPress(KeyEvent.VK_I);
		rbt.keyRelease(KeyEvent.VK_I);
		rbt.keyPress(KeyEvent.VK_T);
		rbt.keyRelease(KeyEvent.VK_T);
		rbt.keyPress(KeyEvent.VK_E);
		rbt.keyRelease(KeyEvent.VK_E);
		rbt.keyPress(KeyEvent.VK_1);
		rbt.keyRelease(KeyEvent.VK_1);
		rbt.keyPress(KeyEvent.VK_6);
		rbt.keyRelease(KeyEvent.VK_6);
		rbt.keyPress(KeyEvent.VK_4);
		rbt.keyRelease(KeyEvent.VK_4);
		rbt.keyPress(KeyEvent.VK_4);
		rbt.keyRelease(KeyEvent.VK_4);
		rbt.keyPress(KeyEvent.VK_1);
		rbt.keyRelease(KeyEvent.VK_1);
		rbt.keyPress(KeyEvent.VK_PERIOD);
		rbt.keyRelease(KeyEvent.VK_PERIOD);
		rbt.keyPress(KeyEvent.VK_2);
		rbt.keyRelease(KeyEvent.VK_2);
		Sleeper.sleepTightInSeconds(1);
		
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		Sleeper.sleepTightInSeconds(1);
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		Sleeper.sleepTightInSeconds(1);
		rbt.keyPress(KeyEvent.VK_S);
		rbt.keyRelease(KeyEvent.VK_S);
		rbt.keyPress(KeyEvent.VK_I);
		rbt.keyRelease(KeyEvent.VK_I);
		rbt.keyPress(KeyEvent.VK_T);
		rbt.keyRelease(KeyEvent.VK_T);
		rbt.keyPress(KeyEvent.VK_E);
		rbt.keyRelease(KeyEvent.VK_E);
		rbt.keyPress(KeyEvent.VK_1);
		rbt.keyRelease(KeyEvent.VK_1);
		rbt.keyPress(KeyEvent.VK_6);
		rbt.keyRelease(KeyEvent.VK_6);
		rbt.keyPress(KeyEvent.VK_4);
		rbt.keyRelease(KeyEvent.VK_4);
		rbt.keyPress(KeyEvent.VK_4);
		rbt.keyRelease(KeyEvent.VK_4);
		rbt.keyPress(KeyEvent.VK_1);
		rbt.keyRelease(KeyEvent.VK_1);
		rbt.keyPress(KeyEvent.VK_PERIOD);
		rbt.keyRelease(KeyEvent.VK_PERIOD);
		rbt.keyPress(KeyEvent.VK_2);
		rbt.keyRelease(KeyEvent.VK_2);
		Sleeper.sleepTightInSeconds(1);
		
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		Sleeper.sleepTightInSeconds(1);
		rbt.keyPress(KeyEvent.VK_TAB);
		rbt.keyRelease(KeyEvent.VK_TAB);
		Sleeper.sleepTightInSeconds(1);
		rbt.keyPress(KeyEvent.VK_ENTER);
		rbt.keyRelease(KeyEvent.VK_ENTER);
	}
}
