package com.afqa.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Admin_Login_Page {
	
	@FindBy(id="Username")
	WebElement AdminUname;
	@FindBy(id="Password")
	WebElement AdminPword;
	@FindBy(className="formsubmit")
	WebElement LoginBtn;
	@FindBy(xpath="html/body/div[1]/div[1]/div/ul/li/a/img")
	WebElement HomeBtn;
	
	public void AdminLogin()
	{
		AdminUname.sendKeys("ucshekar");
		AdminPword.sendKeys("FiNext@123");
		LoginBtn.click();
	}
}
