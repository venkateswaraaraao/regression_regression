package com.afqa.page;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Online_DocUpload_Page {
	
	static Logger Log = Logger.getLogger(Online_DocUpload_Page.class.getName());
	
	@FindBy(className="ebvInstantButton")
	WebElement EBVyellowBtn;
	
	@FindBy(className="VerifyBankFromDocScreen")
	WebElement BankEBVVerifyBtn;
	@FindBy(className="VerifyPhoneFromDocScreen")
	WebElement PhoneVerifyBtn;
	@FindBy(className="VerifyEmailFromDocScreen")
	WebElement EmailVerifyBtn;
	
	@FindBy(xpath="html/body/div[4]/div[1]/div/div[2]/div/div[2]/div/form/div[2]/div[5]/div/div[2]/div[1]/div[3]/label[2]")
	WebElement ProofOFBankAcc2;
	@FindBy(className="VerifyBankFromDocScreen")
	WebElement E_ProofOFBankAcc;
	@FindBy(id="Mdl_Documents_2__Location")
	WebElement IncmProfBTN;
	@FindBy(id="chatBox")
	WebElement initialChatBox;
	@FindBy(id="DocumentDetails")
	public WebElement FinishBTN;
	
	@FindBy(xpath="html/body/header/div[3]/a[2]/img")
	WebElement LogoutBTN;
	@FindBy(xpath="html/body/div[3]/div[1]/div/div[2]/div/div[2]/div/form/div[2]/div[1]/span")
	WebElement AppAmountBar;
	@FindBy(xpath="html/body/div[3]/div[1]/div/div[2]/div/div[2]/div/form/div[2]/div[1]/span[2]")
	WebElement AppAmountBar2;
	@FindBy(id="globalPopup")
	WebElement Popup;
	@FindBy(className="confirmVBTInputField")
	WebElement VBTInput;
	@FindBy(className="popupDialogYes")
	WebElement VBTConfirmBtn;
	@FindBy(className="popupDialogNo")
	WebElement VBTSkipBtn;
	
	public void hadleChatBox()
	{
		initialChatBox.click();
		Sleeper.sleepTightInSeconds(1);
		initialChatBox.click();
		Sleeper.sleepTightInSeconds(1);
		initialChatBox.click();
	}
	
	public void docUpload(Row r,WebDriver driver) throws AWTException
	{
		String idOne="Mdl_Documents_";
		String idTwo="__Location";
		int noOfDocs=5;
		
		if(noOfDocs<1)
		{
			Log.info("No Document is going to upload");
		}
		else
		{
			Robot rbt = new Robot();		
			for(int k=0;k<noOfDocs;k++)
			{
				try
				{
					WebElement BrowseBtn=driver.findElement(By.id(idOne+k+idTwo));
					
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", BrowseBtn);
					BrowseBtn.click();
			
					StringSelection ss = new StringSelection("E:\\UploadImages\\img-"+k+".jpg");
					Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
					
					Sleeper.sleepTightInSeconds(2);
					rbt.keyPress(KeyEvent.VK_CONTROL);
					rbt.keyPress(KeyEvent.VK_V);
					rbt.keyRelease(KeyEvent.VK_CONTROL);
					rbt.keyRelease(KeyEvent.VK_V);
					Sleeper.sleepTightInSeconds(2);
					rbt.keyPress(KeyEvent.VK_ENTER);
					rbt.keyRelease(KeyEvent.VK_ENTER);
					Sleeper.sleepTightInSeconds(2);
				}
				catch(Exception e)
				{
					Log.info("No presence of Element: Unable to look into Browse Button of "+ k);
				}
			}
		}
	}
	
	public void OldDocumentUpload(Row r, WebDriverWait wait) throws AWTException, InterruptedException
	{
		String NeedDocUpload=r.getCell(7).getStringCellValue();
		if(NeedDocUpload.equals("Yes"))
		{
			StringSelection ss = new StringSelection("D:\\UploadImages\\img.jpg");
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
			Sleeper.sleepTightInSeconds(2);
			
			Sleeper.sleepTightInSeconds(2);
			Robot rbt = new Robot();		
			rbt.keyPress(KeyEvent.VK_CONTROL);
			rbt.keyPress(KeyEvent.VK_V);
			rbt.keyRelease(KeyEvent.VK_CONTROL);
			rbt.keyRelease(KeyEvent.VK_V);
			Sleeper.sleepTightInSeconds(2);
			rbt.keyPress(KeyEvent.VK_ENTER);
			rbt.keyRelease(KeyEvent.VK_ENTER);
			Sleeper.sleepTightInSeconds(2);
			
			try{
			}
			catch(Exception e){
				ProofOFBankAcc2.click();
			}
			Sleeper.sleepTightInSeconds(2);
			rbt.keyPress(KeyEvent.VK_CONTROL);
			rbt.keyPress(KeyEvent.VK_V);
			rbt.keyRelease(KeyEvent.VK_CONTROL);
			rbt.keyRelease(KeyEvent.VK_V);
			Sleeper.sleepTightInSeconds(2);
			rbt.keyPress(KeyEvent.VK_ENTER);
			rbt.keyRelease(KeyEvent.VK_ENTER);
			Sleeper.sleepTightInSeconds(2);
			
			IncmProfBTN.click();
			Sleeper.sleepTightInSeconds(2);
			rbt.keyPress(KeyEvent.VK_CONTROL);
			rbt.keyPress(KeyEvent.VK_V);
			rbt.keyRelease(KeyEvent.VK_CONTROL);
			rbt.keyRelease(KeyEvent.VK_V);
			Sleeper.sleepTightInSeconds(2);
			rbt.keyPress(KeyEvent.VK_ENTER);
			rbt.keyRelease(KeyEvent.VK_ENTER);
			
			Log.info("Document Upload is Done");
			String AppAmountBarText=AppAmountBar.getText();
			String AppAmount = AppAmountBarText.substring(AppAmountBarText.length() - 9);
			Log.info("Customer Approval Amount is "+AppAmount);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		}
		else
		{
			Log.info("Document upload Skipped");
			String AppAmountBarText=AppAmountBar.getText();
			String AppAmount = AppAmountBarText.substring(AppAmountBarText.length() - 9);
			Log.info("Customer Approval Amount is "+AppAmount);
		}	
	}
	
	public void docFinish(WebDriver driver){
		JavascriptExecutor jse=(JavascriptExecutor) driver;
		jse.executeScript("arguments[0].scrollIntoView();", FinishBTN);
		Sleeper.sleepTightInSeconds(2);
		FinishBTN.click();
	}

	public void clickShowAppAmount()
	{
		EBVyellowBtn.click();
	}
}
