package com.afqa.page;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Online_RequestAdvance_Page
{
	@FindBy(className="RequestCashAdvance")
	WebElement RequestOpn;
	
	@FindBy(id="AdvanceOrDrawRequested")
	WebElement advance;
	
	@FindBy(name="IsInGoDraw")
	WebElement DepositToAcc;
	
	@FindBy(className="drawSubmitbutton")
	WebElement submitBtn;
	
	@FindBy(className="popupDialogYes")
	WebElement confirmBtn;
	
	public void AdvanceDetails(WebDriverWait wait)
	{
		RequestOpn.click();
		
		advance.click();
		advance.sendKeys("10");
		Sleeper.sleepTightInSeconds(2);
		
		DepositToAcc.click();
		Sleeper.sleepTightInSeconds(2);
		DepositToAcc.sendKeys(Keys.ARROW_DOWN);
		Sleeper.sleepTightInSeconds(2);
		DepositToAcc.sendKeys(Keys.ARROW_DOWN);
		Sleeper.sleepTightInSeconds(3);
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		submitBtn.click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Confirm")));
		Sleeper.sleepTightInSeconds(2);
		confirmBtn.click();
	}
}
