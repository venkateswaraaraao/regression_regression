package com.afqa.page;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Online_Video_Page {
	
	static Logger Log = Logger.getLogger(Online_Video_Page.class.getName());
	
	@FindBy(id="header_grayDiv")
	WebElement GrayHeaderBar;
	@FindBy(xpath="//*[@id=\'sticky-wrapper\']/header/div[3]/a[2]/img")
	WebElement LogoutBtn;
	
	public void VideoPageCheck(WebDriverWait wait)
	{
		String Welcome=GrayHeaderBar.getText();
		if(Welcome.contains("Welcome"))
		{
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("popupProgressDialogMask")));
			LogoutBtn.click();
			Log.info("Logged Out from Online portal");
			Log.info("-----------------------------------------------------------------------");
		}
		else
		{
			Log.info("Video page not displayed");
			Log.info("-----------------------------------------------------------------------");
		}
	}
}
