package com.afqa.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Store_Login_Page {
	
	@FindBy(id="Username")
	WebElement StoreUname;
	@FindBy(id="Password")
	WebElement StorePword;
	@FindBy(className="formsubmit")
	WebElement LoginBtn;
	@FindBy(xpath="html/body/div[1]/div[1]/div/ul/li/a/img")
	WebElement HomeBtn;
		
	public void StoreLogin()
	{
		StoreUname.sendKeys("ucshekar");
		StorePword.sendKeys("FiNext@123");
		LoginBtn.click();
	}
}
