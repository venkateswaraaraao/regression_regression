package com.afqa.page;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Store_CxRegister_Page
{
	static Logger Log = Logger.getLogger("Store_Register_Page");
	
	Constants_Page pageCon=null;
	
	public Store_CxRegister_Page(WebDriver driver)
	{
		pageCon=PageFactory.initElements(driver,Constants_Page.class);
	}
	
	@FindBy(id="SelectedProductDetails_0__Selected")
	WebElement UnsecLOCChkBox;
	@FindBy(id="SelectedProductDetails_1__Selected")
	WebElement MiscChkBox;
	@FindBy(id="SelectedProductDetails_2__Selected")
	WebElement NetSpendChkBox;
	@FindBy(id="SelectedProductDetails_3__Selected")
	WebElement secLOCChkBox;
	
	@FindBy(id="PersonalDetails_SSN")
	WebElement SSN;
	@FindBy(id="CustomerIdentity_IdentityNumber")
	WebElement IDNumber;
	@FindBy(id="CustomerIdentity_LTIdentityTypeCodeID")
	WebElement IDType;
	@FindBy(id="CustomerIdentity_StateID")
	WebElement IDState;
	@FindBy(id="CustomerIdentity_IssueDate")
	WebElement IDIssuedDate;
	@FindBy(xpath="html/body/div[5]")
	WebElement IDIssuedCal;
	@FindBy(id="CustomerIdentity_ExpiryDate")
	WebElement IDExpiryDate;
	@FindBy(xpath="html/body/div[6]")
	WebElement IDExpiryCal;
	
	@FindBy(id="PersonalDetails_FirstName")
	WebElement FName;
	@FindBy(id="PersonalDetails_MiddleName")
	WebElement MName;
	@FindBy(id="PersonalDetails_LastName")
	WebElement LName;
	@FindBy(id="CustomerAddress_AddressLine1")
	WebElement AddressOne;
	@FindBy(id="CustomerAddress_AddressLine2")
	WebElement AddressLine2;
	@FindBy(id="PersonalDetails_IsActiveMilitary")
	WebElement ActiveMilitary;
	@FindBy(className="CustomerState")
	WebElement StateName;
	@FindBy(className="CustomerCity")
	WebElement CityName;
	@FindBy(id="ZipCode")
	WebElement ZipID;
	@FindBy(id="CustomerPhone_PhoneNumber")
	WebElement PrimaryPhone;
	@FindBy(id="CustomerPhone_SecondaryPhone")
	WebElement SecondaryPhone;
	@FindBy(id="CustomerPhone_DNCMessage")
	WebElement PhoneConsent;
	@FindBy(id="CustomerPhone_DNCMessage")
	WebElement verifyPhone;
	@FindBy(className="verifyCustomerPhoneVBT")
	WebElement verifyPhoneLink;
	@FindBy(className="btn_close")
	WebElement verifyPopClose;
	@FindBy(className="confirmVBTInput")
	WebElement PINtxtbox;
	@FindBy(className="confirmCustomerVerification")
	WebElement PINConfirmBtn;
	@FindBy(id="PersonalDetails_Email")
	public WebElement CxEmail;
	@FindBy(id="PersonalDetails_DNCEmail")
	public WebElement RecieveEMail;
	@FindBy(id="CustomerInsight_LTSecurityQuestionID")
	WebElement secQuestion;
	@FindBy(id="CustomerInsight_SecurityAnswer")
	WebElement secAnswer;
	
	@FindBy(name="PersonalDetails.DOB")
	WebElement DOB;
	@FindBy(xpath="html/body/div[7]")
	WebElement DOBCal;
	@FindBy(id="PersonalDetails_CustomerComments")
	WebElement CommentsBox;
	@FindBy(className="xdsoft_prev")
	WebElement Cal_Prev_Btn;
	@FindBy(xpath="html/body/div[7]/div[1]/div[2]/table/tbody/tr[2]/td[1]")
	WebElement Cal_Date;
	
	@FindBy(id="IncomeDetails_LTSourceOfIncomeID")
	WebElement EmpTypeDD;
	@FindBy(id="IncomeDetails_EmployerName")
	WebElement CompanyName;
	@FindBy(id="IncomeDetails_EmployerAddressLine1")
	WebElement EmpAdd1;
	@FindBy(id="IncomeDetails_StateID")
	WebElement EmpStateID;
	@FindBy(id="IncomeDetails_CityID")
	WebElement EmpCity;
	@FindBy(id="IncomeZipCode")
	WebElement EmpZip;
	@FindBy(id="IncomeDetails_EmployerPhoneNumber")
	WebElement EmpPhone;
	public @FindBy(id="IncomeDetails_HireDate")
	WebElement HireDate;
	@FindBy(xpath="html/body/div[8]")
	WebElement HireDateCal;
	public @FindBy(id="IncomeDetails_GrossIncomeAmount")
	WebElement Income;
	public @FindBy(id="IncomeType")
	WebElement IncomeType;
	@FindBy(id="IncomeDetails_LTPayFrequencyID")
	WebElement PayFrequency;
	@FindBy(id="IncomeDetails_DayOfWeek")
	WebElement Week_Bi_DayOfWeek;
	@FindBy(id="PayDate")
	WebElement Week_NextPayDay;
	@FindBy(id="radnextpayday")
	WebElement Bi_NextPayDay1;
	@FindBy(id="radsubsequentpaydate")
	WebElement Bi_NextPayDay2;
	@FindBy(id="firstCaseradio")
	WebElement SemiM_SpecDates;
	@FindBy(id="secondCaseradio")
	WebElement SemiM_SpecDays;
	@FindBy(id="IncomeDetails_DateOfMonth1")
	WebElement SemiM_DateofMonth1;
	@FindBy(id="IncomeDetails_DateOfMonth2")
	WebElement SemiM_DateofMonth2;
	@FindBy(id="IncomeDetails_WeekOfMonth1")
	WebElement SemiM_WeekofMonth1;
	@FindBy(id="IncomeDetails_DayOfWeek1")
	WebElement SemiM_DayofWeek1;
	@FindBy(id="IncomeDetails_WeekOfMonth2")
	WebElement SemiM_WeekofMonth2;
	@FindBy(id="IncomeDetails_DayOfWeek2")
	WebElement SemiM_DayofWeek2;
	@FindBy(id="firstCaseradio")
	WebElement Monthly_SpecDate;
	@FindBy(id="secondCaseradio")
	WebElement Monthly_SpecDay;
	@FindBy(id="IncomeDetails_DateOfMonth1")
	WebElement Monthly_DateOfMonth;
	@FindBy(id="IncomeDetails_WeekOfMonth")
	WebElement Weekly_WeekOfMonth;
	@FindBy(id="IncomeDetails_DayOfWeek")
	WebElement Weekly_MonthDayOfWeek;
	@FindBy(id="CustomerBanks_0__AccountTypeID")
	WebElement AccountType;
	@FindBy(id="BankRoutingCnfrm")
	WebElement RoutingNumber;
	@FindBy(className="RoutingNo")
	WebElement ConfirmRouting;
	@FindBy(id="CustomerBanks_0__BankName")
	WebElement BankName;
	@FindBy(id="AccntNoCnfrm")
	WebElement AccountNumber;
	@FindBy(className="AccNo")
	WebElement ConfirmAccountNum;
	@FindBy(id="CustomerBanks_0__IsDirectDeposit")
	WebElement DirectDepositCheck;
	@FindBy(id="verifyBank")
	WebElement BankVerify;
	@FindBy(linkText="Yes")
	WebElement BankVerifyYesBtn;
	@FindBy(linkText="Use keyboard")
	WebElement verifyUseKeyboard;
	@FindBy(className="clscheckboxselection")
	WebElement CardPrimary;
	@FindBy(id="CreditCardDetailsNmbrChk")
	WebElement CardNumber;
	@FindBy(id="CustomerCards_0__CardIssuer")
	WebElement CardIssuer;
	@FindBy(id="CreditCardDetailsNameChk")
	WebElement CardHolderName;
	@FindBy(id="CreditCardDetailsCvvChk")
	WebElement CardCVV;
	@FindBy(id="CustomerCards_0__debitCardExpiryDt")
	WebElement CardExpiryDate;
	@FindBy(className="clsIsSameAsMailingAddress")
	WebElement checkIsSameMailAdd;
	@FindBy(className="clsbillingAddr1")
	WebElement CardBillingAdd1;
	@FindBy(id="CustomerCards_0__BillingAddress2")
	WebElement CardBillingAdd2;
	@FindBy(className="clsbillingState")
	WebElement CardState;
	@FindBy(className="clsbillingCity")
	WebElement CardCity;
	
	@FindBy(id="CustomerCards_0__ZipCode")
	WebElement BillingZip;

	@FindBy(xpath="//*[@id='body']/div/div[7]/form/div[2]/div/div[1]/div[1]/div/div[4]/div/div/div[1]/span[1]/img")
	WebElement docToggleIcon;
	@FindBy(className="uploadDocumentAddMore")
	WebElement DocAddMoreBtn;
	@FindBy(id="Documents_0__DocumentTypeId")
	WebElement DocTypeOne;
	@FindBy(id="Documents_0__File")
	WebElement DocBrowseOne;
	@FindBy(id="Documents_1__DocumentTypeId")
	WebElement DocTypeTwo;
	@FindBy(id="Documents_1__File")
	WebElement DocBrowseTwo;
	@FindBy(id="Documents_2__DocumentTypeId")
	WebElement DocTypeThree;
	@FindBy(id="Documents_2__File")
	WebElement DocBrowseThree;
	
	@FindBy(id="CustomerReferenceDetails_ReferenceName")
	WebElement RefName;
	@FindBy(id="CustomerReferenceDetails_CustomerRelationship")
	WebElement RelationWithCx;
	@FindBy(id="CustomerReferenceDetails_ReferencePhone")
	WebElement RefPhone;
	@FindBy(linkText="Ready To Approve")
	WebElement Ready2ApproveBtn;
	
	@FindBy(className="popupDialogHdr")
	WebElement PopupMsgHdr;
	@FindBy(xpath="//*[@id='globalPopup']/div[2]")
	WebElement PopupMessage;
	@FindBy(className="popupDialogCont")
	WebElement PopUpMsg;
	
	@FindBy(className="popupDialogErr")
	WebElement SuccessPopMSG;
	@FindBy(xpath="html/body/div[2]/div/div[4]/div[2]/ol/li")
	WebElement SuccessPopAlert;
	@FindBy(xpath="//*[@id='globalPopup']/div[2]/div/a[1]")
	WebElement PopIgnoreYesBtn;
	@FindBy(xpath="//*[@id='globalPopup']/div[2]/div/a[2]")
	WebElement PopIgnoreNoBtn;
	@FindBy(linkText="OK")
	WebElement PopOKBtn;
	
	@FindBy(linkText="Verify Now")
	WebElement VerifyNowBtn;
	
	@FindBy(className="InsightSaveBtnShow")
	WebElement SaveBtn;
	
	@FindBy(xpath="//*[@id='body']/div/div[7]/form/div[2]/div/div[1]/div[1]/div/div[9]/div/span")
	WebElement saveEditBtn;
	@FindBy(className="popupDialogCont")
	public WebElement warningMsg;
	
	public void OpenMiscCxRegister(WebDriverWait wait )
	{
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("PersonalDetails_SSN")));
		UnsecLOCChkBox.click();
		
		Sleeper.sleepTightInSeconds(2);
		MiscChkBox.click();
	}
	
	public void OpenNetSpendCxRegister(WebDriverWait wait )
	{
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("PersonalDetails_SSN")));
		
		UnsecLOCChkBox.click();
		
		Sleeper.sleepTightInSeconds(2);
		NetSpendChkBox.click();
	}
	
	public void enterInvalidSSN(WebDriverWait wait)
	{
		SSN.click();
		SSN.sendKeys("111111111");
		IDNumber.click();
		
		Sleeper.sleepTightInSeconds(4);
		String SSNPopup=PopupMessage.getText();
		Log.info(SSNPopup);
		PopOKBtn.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupDialogHdr")));
		Log.info("Exist SSN Check: Exist SSN Popup displayed");
		
		Log.info("---------------------------------------------------");
	}
	
	public void enterValidSSN(WebDriverWait wait){
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(2);
		SSN.click();
		SSN.sendKeys(pageCon.storeSSN());
		IDNumber.click();
	}

	public void enterInvalidPhotoID()
	{
		IDNumber.sendKeys("Feb195125");
		IDNumber.sendKeys(Keys.TAB);
		
		Sleeper.sleepTightInSeconds(3);
		String PhotoIDPopup=PopupMessage.getText();
		Log.info(PhotoIDPopup);
		PopOKBtn.click();
		
		Sleeper.sleepTightInSeconds(2);
		/*String PhotoIDIgnorePopup=PopupMessage.getText();
		Log.info("Exist PhotoID ignore popup Check:"+PhotoIDIgnorePopup);
		PopIgnoreNoBtn.click();*/
		
		Log.info("---------------------------------------------------");
	}

	public void enterValidPhotoID(WebDriverWait wait)
	{	
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupDialogHdr")));
		IDNumber.sendKeys("STQA"+pageCon.randNum());
		
		Sleeper.sleepTightInSeconds(2);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		new Select(IDType).selectByIndex(2);
		
		new Select(IDState).selectByVisibleText("Tennessee");
	}
	
	public void enterPhotoIDDetails(WebDriverWait wait)
	{
		new Select(IDState).selectByVisibleText("Tennessee");
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		/*IDIssuedDate.click();
		Sleeper.sleepTightInSeconds(2);
		IDIssuedCal.findElement(By.className("xdsoft_prev")).click();
		Sleeper.sleepTightInSeconds(1);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("html/body/div[5]/div[1]/div[2]/table/tbody/tr[2]/td[1]"))).click();
		
		IDIssuedDate.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.ARROW_LEFT));
		IDIssuedDate.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.ARROW_LEFT));
		IDIssuedDate.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.ARROW_LEFT));
		
		IDIssuedDate.sendKeys("12122012");
		
		IDExpiryDate.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(1);
		IDExpiryCal.findElement(By.className("xdsoft_prev")).click();
		Sleeper.sleepTightInSeconds(1);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("html/body/div[6]/div[1]/div[2]/table/tbody/tr[2]/td[1]"))).click();
		
		IDExpiryDate.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.ARROW_LEFT));
		IDExpiryDate.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.ARROW_LEFT));
		IDExpiryDate.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.ARROW_LEFT));
		
		IDExpiryDate.sendKeys("12122020");
		IDExpiryDate.sendKeys(Keys.TAB);*/
	}
	
	public void enterName(WebDriver driver)
	{
		Constants_Page pageCon=PageFactory.initElements(driver, Constants_Page.class);
		FName.sendKeys(pageCon.PartName());
		LName.sendKeys(pageCon.PartName());
	}
	
	public String stFName()
	{
		String FirstName=FName.getAttribute("value");
		return FirstName;
	}
	
	public void enterAddress(WebDriverWait wait)
	{
		AddressOne.clear();
		AddressOne.sendKeys("AddressOne");
		
		AddressLine2.sendKeys("1-877-565");
		Sleeper.sleepTightInSeconds(2);
		new Select(ActiveMilitary).selectByIndex(2);
		Sleeper.sleepTightInSeconds(3);
		new Select(StateName).selectByVisibleText("Tennessee");	
		
		Sleeper.sleepTightInSeconds(2);
		new Select(CityName).selectByIndex(5);
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(2);
		ZipID.click();
		ZipID.sendKeys("50002");
		
	}
	
	public void enterInvalidPhone(WebDriverWait wait)
	{
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		PrimaryPhone.click();
		//PrimaryPhone.clear();
		Sleeper.sleepTightInSeconds(3);
		PrimaryPhone.sendKeys("7655765677");
		PrimaryPhone.sendKeys(Keys.TAB);
		
		Sleeper.sleepTightInSeconds(2);
		Log.info(PopupMessage.getText());
		PopOKBtn.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupDialogHdr")));
		Log.info("Exist Phone Check: Exist Phone Popup displayed");
		
		Log.info("---------------------------------------------------");
	}

	public void enterValidPhone()
	{
		PrimaryPhone.clear();
		PrimaryPhone.sendKeys(pageCon.onlinePhNumber());
		//PhoneConsent.click();
	}
	
	public void verifyPopClose()
	{
		verifyPopClose.click();
	}
	
	public void enterInvalidEmail(WebDriverWait wait)
	{
		CxEmail.sendKeys("opp@test3.com");
		CxEmail.sendKeys(Keys.TAB);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("OK")));
		Sleeper.sleepTightInSeconds(2);
		Log.info(PopupMessage.getText());
		PopOKBtn.click();
	}	
	
	public void enterValidEmail(WebDriverWait wait, Row r)
	{
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupDialogHdr")));
		CxEmail.clear();
		CxEmail.sendKeys(pageCon.storeEmail(r));
		CxEmail.sendKeys(Keys.TAB);
	}	
	
	public void selectReceiveMail(WebDriverWait wait)
	{
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		new Select(RecieveEMail).selectByIndex(1);

	}
	
	public void enterSecurityDetails(){
		new Select(secQuestion).selectByIndex(2);
		secAnswer.sendKeys("10044");
	}
	
	public void enterDOB(WebDriverWait wait)
	{
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		DOB.click();
		Sleeper.sleepTightInSeconds(3);
		/*wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		DOBCal.findElement(By.className("xdsoft_prev")).click();
		Sleeper.sleepTightInSeconds(1);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("html/body/div[7]/div[1]/div[2]/table/tbody/tr[2]/td[1]"))).click();*/
		
		DOB.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.ARROW_LEFT));
		DOB.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.ARROW_LEFT));
		DOB.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.ARROW_LEFT));
		
		DOB.sendKeys("12121985");
		MName.click();
	}
	
	public void DOB3DataCheck(WebDriverWait wait)
	{
		Calendar now = Calendar.getInstance();
		now.add(Calendar.YEAR, -18);
		now.add(Calendar.DATE, 1);
		Date date=now.getTime();
		SimpleDateFormat format1 = new SimpleDateFormat("MM-dd-yyyy");
		String ZinvalidDOB=format1.format(date);
		String invalidDOB=ZinvalidDOB.replace("-", "");
	   
		now = Calendar.getInstance();
		now.add(Calendar.YEAR, -18);
		date=now.getTime();
		format1 = new SimpleDateFormat("MM-dd-yyyy");
		String ZvalidDOB=format1.format(date);
		String validDOB=ZvalidDOB.replace("-", "");
	   
		now = Calendar.getInstance();
		now.add(Calendar.YEAR, -18);
		now.add(Calendar.DATE, -1);
		date=now.getTime();
		format1 = new SimpleDateFormat("MM-dd-yyyy");
		String ZvalidDOB2=format1.format(date);
		String validDOB2=ZvalidDOB2.replace("-", "");
		
		String[] DOBDate={invalidDOB,validDOB,validDOB2};
		for(int i=0;i<2;i++)
		{
			Sleeper.sleepTightInSeconds(3);
			DOB.click();
			Sleeper.sleepTightInSeconds(1);
			DOBCal.findElement(By.className("xdsoft_prev")).click();
			Sleeper.sleepTightInSeconds(1);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("html/body/div[7]/div[1]/div[2]/table/tbody/tr[2]/td[1]"))).click();
			
			DOB.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.ARROW_LEFT));
			DOB.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.ARROW_LEFT));
			DOB.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.ARROW_LEFT));
			
			DOB.sendKeys(DOBDate[i]);
			System.out.println();
			CommentsBox.click();
			Sleeper.sleepTightInSeconds(2);
			
			try
			{
				Log.info("DOB Check: "+DOBDate[i]+": "+PopupMessage.getText());
				PopOKBtn.click();
			}
			catch(Exception e)
			{
				Log.info("DOB Check: "+DOBDate[i]+": Popup not displayed");
			}
	    }
	}
	
	public String stEmail()
	{
		String eMail=CxEmail.getAttribute("value");
		return eMail;
	}
	
	public String stPhone(){
		String ePhone=PrimaryPhone.getAttribute("value");
		String ePhoneNum=ePhone.replace("-", "");
		return ePhoneNum;
	}
	
	public void enterEmpName()
	{
		CompanyName.sendKeys("ABC Systems");
	}
	
	public void enterAddressCityZip(WebDriverWait wait)
	{
		EmpAdd1.sendKeys("11 Circle Street");
		new Select(EmpCity).selectByVisibleText("Knoxville");
		Sleeper.sleepTightInSeconds(2);
		EmpZip.click();
		EmpZip.sendKeys("50243");
	}
	
	public void enterEmpPhone()
	{
		EmpPhone.clear();
		EmpPhone.sendKeys("5000022891");
	}
	
	public void enterIncomeStartDate(WebDriverWait wait)
	{
		HireDate.click();
		Sleeper.sleepTightInSeconds(1);
		/*HireDateCal.findElement(By.className("xdsoft_prev")).click();
		Sleeper.sleepTightInSeconds(1);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("html/body/div[8]/div[1]/div[2]/table/tbody/tr[2]/td[1]"))).click();
		*/HireDate.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.ARROW_LEFT));
		HireDate.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.ARROW_LEFT));
		HireDate.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.ARROW_LEFT));
		HireDate.sendKeys("12122012");
	}
	
	public void enterIncome(Row r)
	{
		Income.sendKeys(r.getCell(2).getStringCellValue());
	}
	
	public void selectIncomeType(Row r)
	{
		String CxIncomeType=r.getCell(3).getStringCellValue();
		if(CxIncomeType.equals("Gross"))
		{
			new Select(IncomeType).selectByIndex(1);
		}
		else
		{
			new Select(IncomeType).selectByIndex(2);
		}
	}
	
	public void IncomeDetails(Row r, WebDriverWait wait)
	{
		String EmpType=r.getCell(7).getStringCellValue();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(2);
		

		if(EmpType.equals("1"))
		{
			new Select(EmpTypeDD).selectByIndex(1);
			Sleeper.sleepTightInSeconds(2);
			enterEmpName();
			
			enterIncome(r);
			enterIncomeStartDate(wait);
			//selectIncomeType(r);
		}
		else
		{
			if(EmpType.equals("2"))
			{
				enterEmpName();
				enterIncome(r);
				//selectIncomeType(r);
			}
			else
			{
				if(EmpType.equals("3"))
				{
					enterIncomeStartDate(wait);
					enterIncome(r);
				}
				else
				{
					enterEmpName();
					//enterIncomeStartDate(wait);
					enterIncome(r);
				}
			}
		}
	}
	
	public void IncomeDetailsForNOAA(Row r, WebDriverWait wait)
	{
		String EmpType=r.getCell(7).getStringCellValue();
		if(EmpType.equals("1"))
		{
			enterEmpName();
			
			Income.sendKeys("100");
			enterIncomeStartDate(wait);
			//selectIncomeType(r);
		}
		else
		{
			if(EmpType.equals("2"))
			{
				enterEmpName();
				Income.sendKeys("100");
				//selectIncomeType(r);
			}
			else
			{
				if(EmpType.equals("3"))
				{
					enterIncomeStartDate(wait);
					Income.sendKeys("100");
				}
				else
				{
					enterEmpName();
					Income.sendKeys("100");
				}
			}
		}
	}
	public void FrequencyDetails(Row r, WebDriverWait wait)
	{
		String PayFreq=r.getCell(4).getStringCellValue();
		String WeekDay=r.getCell(5).getStringCellValue();
		String DayOrDate=r.getCell(6).getStringCellValue();
		if(PayFreq.equals("1"))
		{
			new Select(PayFrequency).selectByIndex(1);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("IncomeDetails_DayOfWeek")));
			new Select(Week_Bi_DayOfWeek).selectByIndex(Integer.parseInt(WeekDay));
		}
		else
		{
			if(PayFreq.equals("2"))
			{
				new Select(PayFrequency).selectByIndex(2);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("IncomeDetails_DayOfWeek")));
				new Select(Week_Bi_DayOfWeek).selectByIndex(Integer.parseInt(WeekDay));
				String NextPayDay=r.getCell(7).getStringCellValue();
				if(NextPayDay.equals("1"))
				{
					Bi_NextPayDay1.click();
				}
				else
				{
					Bi_NextPayDay2.click();
				}
			}
			else
			{
				if(PayFreq.equals("3"))
				{
					new Select(PayFrequency).selectByIndex(3);
					if(DayOrDate.equals("Day"))
					{
						SemiM_SpecDays.click();
						new Select(SemiM_WeekofMonth1).selectByVisibleText("First");
						new Select(SemiM_DayofWeek1).selectByVisibleText("Monday");
						new Select(SemiM_WeekofMonth2).selectByVisibleText("Third");
						new Select(SemiM_DayofWeek2).selectByVisibleText("Monday");
					}
					else
					{
						SemiM_SpecDates.click();
						new Select(SemiM_DateofMonth1).selectByIndex(5);
						new Select(SemiM_DateofMonth2).selectByIndex(7);
					}
				}
				else
				{
					if(PayFreq.equals("4"))
					{
						new Select(PayFrequency).selectByIndex(4);
						if(DayOrDate.equals("Day"))
						{
							Monthly_SpecDay.click();
							new Select(Weekly_WeekOfMonth).selectByIndex(1);
							new Select(Weekly_MonthDayOfWeek).selectByIndex(3);
						}
						else
						{
							Monthly_SpecDate.click();
							new Select(Monthly_DateOfMonth).selectByIndex(10);
						}
					}
				}
			}
		}
		BankName.click();
	}
	
	public void BankDetails(Row r, WebDriver driver, WebDriverWait wait)
	{
		String AccType = r.getCell(8).getStringCellValue();
		String IsdirectDeposit=r.getCell(9).getStringCellValue();
		if(AccType.equals("1"))
		{
			new Select(AccountType).selectByIndex(1);
			
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,250)", "");
			
			RoutingNumber.click();
			RoutingNumber.sendKeys("999988181");
			/*ConfirmRouting.click();
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
			ConfirmRouting.sendKeys("999988181");
			*/BankName.click();
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
			BankName.sendKeys("Bank of America");
			String AccNbr="3000"+pageCon.randNum();
			AccountNumber.sendKeys(AccNbr);
			ConfirmAccountNum.click();
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
			ConfirmAccountNum.sendKeys(AccNbr);
			BankName.click();
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
			if(IsdirectDeposit.equals("Yes"))
			{
				new Select(DirectDepositCheck).selectByIndex(1);
			}
			else
			{
				new Select(DirectDepositCheck).selectByIndex(2);
			}
		}
		else
		{
			if(AccType.equals("2"))
			{
				new Select(AccountType).selectByIndex(2);
				Log.info("Choosen No Account");
			}
			else
			{
				if(AccType.equals("3"))
				{
					new Select(AccountType).selectByIndex(3);
					RoutingNumber.click();
					RoutingNumber.sendKeys("999999963");
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
					ConfirmRouting.click();
					ConfirmRouting.sendKeys("999999963");
					BankName.click();
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
					BankName.sendKeys("Bank of America");
					String AccNbr="3000"+pageCon.randNum();
					AccountNumber.sendKeys(AccNbr);
					ConfirmAccountNum.click();
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
					ConfirmAccountNum.sendKeys(AccNbr);
					BankName.click();
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
					if(IsdirectDeposit.equals("Yes"))
					{
						new Select(DirectDepositCheck).selectByIndex(1);
					}
					else
					{
						new Select(DirectDepositCheck).selectByIndex(2);
					}
				}
				else
				{
					if(AccType.equals("4"))
					{
						new Select(AccountType).selectByIndex(4);
						RoutingNumber.click();
						RoutingNumber.sendKeys("999999963");
						wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
						ConfirmRouting.click();
						ConfirmRouting.sendKeys("999999963");
						BankName.click();
						wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
						BankName.sendKeys("Bank of America");
						String AccNbr="3000"+pageCon.randNum();
						AccountNumber.sendKeys(AccNbr);
						ConfirmAccountNum.click();
						wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
						ConfirmAccountNum.sendKeys(AccNbr);
						BankName.click();
						wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
						if(IsdirectDeposit.equals("Yes"))
						{
							new Select(DirectDepositCheck).selectByIndex(1);
						}
						else
						{
							new Select(DirectDepositCheck).selectByIndex(2);
						}
					}
					else
					{
						new Select(AccountType).selectByIndex(5);
						RoutingNumber.click();
						RoutingNumber.sendKeys("999999963");
						wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
						/*ConfirmRouting.click();
						ConfirmRouting.sendKeys("999999963");
						*/BankName.click();
						wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
						BankName.sendKeys("Bank of America");
						String AccNbr="3000"+pageCon.randNum();
						AccountNumber.sendKeys(AccNbr);
						ConfirmAccountNum.click();
						wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
						ConfirmAccountNum.sendKeys(AccNbr);
						BankName.click();
						wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
						if(IsdirectDeposit.equals("Yes"))
						{
							new Select(DirectDepositCheck).selectByIndex(1);
						}
						else
						{
							new Select(DirectDepositCheck).selectByIndex(2);
						}
					}
				}
			}
		}
	}
	
	public void CardDetails(WebDriver driver, WebDriverWait wait)
	{
		Actions action=new Actions(driver);
		action.moveToElement(CardNumber).build().perform();
		Sleeper.sleepTightInSeconds(1);
		action.moveToElement(CardPrimary).click().build().perform();

		/*String CardNum=pageCon.cardNumber();
		CardNumber.sendKeys(CardNum);*/
		CardNumber.sendKeys("5105105105105100");
		CardHolderName.sendKeys("Test");
		CardCVV.sendKeys("123");
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(2);
		CardExpiryDate.click();
		CardExpiryDate.sendKeys("1220");
		Sleeper.sleepTightInSeconds(2);
		
		checkIsSameMailAdd.click();
		/*CardBillingAdd1.sendKeys("Lane One");
		
		CardBillingAdd2.sendKeys("Lane 2");
		
		new Select(CardState).selectByVisibleText("Tennessee");
		Sleeper.sleepTightInSeconds(5);
		
		new Select(CardCity).selectByIndex(3);
		
		BillingZip.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		BillingZip.sendKeys("57075");*/
		
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Log.info("Card Details Entered");
		
		String CardIssuerName=CardIssuer.getAttribute("value");
		if(CardIssuerName.isEmpty())
		{
			CardIssuer.sendKeys("BOA");
		}
	}
	
	public void uploadDocs(Row r, WebDriver driver) throws AWTException
	{
		String idOne="Documents_";
		String idTwo="__File";
		
		String DocidOne="Documents_";
		String DocidTwo="__DocumentTypeId";
		
		Robot rbt = new Robot();		
		
		int noOfDocs=(int) r.getCell(10).getNumericCellValue();
		if(noOfDocs==0)
		{
			System.out.println("No Document is going to upload");
			Log.info("No Document is going to upload");
		}
		else
		{
			for(int k=0;k<noOfDocs;k++)
			{
				try
				{
					WebElement BrowseBtn=driver.findElement(By.id(idOne+k+idTwo));
					WebElement DocTypeDD=driver.findElement(By.id(DocidOne+k+DocidTwo));
					DocAddMoreBtn.click();
					Sleeper.sleepTightInSeconds(2);
					if(k==2)
					{
						new Select(DocTypeDD).selectByIndex(8);
					}
					else
					{
						new Select(DocTypeDD).selectByIndex(k+2);
					}	
					
					Sleeper.sleepTightInSeconds(2);
					BrowseBtn.click();
					
					StringSelection ss = new StringSelection("E:\\UploadImages\\img-"+k+".jpg");
					Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
					
					Sleeper.sleepTightInSeconds(2);
					rbt.keyPress(KeyEvent.VK_CONTROL);
					rbt.keyPress(KeyEvent.VK_V);
					rbt.keyRelease(KeyEvent.VK_CONTROL);
					rbt.keyRelease(KeyEvent.VK_V);
					Sleeper.sleepTightInSeconds(2);
					rbt.keyPress(KeyEvent.VK_ENTER);
					rbt.keyRelease(KeyEvent.VK_ENTER);
					Sleeper.sleepTightInSeconds(2);
			
				}
				catch(Exception e)
				{
					Log.info("No presence of Element: Unable to look into Browse Button of "+ k);
				}
			}
		}
	}
	
	public void RefDetailsToEnd()
	{
		RefName.sendKeys("Charlotte");
		new Select(RelationWithCx).selectByIndex(3);
		RefPhone.click();
		RefPhone.sendKeys("5642358890");
	}
	
	public void ReadyToApprove()
	{
		Ready2ApproveBtn.click();
	}
	
	public void SaveCustomer()
	{
		SaveBtn.click();
	}
	
	public void SaveEditCustomer(WebDriver driver)
	{
		saveEditBtn.click();
	}
	
	public void validation()
	{
	}
	
	public void RegisterConfirmSuccess()
	{
		try
		{
			String PopMSG=PopUpMsg.getText();
			if (PopMSG.contains("successfully"))
			{
				Log.info("Popup dispayed Successful Message");
			}
			else
			{
				Log.info("Popup dispayed Failed Message");
			}
			PopOKBtn.click();
		}
		catch(Exception e)
		{
			String errorInfo=e.getMessage();
			
			if(errorInfo.contains("Cannot Focus"))
			{
				Log.info("Failed due to Cannot Focus Element");
			}
			if(errorInfo.contains("Timed out"))
			{
				Log.info("Failed due to Timed Out Exception");
			}
			if(errorInfo.contains("no such element"))
			{
				Log.info("Failed due to No Such Element Exception");
			}
		}	
	}
	
	public void VerifyNow(WebDriverWait wait)
	{
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(2);
		BankVerify.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Yes")));
		//wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupDialogMask")));
		Sleeper.sleepTightInSeconds(2);
		BankVerifyYesBtn.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(3);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Use keyboard")));
		verifyUseKeyboard.click();
	}
	
	public void VerifyEmail(WebDriverWait wait)
	{
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		VerifyNowBtn.click();
	}
	
	public void EmailVerifyPopup()
	{
		try
		{
			String PopMSG=PopupMessage.getText();
			Log.info(PopMSG);
			PopOKBtn.click();
		}
		catch(Exception e)
		{
			String PopInfo=PopupMessage.getText();
			Log.info(PopInfo);
		}
	}
}
