package com.afqa.page;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.apache.log4j.Logger;

public class Store_Payment_Page {
static Logger Log = Logger.getLogger(Online_Registration_Page.class.getName());
	
	Constants_Page pageCon=null;
	
	public Store_Payment_Page(WebDriver driver)
	{
		 pageCon=PageFactory.initElements(driver, Constants_Page.class);
	}
	
	
	
	@FindBy(className="moreButton")
	WebElement moreBtn;
	
	@FindBy(id="paymentLnkBtn")
	WebElement paymentBtn;

	@FindBy(name="rdPayType")
	WebElement payOtherRButton;
	
	@FindBy(id="otherradio")
	WebElement principalRed;
	
	@FindBy(className="customerPaymentOtherBalance")
	WebElement Principalamt;
	
	@FindBy(id="LOCs_MinPaymentDue")
	WebElement minDueAmt;
	
	@FindBy(className="lOCPaymentMonetaryCodeClass")
	WebElement payBy;
	
	@FindBy(id="currbalradio")
	WebElement payOff;
	
	@FindBy(id="PayOffAmount")
	WebElement payOffAmount;
	
	@FindBy(className="tenderAmt")
	WebElement tenderAmt;
	
	@FindBy(className="CustomerPaymentTenderAmount")
	WebElement cardTenderAmt;
	
	@FindBy(className="customerCardsList")
	public WebElement selectCard;
	
	@FindBy(className="CCLastFourDigits")
	WebElement cardNum;
	
	@FindBy(className="NameOnTheDebitCard")
	public WebElement nameOnCard;
	
	@FindBy(id="MonetaryCodelistPartialView_0__CheckCardHolderName")
	WebElement ExistCardNameOnCard;
	
	@FindBy(id="MonetaryCodelistPartialView_0__CardIssuer")
	WebElement cardIssuer;
	
	@FindBy(className="debitCardCVV")
	WebElement cvv;
	
	@FindBy(id="MonetaryCodelistPartialView_0__debitCardExpiryDt")
	public WebElement expDate;
	
	@FindBy(id="MonetaryCodelistPartialView_0__CheckCardExpiryDt")
	WebElement ExistingCardExpDate;
	
	@FindBy(id="MonetaryCodelistPartialView_0__BillingAddress1")
	WebElement cardBillingAdd1;
	
	@FindBy(id="MonetaryCodelistPartialView_0__BillingAddress2")
	WebElement cardBillingAdd2;
	
	@FindBy(id="MonetaryCodelistPartialView_0__BillingState")
	WebElement cardBillingState;
	
	@FindBy(id="MonetaryCodelistPartialView_0__BillingCity")
	WebElement cardBillingCity;
	
	@FindBy(className="BillingZipCode")
	WebElement billingZipCode;
	
	@FindBy(id="customerPaymentButton")
	WebElement processPayment;
	
	@FindBy(className="globalPasswordBox")
	WebElement pwd;
	
	@FindBy(linkText="Verify")
	WebElement verifyBtn;
	
	@FindBy(linkText="No Receipt")
	WebElement noReceiptBtn;
	
	@FindBy(linkText="Email Receipt")
	WebElement emailReceiptBtn;
	
	@FindBy(linkText="OK")
	WebElement okBtn;
	
	
	public void clickMoreBtn()
	{
		moreBtn.click();
	}
	
	public void clickMinDueAmtCard()
	{
		String minDue=minDueAmt.getAttribute("value");
		System.out.println(minDue);
	
		new Select(payBy).selectByIndex(3);
		Sleeper.sleepTightInSeconds(3);
		
		cardTenderAmt.sendKeys(minDue);
	}	
	
	public void clickPayOffCard()
	{
		payOff.click();
		String payOffAmt=payOffAmount.getAttribute("value");
		
		new Select(payBy).selectByIndex(3);
		Sleeper.sleepTightInSeconds(3);
		
		cardTenderAmt.sendKeys(payOffAmt);
	}
	
	public void clickPayOtherAmtCard()
	{
		principalRed.click();
		Sleeper.sleepTightInSeconds(1);
		Principalamt.sendKeys("10");
		
		new Select(payBy).selectByIndex(3);
		Sleeper.sleepTightInSeconds(3);
		
		cardTenderAmt.sendKeys("10");
	}
	
	public void PaymentBtn(WebDriverWait wait) throws AWTException
	{
		Robot rbt=new Robot();
		
		rbt.keyPress(KeyEvent.VK_DOWN);
		rbt.keyRelease(KeyEvent.VK_DOWN);
		rbt.keyPress(KeyEvent.VK_DOWN);
		rbt.keyRelease(KeyEvent.VK_DOWN);
		rbt.keyPress(KeyEvent.VK_DOWN);
		rbt.keyRelease(KeyEvent.VK_DOWN);
		rbt.keyPress(KeyEvent.VK_DOWN);
		rbt.keyRelease(KeyEvent.VK_DOWN);
		rbt.keyPress(KeyEvent.VK_DOWN);
		rbt.keyRelease(KeyEvent.VK_DOWN);
		rbt.keyPress(KeyEvent.VK_DOWN);
		rbt.keyRelease(KeyEvent.VK_DOWN);
		rbt.keyPress(KeyEvent.VK_DOWN);
		rbt.keyRelease(KeyEvent.VK_DOWN);
		rbt.keyPress(KeyEvent.VK_DOWN);
		rbt.keyRelease(KeyEvent.VK_DOWN);
		rbt.keyPress(KeyEvent.VK_DOWN);
		rbt.keyRelease(KeyEvent.VK_DOWN);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("paymentLnkBtn")));
		Sleeper.sleepTightInSeconds(2);
		paymentBtn.click();
		Sleeper.sleepTightInSeconds(3);
	}
	public void clickMinPayDueCash() 
	{	
		String minDue=minDueAmt.getAttribute("value");
		System.out.println(minDue);
		
		new Select(payBy).selectByVisibleText("Cash");
		Sleeper.sleepTightInSeconds(3);
		
		tenderAmt.click();
		tenderAmt.sendKeys(minDue);
	}
	
	public void clickPayOffCash()
	{
		payOff.click();
		String payOffAmt=payOffAmount.getAttribute("value");
		
		new Select(payBy).selectByVisibleText("Cash");
		Sleeper.sleepTightInSeconds(3);
		
		tenderAmt.sendKeys(payOffAmt);
	}
	
	public void clickPayOtherAmtCash()
	{
		principalRed.click();
		Sleeper.sleepTightInSeconds(1);
		Principalamt.sendKeys("10");
		
		new Select(payBy).selectByVisibleText("Cash");
		Sleeper.sleepTightInSeconds(3);
		
		tenderAmt.sendKeys("10");
	}
	
	public void selectCard()
	{
		new Select(selectCard).selectByIndex(1);
		Sleeper.sleepTightInSeconds(2);
		ExistingCardExpDate.click();
	}
	
	public void enterCardDetails(String expiryDate, String nameCard)
	{
		ExistingCardExpDate.sendKeys(expiryDate);
		ExistCardNameOnCard.sendKeys(nameCard);
	}
	
	public void AddCardDetails(WebDriverWait wait) 
	{
		Sleeper.sleepTightInSeconds(5);
		new Select(selectCard).selectByVisibleText("--Add Another Card--");
		String CardNumber=pageCon.cardNumber();;
		cardNum.sendKeys(CardNumber);
		
		nameOnCard.sendKeys("jack");
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		
		cvv.sendKeys("110");
		
		expDate.sendKeys("12/22");
		
		cardBillingAdd1.sendKeys("Add One");
		
		cardBillingAdd2.sendKeys("Add Two");
		
		new Select(cardBillingState).selectByVisibleText("Tennessee");
		Sleeper.sleepTightInSeconds(2);
		
		new Select(cardBillingCity).selectByIndex(5);
		
		billingZipCode.sendKeys("34567");
		Sleeper.sleepTightInSeconds(3);
	}
	
	public void processPaymentForCash(WebDriverWait wait) 
	{
		processPayment.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(1);
		processPayment.click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("globalPasswordBox")));
		pwd.sendKeys("FiNext@123");
		verifyBtn.click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Email Receipt")));
		Sleeper.sleepTightInSeconds(2);
		
		emailReceiptBtn.click();
		Sleeper.sleepTightInSeconds(2);
		okBtn.click();
	}
	
	public void processPayment(WebDriverWait wait) 
	{
		processPayment.click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("globalPasswordBox")));
		pwd.sendKeys("FiNext@123");
		verifyBtn.click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Email Receipt")));
		Sleeper.sleepTightInSeconds(2);
		
		emailReceiptBtn.click();
		Sleeper.sleepTightInSeconds(2);
		okBtn.click();
	}
}
