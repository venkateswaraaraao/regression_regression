package com.afqa.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Online_CxHome_Menu {
	
	@FindBy(xpath="html/body/div[9]/div[2]/div[1]/div[2]/div[2]/a")
	WebElement AccDetailsLink;
	public void AccountDetails()
	{
		AccDetailsLink.click();
	}

	@FindBy(xpath="html/body/div[9]/div[2]/div[1]/div[3]/div[2]/a")
	WebElement ViewTransLink;
	public void ViewTransactions()
	{
		ViewTransLink.click();
	}
	
	@FindBy(className="MakePayment")
	WebElement MakePaymentLink;
	public void MakePayment()
	{
		MakePaymentLink.click();
	}
	
	@FindBy(xpath="html/body/div[9]/div[2]/div[2]/div[3]/div[2]/a")
	WebElement ViewStatementsLink;
	public void ViewStatements()
	{
		ViewStatementsLink.click();
	}
	
	@FindBy(id="requestAdvanceBtn")
	WebElement ReqCashAdvLink;

	public void RequestCashAdvance()
	{
		ReqCashAdvLink.click();
	}
	
	@FindBy(xpath="html/body/div[9]/div[2]/div[3]/div[2]/div[2]/a")
	WebElement ChangePWDLink;
	public void ChangePassword()
	{
		ChangePWDLink.click();
	}
	
	@FindBy(xpath="html/body/div[9]/div[2]/div[3]/div[3]/div[2]/a")
	WebElement ViewDocsLink;
	public void ViewDocuments()
	{
		ViewDocsLink.click();
	}
	
	@FindBy(xpath="html/body/div[9]/div[2]/div[3]/div[4]/div[2]/a")
	WebElement ManagePaySettingsLink;
	public void ManagePaymentSettings()
	{
		ManagePaySettingsLink.click();
	}
}
