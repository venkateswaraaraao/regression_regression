package com.afqa.page;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Online_RegisterLogin_Page {
	
	static Logger Log = Logger.getLogger("RegisterLogin_Page");

	@FindBy(id="ExistingCustomerEmail")
	WebElement emailBox;
	@FindBy(className="editExistingCustomerEmail")
	WebElement editEmailBtn;
	@FindBy(id="Password")
	WebElement pWord;
	@FindBy(id="ConfirmPassword")
	WebElement confPword;
	@FindBy(id="SecurityQuestion")
	WebElement secQuestion;
	@FindBy(id="SecurityAnswer")
	WebElement secAnswer;
	@FindBy(className="addLoginSubmit")
	WebElement submitBtn;
	@FindBy(className="cancel")
	WebElement cancelBtn;
	@FindBy(id="globalPopup")
	WebElement successPopup;
	@FindBy(xpath="//*[@id='globalPopup']/div[2]/div/div/a")
	WebElement okBtn;
	@FindBy(className="existingCustomer")
	WebElement ApplyNowBtn;
	
	public void EditEmailRegisterAcc(String eMail, WebDriverWait wait){
		
		String oldEmail=emailBox.getAttribute("value");
		editEmailBtn.click();
		emailBox.clear();
		emailBox.sendKeys(eMail);
		pWord.sendKeys("password");
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		confPword.sendKeys("password");
		new Select(secQuestion).selectByIndex(2);
		secAnswer.sendKeys("0255");
		String newEmail=emailBox.getAttribute("value");
		
		if(!oldEmail.equals(newEmail))
		{
			System.out.println("New Email updated successfully");
			Log.info("New Email updated successfully");
		}
		else
		{
			System.out.println("Email is not updated, email update failed");
			Log.info("Email is not updated, email update failed");
		}
	}
	
	public void RegisterAcc()
	{
		pWord.sendKeys("password");
		confPword.sendKeys("password");
		new Select(secQuestion).selectByIndex(2);
		secAnswer.sendKeys("0255");
	}
	
	public String newEmail(){
		String nEmail=emailBox.getAttribute("value");
		return nEmail;
	}
	
	public void registerLoginPopup(){
		
		DOMConfigurator.configure("log4j.xml");
		
		submitBtn.click();
		Sleeper.sleepTightInSeconds(2);
		String PopupMsg=successPopup.getText();
		System.out.println(PopupMsg);
		Log.info(PopupMsg);
		okBtn.click();
	}
	
	public void ApplyNow()
	{
		ApplyNowBtn.click();
	}
}
