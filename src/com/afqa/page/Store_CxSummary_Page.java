package com.afqa.page;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Store_CxSummary_Page {
	static Logger Log = Logger.getLogger(Store_CxSummary_Page.class.getName());
	
	@FindBy(className="ccustomerSummaryTab")
	WebElement CxSummaryBtn;
	
	@FindBy(className="moreButton")
	WebElement moreBtn;
	
	@FindBy(id="paymentLnkBtn")
	WebElement paymentBtn;
	
	public void clickMoreBtn()
	{
		moreBtn.click();
	}
	
	public void clickMakePayment()
	{
		paymentBtn.click();
	}
	
	public void CheckCxSumamry(WebDriverWait wait)
	{
		String CxSummary=CxSummaryBtn.getText();
		
		if(CxSummary.contains("Summary"))
		{
			Log.info("Customer Navigated to CustomerSummary Page");
		}
		else
		{
			Log.info("Customer should Navigate to customerSummary Page");
		}
		
	}
}
