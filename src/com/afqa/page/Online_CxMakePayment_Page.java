package com.afqa.page;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Online_CxMakePayment_Page {
	
	static Logger Log = Logger.getLogger("CxMakePayment_Page");
	
	Online_CxHome_Menu CxMenu=null;
	public Online_CxMakePayment_Page(WebDriver driver)
	{
		CxMenu=PageFactory.initElements(driver, Online_CxHome_Menu.class);
	}
	
	@FindBy(id="minpayradio")
	WebElement MinPayRDBtn;
	@FindBy(xpath="//*[@id=\'BodyDiv paymentBody\']/div/div[3]/div[1]/div[2]/div[3]/div/table/tbody/tr[2]/td[1]/input[1]")
	WebElement StmtCheck;
	@FindBy(id="currbalradio")
	WebElement PayOffRDBtn;
	@FindBy(id="otherradio")
	WebElement PrinciReductRDBtn;
	@FindBy(id="Other")
	WebElement PRAmount;
	@FindBy(className="CustomerPortalMonetaryCodePaymentDD")
	WebElement PayByType;
	@FindBy(id="MonetaryCodelistPartialView_0__PayAmount")
	WebElement PayAmount;
	@FindBy(className="CustomerPortalCardNumbersDD")
	WebElement selectCardDD;
	@FindBy(className="CustomerPortalBankAccountDD")
	WebElement Account;
	@FindBy(id="customerPaymentButton")
	WebElement ProcessPaymentBtn;

	@FindBy(className="CustomerPortalCCLastFourDigits")
	WebElement newCardNumber;
	@FindBy(className="CustomerPortalNameOnTheDebitCard")
	WebElement nameOnCard;
	@FindBy(className="CustomerPortaldebitCardCVV")
	WebElement newCardCVV;
	@FindBy(className="CustomerPortaldebitCardExpiryDt")
	WebElement cardExpiryDate;
	@FindBy(className="IsSameAsMailingAddress")
	WebElement sameAsMailAddCheckBox;
	@FindBy(className="CustomerPortalBillingZipCode")
	WebElement billingZip;
	
	@FindBy(id="globalPopup")
	WebElement PaymentPopup;
	@FindBy(className="popupDialogYes")
	WebElement PopupOKBtn;
	
	public void MakePayment(Row r)
	{
		CxMenu.MakePayment();
		String PayType=r.getCell(0).getStringCellValue();
		if(PayType.equals("1"))
		{
			MinPayRDBtn.click();
		}
		else
		{
			if(PayType.equals("1"))
			{
				PayOffRDBtn.click();
			}
			else
			{
				PrinciReductRDBtn.click();
			}
		}
		new Select(PayByType).selectByIndex(1);
		PayAmount.sendKeys("10.00");
		new Select(Account).selectByIndex(0);
		ProcessPaymentBtn.click();
	}
	
	public void clickMinPayDue()
	{
		MinPayRDBtn.click();
		
		Sleeper.sleepTightInSeconds(2);
		
		StmtCheck.click();
	}
	
	public void clickPayOff()
	{
		PayOffRDBtn.click();
	}
	
	public void clickPrincipleRed()
	{
		PrinciReductRDBtn.click();
		Sleeper.sleepTightInSeconds(2);
		PRAmount.sendKeys("1");
	}

	public void selectPayBy_DebitFromBank()
	{
		new Select(PayByType).selectByIndex(1);
	}
	
	public void selectPayBy_DebitCard()
	{
		new Select(PayByType).selectByIndex(2);
		Sleeper.sleepTightInSeconds(3);
	}
	
	public void selectPayBy_DebitCard_addCard(String CardNumber)
	{
		new Select(selectCardDD).selectByVisibleText("Add Another Card");	
		Sleeper.sleepTightInSeconds(3);
		newCardNumber.sendKeys(CardNumber);
		nameOnCard.sendKeys("chandu");
		newCardCVV.sendKeys("258");

		cardExpiryDate.click();
		cardExpiryDate.sendKeys("09/2025");
		
		sameAsMailAddCheckBox.click();
		
		billingZip.sendKeys("55022");
	}
	
	public void paymentByAddCard(WebDriverWait wait, String CardNumber)
	{
		selectPayBy_DebitCard();
		selectPayBy_DebitCard_addCard(CardNumber);
		ProcessPaymentBtn.click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("globalPopup")));
		PopupOKBtn.click();
		String PopMsg=PaymentPopup.getText();
		Log.info(PopMsg);
	}
	
	public void paymentByDebitFromBank(WebDriverWait wait)
	{	
		selectPayBy_DebitFromBank();
		ProcessPaymentBtn.click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("globalPopup")));
		PopupOKBtn.click();
		String PopMsg=PaymentPopup.getText();
		Log.info(PopMsg);
	}
}
