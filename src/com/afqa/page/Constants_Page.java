package com.afqa.page;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.apache.poi.ss.usermodel.Row;

public class Constants_Page 
{
	public String PartName()
	{
		String[] Names={"Emma","Alma","Rose","Tina","Heba","Mia","Cami","Syra",
						"Dune","Suvi","Brit","Saga","Kaia","Sofi","Nina","Alix",
						"Lena","Lita","Elin","Alma","Erel","Nora","Aria","Lily","Veda",
						"Zoey","Maia","Isla","Emna","Emmy","Esma","Erin","Anne",
						"Aroa","Cate","Nora","Leah","Adia","Asma","Jade","Musa","Miah",
						"Jena","Floy","Dona","Zola","Poet","Mila","Dove","Bree"};
		String FName = (Names[new Random().nextInt(Names.length)]);
		return FName;
	}

	public String randNum()
	{
		int eNum=(int) ((int) 10000 * Math.random());
		String Ename=String.valueOf(eNum);
		return Ename;
	}

	public String todayDate()
	{
		DateFormat dateFormat = new SimpleDateFormat("MMddYY");
		Date date = new Date();
		String Today=dateFormat.format(date);
		return Today;
	}
	
	public String Today()
	{
		DateFormat dateFormat = new SimpleDateFormat("mmddyy");
		Date date = new Date();
		String Today=dateFormat.format(date);
		return Today;
	}

	public String TodayID()
	{
		DateFormat dateFormat = new SimpleDateFormat("MMdd");
		Date date = new Date();
		String Today=dateFormat.format(date);
		return Today;
	}

	public String onlineEmail()
	{
		String OnEmail=PartName()+"_"+randNum()+"@"+todayDate()+".com";
		return OnEmail;
	}
	
	public String spouseEmail()
	{
		String SpEmail="spouse_"+randNum()+"@"+todayDate()+".com";
		return SpEmail;
	}
	
	public String storeEmail(Row r)
	{
		String regType=r.getCell(0).getStringCellValue();
		
		String StoreEMail=null;
		if(regType.equals("US")){
			StoreEMail=PartName()+"_us"+randNum()+"@"+todayDate()+".com";
		}
		if(regType.equals("Misc")){
			StoreEMail=PartName()+"_mc"+randNum()+"@"+todayDate()+".com";
		}
		if(regType.equals("NS")){
			StoreEMail=PartName()+"_ns"+randNum()+"@"+todayDate()+".com";
		}
		if(regType.equals("Sec")){
			StoreEMail=PartName()+"_sc"+randNum()+"@"+todayDate()+".com";
		}
		return StoreEMail;
	}

	public String onlinePhNumber()
	{
		Long PhNum=(long) ((long) 1000*(6+Math.random()));
		String PhoneNumber=String.valueOf(PhNum);
		String phone=Today()+PhoneNumber;
		return phone;
	}

	public String storePhNumber()
	{
		Long PhNum=(long) ((long) 1000*(5+Math.random()));
		String PhoneNumber=String.valueOf(PhNum);
		String phone=Today()+PhoneNumber;
		return phone;
	}

	public String onlineSSN()
	{
		Long SSN=(long) ((long) 100*(6+Math.random()));
		String SSNum=String.valueOf(SSN);
		String ssn=Today()+SSNum;
		return ssn;
	}

	public String storeSSN()
	{
		Long SSN=(long) ((long) 100*(6+Math.random()));
		String SSNum=String.valueOf(SSN);
		String ssn=Today()+SSNum;
		return ssn;
	}

	public String onlinePhotoID()
	{
		Long PhotoID=(long) ((long) 100000*(5+Math.random()));
		String photoID=String.valueOf(PhotoID);
		String Photoid="TN"+TodayID()+photoID;
		return Photoid;
	}

	public String AccountNum()
	{
		Long AcNum=(long) ((long) 1000*(5+Math.random()));
		String AccNum=String.valueOf(AcNum);
		String AccountNum=Today()+AccNum;
		return AccountNum;
	}

	public String EmpPhNum()
	{
		Long EmpPh=(long) ((long) 1000000000 *(5+ Math.random()));
		String EmpPhone=String.valueOf(EmpPh);
		return EmpPhone;
	}

	public String cardNumber()
	{
		String[] CardNums={"5200827832667243","5200823911522102","5200826748182578","5200825358393160","5200823261837647",
							"5200820789312632","5200820562405942","5200828449258483","5200829882835662","5200824622405967"};
		
		String CardNum = (CardNums[new Random().nextInt(CardNums.length)]);
		return CardNum;
	}
}
