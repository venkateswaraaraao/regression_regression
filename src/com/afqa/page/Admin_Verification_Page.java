package com.afqa.page;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Admin_Verification_Page
{
	static Logger Log = Logger.getLogger(Admin_Verification_Page.class.getName());
	
	@FindBy(xpath="//*[contains(@fnhref,'/Admin/AdminHome/Home')]")
	WebElement HomeBtn;
	@FindBy(xpath="//*[contains(@fnhref,'/Admin/AdminHome/GetCurrentVerifications')]")
	WebElement VerifyIcon;
	@FindBy(xpath="//*[contains(@fnhref,'/Admin/AdminHome/LogOut')]")
	WebElement LogOutBtn;
	@FindBy(className="finext-grid-offlineSearch")
	WebElement GridSearchBox;
	@FindBy(xpath="//*[contains(@fnhref,'/Admin/AdminHome/GetCurrentVerifications?verificationType=NoRisk')]")
	WebElement RegularTab;
	@FindBy(xpath="//*[contains(@fnhref,'/Admin/AdminHome/GetCurrentVerifications?verificationType=Suspicious')]")
	WebElement SuspiciousTab;
	@FindBy(xpath="//*[contains(@fnhref,'/Admin/AdminHome/GetCurrentVerifications?verificationType=HigRisk')]")
	WebElement HighRiskTab;
	@FindBy(xpath="//*[contains(@fnhref,'/Admin/AdminHome/GetSupervisorRequests')]")
	WebElement SuperVisorReqTab;
	@FindBy(xpath="//*[contains(@fnhref,'/Admin/AdminHome/GetCustomersToRelease')]")
	WebElement ReleaseCxTab;
	@FindBy(xpath="//*[@id='BodyDiv']/div/div[2]/div/div[3]/div[2]/table/thead/tr[2]/th[1]")
	WebElement emptySpace;
	@FindBy(linkText="Last")
	WebElement GridLastLink;
	@FindBy(xpath="//*[@id='BodyDiv']/div/div[2]/div/div[3]/div[2]/table/tbody/tr[3]/td[1]")
	WebElement CxIDinGrid;
	@FindBy(id="VerficationDetails_PersonalDetails_IsActiveMilitary")
	WebElement ActiveMilitaryDD;
	@FindBy(id="VerficationDetails_IncomeType")
	WebElement VerifyIncomeType;
	public @FindBy(id="VerficationDetails_StatusID")
	WebElement VerifyStatusDD;
	public @FindBy(id="verifyData")
	WebElement verifySaveBtn;
	@FindBy(className="popupDialogCont")
	WebElement verifyPopup;
	@FindBy(className="popupDialogYes")
	WebElement popupOKBtn;
	@FindBy(id="NMIDocuments_BankStatement")
	WebElement checkNeedBankStmt;
	@FindBy(id="NMIDocuments_NMIReasonForProofOfBankAcc")
	WebElement selectReasonForBnkStmt;
	@FindBy(id="NMIDocuments_ProofOfIncome")
	WebElement checkNeedProofOfIncome;
	@FindBy(id="NMIDocuments_NMIReasonForProofOfIncome")
	WebElement selectReasonForProofOfInc;
	@FindBy(xpath="//input[contains(@value,'Submit')]")
	WebElement submitBtn;
	
	public void clickRiskTab(WebDriverWait wait, String CxRiskRate,String CxID)
	{
		if(CxRiskRate.equals("20"))
		{
			HighRiskTab.click();
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
			Sleeper.sleepTightInSeconds(5);
			
			String LastBtn=GridLastLink.getText();
			Log.info(LastBtn);
			if(LastBtn.contains("Last"))
			{
				GridLastLink.click();
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
				Sleeper.sleepTightInSeconds(3);
				GridSearchBox.sendKeys(CxID);
				Sleeper.sleepTightInSeconds(5);
				emptySpace.click();
			}
			else
			{
				GridSearchBox.click();
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
				Sleeper.sleepTightInSeconds(2);
				GridSearchBox.sendKeys(CxID);
				Sleeper.sleepTightInSeconds(2);
				emptySpace.click();
				Sleeper.sleepTightInSeconds(2);
			}
			
		}
		if(CxRiskRate.equals("10"))
		{
			SuspiciousTab.click();
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
			Sleeper.sleepTightInSeconds(5);
			
			String LastBtn=GridLastLink.getText();
			Log.info(LastBtn);
			
			if(LastBtn.contains("Last"))
			{
				GridLastLink.click();
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
				Sleeper.sleepTightInSeconds(3);
				GridSearchBox.sendKeys(CxID);
				Sleeper.sleepTightInSeconds(5);
				emptySpace.click();
			}
			else
			{
				GridSearchBox.click();
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
				Sleeper.sleepTightInSeconds(2);
				GridSearchBox.sendKeys(CxID);
				Sleeper.sleepTightInSeconds(2);
				emptySpace.click();
				Sleeper.sleepTightInSeconds(2);
			}
		}
		if(CxRiskRate.equals("0"))
		{
			RegularTab.click();
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
			Sleeper.sleepTightInSeconds(5);
			
			String LastBtn=GridLastLink.getText();
			Log.info(LastBtn);
			
			if(LastBtn.contains("Last"))
			{
				GridLastLink.click();
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
				Sleeper.sleepTightInSeconds(3);
				GridSearchBox.sendKeys(CxID);
				Sleeper.sleepTightInSeconds(5);
				emptySpace.click();
			}
			else
			{
				GridSearchBox.click();
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
				Sleeper.sleepTightInSeconds(2);
				GridSearchBox.sendKeys(CxID);
				Sleeper.sleepTightInSeconds(2);
				emptySpace.click();
				Sleeper.sleepTightInSeconds(2);
			}
		}
	}
	
	public void pickNupdateCustomer(String CxID, WebDriverWait wait,String CxRiskRate, WebDriver driver)
	{		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(@fnhref,'/Admin/AdminHome/GetCurrentVerifications')]")));
		VerifyIcon.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(5);
		
		//Look into Verification Tabs based on Customer Risk Rating
		clickRiskTab(wait, CxRiskRate,CxID);
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(2);
		
		String CustID=CxIDinGrid.getText();
		if(CustID.equals(CxID))
		{
			try
			{
				CxIDinGrid.click();
				Log.info("Clicked on CxID For Verification");
			}
			catch(NoSuchElementException E)
			{
				Log.info("Unable to pick the Customer");
			}
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("cusBankDetails")));
			new Select (ActiveMilitaryDD).selectByIndex(2);
			Sleeper.sleepTightInSeconds(2);
			
			//new Select(VerifyIncomeType).selectByIndex(1);
		}
		
		/*try
		{														
			boolean dispayCxinGrid=driver.findElements(By.xpath("//*[@id='BodyDiv']/div/div[2]/div/div[3]/div[2]/table/tbody/tr[3]/td[1]")).size()>0;
			if(dispayCxinGrid==false)
			{
				clickRiskTab(wait, CxRiskRate);
				GridLastLink.click();
				
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
				Sleeper.sleepTightInSeconds(3);
				GridSearchBox.sendKeys(CxID);
				Sleeper.sleepTightInSeconds(5);
				emptySpace.click();
			}
			//String CustID=CxIDinGrid.getText();
			if(CustID.equals(CxID))
			{
				try
				{
					CxIDinGrid.click();
					Log.info("Clicked on CxID For Verification");
				}
				catch(NoSuchElementException E)
				{
					Log.info("Unable to pick the Customer");
				}
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("cusBankDetails")));
				new Select (ActiveMilitaryDD).selectByIndex(2);
				Sleeper.sleepTightInSeconds(2);
				
				//new Select(VerifyIncomeType).selectByIndex(1);
			}
		}
		catch(Exception e)
		{
			Log.info("Customer not in the verification");
		}*/
	}
	
	public void updateVerificationStatus(WebDriver driver, Row r)
	{
		int setVerifyStatus=(int) r.getCell(13).getNumericCellValue();
		
		new Select (VerifyStatusDD).selectByIndex(setVerifyStatus);
		if(setVerifyStatus<1)
		{
			Log.info("Customer updated with 'Accepted' Status");
		}
		if(setVerifyStatus<2&&setVerifyStatus>0)
		{
			Log.info("Customer updated with 'Declined' Status");
		}
		if(setVerifyStatus<3&&setVerifyStatus>1)
		{
			Log.info("Customer updated with 'Supervisor Request' Status");
		}
		if(setVerifyStatus<4&&setVerifyStatus>2)
		{
			Log.info("Customer updated with 'Need More Info' Status");
		}
		if(setVerifyStatus>3)
		{
			Log.info("Customer updated with 'Send to Store' Status");
		}
		
		Sleeper.sleepTightInSeconds(5);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", verifySaveBtn);
		
		Sleeper.sleepTightInSeconds(2);
		verifySaveBtn.click();
		try
		{
			verifySaveBtn.click();
		}
		catch(Exception e)
		{}
		try
		{
			Sleeper.sleepTightInSeconds(2);
			
			checkNeedBankStmt.click();
			new Select(selectReasonForBnkStmt).selectByIndex(1);
			
			checkNeedProofOfIncome.click();
			new Select(selectReasonForProofOfInc).selectByIndex(2);
			
			submitBtn.click();
		}
		catch(Exception e)
		{}

	}
	
	public void UpdateVerificationStatusAccept(WebDriver driver)
	{
		new Select(VerifyStatusDD).selectByVisibleText("Accepted");
		
		Log.info("Customer Accepted in Verification");
		
		Sleeper.sleepTightInSeconds(5);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", verifySaveBtn);
		
		Sleeper.sleepTightInSeconds(2);
		verifySaveBtn.click();
	}
	
	public void updateStoreCxStatus(WebDriver driver, Row r, WebDriverWait wait)
	{
		int setVerifyStatus=(int) r.getCell(11).getNumericCellValue();
		
		new Select (VerifyStatusDD).selectByIndex(setVerifyStatus);
		Sleeper.sleepTightInSeconds(5);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", verifySaveBtn);
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(2);
		verifySaveBtn.click();
		try
		{
			verifySaveBtn.click();
		}
		catch(Exception e)
		{}
	}
	
	public void readANDclosePopup()
	{
		String popupMessage=verifyPopup.getText();
		Log.info(popupMessage);
		Log.info("-----------------------------------------------------------------------");
		Sleeper.sleepTightInSeconds(1);
		popupOKBtn.click();
	}
	
	@FindBy(xpath="/html/body/div[1]/div[1]/div/ul/li/a/img")
	WebElement HmBtn;
	public void clickHome()
	{
		HmBtn.click();
	}
}
