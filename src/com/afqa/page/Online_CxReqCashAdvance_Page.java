package com.afqa.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Online_CxReqCashAdvance_Page {
	
	Online_CxHome_Menu CxMenu=null;
	public Online_CxReqCashAdvance_Page(WebDriver driver)
	{
		CxMenu=PageFactory.initElements(driver, Online_CxHome_Menu.class);
	}
	
	@FindBy(id="LOCs_LOCCurrentLimit")
	WebElement CreditLimit;
	@FindBy(id="LOCs_CurrentLOCPrincipal")
	WebElement PrinciBalance;
	@FindBy(id="LOCs_AvailableCredit")
	WebElement AvailCredit;
	@FindBy(id="AdvanceOrDrawRequested")
	WebElement AmntDraw;
	@FindBy(id="MonetaryCodelist_0__CustomerBankID")
	WebElement DepositAcc;
	@FindBy(xpath="html/body/div[9]/div[1]/form/div[2]/div[3]/div[1]/div[7]/div/a[1]")
	WebElement SubmitBtn;
	@FindBy(id="AdvanceOrDrawRequested")
	WebElement AmountToDraw;
	@FindBy(linkText="Get My Cash")
	WebElement getMyCash;
	@FindBy(id="globalPopup")
	WebElement AmtdepositToAccPopup;
	@FindBy(id="TempCardDetails_OneTimeSecurityCode")
	WebElement OTPVerificationField;
	@FindBy(xpath="//*[@class='logoutLink']/img")
    WebElement logout;
	WebElement OneTo2BusinessDayFund;
	@FindBy(className="drawSubmitbutton")
	public WebElement submitBtn;
	@FindBy(className="popupDialogYes")
	WebElement withDrawalConfirmBtn;
	@FindBy(linkText="Ok")
	WebElement okBtn;
	
	public void CashAdvance()
	{
		//CxMenu.RequestCashAdvance();
		String CrLimit=CreditLimit.getText();
		double LOCCreditLimit=Double.parseDouble(CrLimit);
		
		String PrBalance=PrinciBalance.getText();
		double LOCPrincipleBalance=Double.parseDouble(PrBalance);
		double ReqAmount=LOCPrincipleBalance/2;
		String CrAvail=AvailCredit.getText();
		double LOCAvailableCredit=Double.parseDouble(CrAvail);
		
		Double TotalAmount=LOCPrincipleBalance+LOCAvailableCredit;
		if(TotalAmount.equals(LOCCreditLimit))
		{
			System.out.println("Amount Matched");
		}
		else
		{
			System.out.println("Amount not matched");
		}
		
		AmntDraw.sendKeys(String.valueOf(ReqAmount));
	}
	public void RequestAdvance(WebDriverWait wait)
	{
		AmountToDraw.click();
		Sleeper.sleepTightInSeconds(1);
		AmountToDraw.clear();
		Sleeper.sleepTightInSeconds(2);
		AmountToDraw.sendKeys("50");
		Sleeper.sleepTightInSeconds(2);
	}
	
	public void getMyCash(WebDriverWait wait)
    {
		getMyCash.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(1);
		getMyCash.click();
		
    }
	
	public void OTPVerification(String SecurityCode,WebDriverWait wait)
	{
		OTPVerificationField.sendKeys(SecurityCode);
		getMyCash.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(5);
		try{
			String AmtdepositToAccPopupText=AmtdepositToAccPopup.getText();
			System.out.println(AmtdepositToAccPopupText);
			if(AmtdepositToAccPopupText.contains(" to your bank account ending in"))
			{
				getMyCash.click();
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
				Sleeper.sleepTightInSeconds(1);
				okBtn.click();
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
				Sleeper.sleepTightInSeconds(1);			
			}
			else
			{
				okBtn.click();
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
				Sleeper.sleepTightInSeconds(1);
			}
		}
		catch(Exception ex){ } 
		
	}
	public void CxLogout()
	{
		logout.click();
	}
	
	public void OneToTwoBusinessFund(WebDriverWait wait)
	{
		OneTo2BusinessDayFund.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(1);
		OneTo2BusinessDayFund.click();
	}
	public void WithdrawalSubmit(WebDriverWait wait)
	{
		submitBtn.click();
		Sleeper.sleepTightInSeconds(2);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("popupDialogYes")));
		Sleeper.sleepTightInSeconds(2);
		withDrawalConfirmBtn.click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Ok")));
		
		okBtn.click();
	}
	
	public void CXCardDetails(String CxID)
	{
		
	}
}
