package com.afqa.page;

import java.awt.AWTException;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Online_Registration_Page {
	
	static Logger Log = Logger.getLogger(Online_Registration_Page.class.getName());
	
	Constants_Page pageCon=null;
	
	public Online_Registration_Page(WebDriver driver)
	{
		pageCon=PageFactory.initElements(driver, Constants_Page.class);
	}
	
	@FindBy(linkText="Wizard Flow")
	WebElement Wizard_Link;
	@FindBy(id="Email")
	public WebElement Email;
	@FindBy(xpath="//*[@id='globalPopup']/div[2]/div/div[1]/a")
	WebElement Pop_EmailForgotPWD;
	@FindBy(xpath="//*[@id='globalPopup']/div[2]/div/div[2]/a")
	WebElement Pop_EmailLogin;
	@FindBy(id="ConfirmEmail")
	public WebElement ConfirmEmail;
	@FindBy(id="PhoneNumber")
	public WebElement PhNumber;
	@FindBy(xpath="//*[@id='globalPopup']/div[2]/div/div[1]/a")
	WebElement Pop_PhExistOK;
	@FindBy(xpath="//*[@id='globalPopup']/div[2]/div/div[1]/a")
	WebElement Pop_VerifyYes;
	@FindBy(xpath="//*[@id='globalPopup']/div[2]/div/div[2]/a")
	WebElement Pop_VerifyNo;
	@FindBy(id="Password")
	WebElement Pword;
	@FindBy(id="ConfirmPassword")
	WebElement ConfirmPword;
	@FindBy(id="DOB")
	WebElement DateOfBirth;
	@FindBy(id="FirstName")
	WebElement FName;
	@FindBy(id="LastName")
	WebElement LName;
	@FindBy(id="SSN")
	WebElement SSNumber;
	@FindBy(xpath="//*[@id='globalPopup']/div[2]/div/div[1]/a")
	WebElement Pop_SSNForgotPWD;
	@FindBy(xpath="//*[@id='globalPopup']/div[2]/div/div[2]/a")
	WebElement Pop_SSNLogin;
	@FindBy(id="SecurityQuestion")
	WebElement SecQuestion;
	@FindBy(id="SecurityAnswer")
	WebElement SecAnswer;
	
	// for Wisconsin State added
	@FindBy(id="StateID")
	public WebElement StateOne;
	@FindBy(id="MaritalStatusSpouse")
	WebElement MaritalStatus;
	@FindBy(id="FirstNameSpouse")
	WebElement SpouseFirstName;
	@FindBy(id="LastNameSpouse")
	WebElement SpouseLastName;
	@FindBy(id="SSNSpouse")
	WebElement SpouseSSN;
	@FindBy(id="EmailSpouse")
	WebElement SpouseEmail;
		
	@FindBy(id="IsActiveMilitary_false")
	WebElement MilitaryNo;
	@FindBy(id="IsActiveMilitary_true")
	WebElement MilitaryYes;
	@FindBy(xpath="//*[@id='globalPopup']/div[2]/div/div[1]/a")
	WebElement PopMilitaryOK;
	@FindBy(xpath="//*[@id='globalPopup']/div[2]/div/div[2]/a")
	WebElement PopMilitaryChange;
	@FindBy(id="TransactBusinessAgreed")
	WebElement TermsAndCon;
	@FindBy(id="ConsentReportAgreed")
	WebElement ConsentCheck;
	@FindBy(id="IsCommunicationPolicy")
	WebElement ComPolicyCheck;
	@FindBy(id="IsElectronicDisclosure")
	WebElement ElectronicDisclosureCheck;
	@FindBy(tagName="iframe")
	WebElement chatFrame;
	@FindBy(xpath="html/body/div[1]/div/div/div[2]/div/div/div[1]/div/div[1]/div[3]")
	WebElement minimiseChat;
	@FindBy(id="AccountCreation")
	WebElement AccCreationNextBTN;
	@FindBy(xpath="//*[@id='globalPopup']/div[2]/div/div/a")
	WebElement Pop_TermsAndConOK;
	@FindBy(id="globalPopup")
	WebElement PopUp;
	@FindBy(xpath="//*[@id='globalPopup']/div[2]/div/div/a")
	public WebElement PopupOKBTN;
	@FindBy(id="AddressLine1")
	public WebElement Address1;
	@FindBy(id="AddressLine2")
	public	WebElement SuiteNumber;
	@FindBy(id="StateID")
	public WebElement StateDD;
	@FindBy(id="CityID")
	public WebElement CityDD;
	@FindBy(name="NewZipCode")
	public WebElement NewZip;
	@FindBy(id="LTIdentityTypeCodeID")
	WebElement PhotoIDType;
	@FindBy(id="IdentityNumber")
	WebElement PhotoIDNumber;
	@FindBy(id="PhotoIssuedStateID")
	WebElement IDissuedState;
	@FindBy(id="LTSourceOfIncomeID")
	WebElement EmpTypeDD;
	@FindBy(id="EmployerName")
	WebElement EmpName;
	@FindBy(id="EmployerPhoneNumber")
	WebElement EmpPhone;
	@FindBy(id="EmployerAddressLine1")
	WebElement EmpAdd1;
	@FindBy(id="EmployerAddressLine2")
	WebElement EmpAdd2;
	@FindBy(id="IncomeStateID")
	WebElement EmpStateID;
	@FindBy(id="IncomeCityID")
	WebElement EmpCity;
	@FindBy(id="IncomeNewZipCode")
	WebElement NewEmpZip;
	@FindBy(id="LTPayFrequencyID")
	public WebElement PayFrequency;
	@FindBy(id="DayOfWeek")
	public WebElement Week_Bi_DayOfWeek;
	@FindBy(id="PayDate")
	public WebElement PayDay;
	@FindBy(id="SubsequentPayDate")
	public WebElement NextPayDate;
	@FindBy(id="SubsequentPayDateChecked_false")
	WebElement Bi_NextPayDay1;
	@FindBy(id="SubsequentPayDateChecked_true")
	WebElement Bi_NextPayDay2;
	@FindBy(id="SemiMonthlyChoice_true")
	WebElement SemiM_SpecDates;
	@FindBy(id="SemiMonthlyChoice_false")
	WebElement SemiM_SpecDays;
	@FindBy(id="SemiDateOfMonth1")
	WebElement SemiM_DateofMonth1;
	@FindBy(id="SemiDateOfMonth2")
	WebElement SemiM_DateofMonth2;
	@FindBy(id="SemiWeekOfMonth1")
	WebElement SemiM_WeekofMonth1;
	@FindBy(id="SemiDayOfWeek1")
	WebElement SemiM_DayofWeek1;
	@FindBy(id="SemiWeekOfMonth2")
	WebElement SemiM_WeekofMonth2;
	@FindBy(id="SemiDayOfWeek2")
	WebElement SemiM_DayofWeek2;
	@FindBy(id="MonthlyChoice_false")
	WebElement Monthly_SpecDate;
	@FindBy(id="MonthlyChoice_true")
	WebElement Monthly_SpecDay;
	@FindBy(id="DateOfMonth")
	WebElement Monthly_DateOfMonth;
	@FindBy(id="WeekOfMonth")
	WebElement Weekly_WeekOfMonth;
	@FindBy(id="MonthDayOfWeek")
	WebElement Weekly_MonthDayOfWeek;
	@FindBy(id="GrossIncomeAmount")
	WebElement GrossIncome;
	@FindBy(id="NetIncomeAmount")
	WebElement NetIncome;
	@FindBy(id="HireDate")
	WebElement HireDate;
	@FindBy(id="AccountTypeID")
	WebElement AccountType;
	@FindBy(id="BankName")
	WebElement BankName;
	@FindBy(id="ABANumber")
	WebElement RoutingNumber;
	@FindBy(id="ConfirmABANumber")
	WebElement ConfirmRouting;
	@FindBy(id="AccountNumber")
	WebElement AccountNumber;
	@FindBy(id="ConfirmAccountNumber")
	WebElement ConfirmAccountNum;
	@FindBy(id="IsDirectDeposit")
	WebElement DirectDepositCheck;
	@FindBy(id="AllowDecisionLogic")
	WebElement AllowDecisionLogic;
	@FindBy(id="CCLastFourDigits")
	WebElement CardNumber;
	@FindBy(id="ConfirmCCLastFourDigits")
	WebElement ConfirmCardNum;
	@FindBy(id="debitCardCVV")
	WebElement CardCVV;
	@FindBy(id="NameOnTheDebitCard")
	WebElement NameOnCard;
	@FindBy(id="debitCardExpiryDt")
	WebElement CardExpiryDate;
	@FindBy(id="BillingAddress1")
	WebElement CardBillAddress1;
	@FindBy(id="BillingAddress2")
	WebElement CardBillAddress2;
	@FindBy(id="BillingState")
	WebElement BillState;
	@FindBy(id="BillingCity")
	WebElement BillCity;
	@FindBy(name="ZipCode")
	WebElement cardZip;
	@FindBy(id="IsSameAsMailingAddress")
	WebElement cardAddSameAsMailAdd;
	@FindBy(id="PersonalDetails")
	WebElement NxtBTN;
	@FindBy(linkText="Continue")
	WebElement continueBtn;
	@FindBy(partialLinkText="No, I am willing to wait")
	WebElement EBVPopUpNo1;
	@FindBy(partialLinkText="No, I don't need cash immediately")
	WebElement EBVPopUpNo2;
	
	@FindBy(className="ebvInstantButton")
	WebElement approvalAmtBtn;
	
	
	@FindBy(xpath="//*[@id='globalPopup']/div[2]/div/div/a")
	public WebElement ProceedNoCardBtn;
	@FindBy(xpath="//*[@id='globalPopup']/div[2]/div/div[2]/a")
	WebElement EnterCardDtlsBtn;
	@FindBy(id="siteSearch")
	WebElement YodleeSearch;
	@FindBy(xpath="//*[@id='searchSites']/div[1]/div")
	WebElement YodleeSearchRes;
	@FindBy(className="EBVPopUp")
	WebElement YodleePopup;
	@FindBy(className="ebvInstantButton")
	public WebElement ShowAppAmountBtn;
	@FindBy(name="PASSWORD")
	WebElement YodleePWD;
	@FindBy(name="Re-enter_PASSWORD")
	WebElement YodleeConfPWD;
	@FindBy(id="next")
	WebElement YodleeNextBtn;
	@FindBy(id="chatboxContainer")
	WebElement ChatBox;
	@FindBy(id="chatBox")
	WebElement initialChatBox;
	@FindBy(xpath="//*[@id='chatboxContainer']/label")
	WebElement closedChatBox;
	@FindBy(xpath="//*[@id='globalPopup']/div[2]/ol/div[3]")
	WebElement AddCardAmntLabel;
	@FindBy(xpath="//*[@id='globalPopup']/div[2]/ol/div[4]")
	WebElement NoCardAmntLabel;
	@FindBy(xpath="//*[@id='globalPopup']/div[2]/ol/div[5]/a/div/div[2]")
	WebElement NoCardBTNAmntLabel;
	@FindBy(xpath="//*[@id='globalPopup']/div[2]/ol/div[5]/a/div")
	WebElement AddCardBTN;
	@FindBy(xpath="//*[@id='globalPopup']/div[2]/div/div/a")
	WebElement ProceedNoCardLink;
	
	public void enterEmail()
	{
		String data_Email=pageCon.onlineEmail();
		Email.sendKeys(data_Email);
		ConfirmEmail.click();
	}
	
	public void enterConfirmEmail()
	{
		Sleeper.sleepTightInSeconds(1);
		String ConfEmail=Email.getAttribute("value");
		ConfirmEmail.sendKeys(ConfEmail);
	}

	public void enterInvalidEmail(WebDriverWait wait, WebDriver driver) throws InterruptedException, AWTException
	{
		Online_Login_Page FinLogin=PageFactory.initElements(driver, Online_Login_Page.class);
		
		String data_Email=pageCon.onlineEmail();
		Email.sendKeys(data_Email);
		ConfirmEmail.click();
		String ConfEmail;
		Sleeper.sleepTightInSeconds(1);
		if(driver.findElements(By.xpath("//*[@id='globalPopup']/div[2]/div/div[1]/a")).size()>0)
		{
			String EMailPopupText=PopUp.getText();
			if(EMailPopupText.contains("Welcome Back"))
			{
				Pop_EmailLogin.click();
				FinLogin.ToRegister(wait);
				Log.info("Email already Registered");
				String data_Email2=pageCon.onlineEmail();
				Email.sendKeys(data_Email2);
				Sleeper.sleepTightInSeconds(1);
				ConfEmail=Email.getAttribute("value");
				ConfirmEmail.sendKeys(ConfEmail);
			}
		}
		else
		{
			ConfEmail=Email.getAttribute("value");
			ConfirmEmail.sendKeys(ConfEmail);
		}
	}

	public void enterPhNumber(WebDriverWait wait, WebDriver driver) throws InterruptedException
	{
		String Ph=pageCon.onlinePhNumber();
		PhNumber.click();
		PhNumber.sendKeys(Ph);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Pword.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("globalPopup")));
		if(driver.findElements(By.id("globalPopup")).size()!=0)
		{
			String PopupText=PopUp.getText();
			if(PopupText.contains("verify this phone"))
			{
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
				Pop_VerifyNo.click();
			}
		}
	}
	
	public void enterInvalidPhNumber(WebDriverWait wait, WebDriver driver) throws InterruptedException
	{
		PhNumber.click();
		
		String Ph="1111111111";
		String NewPh=pageCon.onlinePhNumber();
		PhNumber.sendKeys(Ph);
		Pword.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("globalPopup")));
		if(driver.findElements(By.id("globalPopup")).size()!=0)
		{
			String PopupText=PopUp.getText();
			if(PopupText.contains("verify this phone"))
			{
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
				Pop_VerifyNo.click();
			}
			else
			{
				if(PopupText.contains("already exists"))
				{
					Pop_PhExistOK.click();
					PhNumber.sendKeys(Keys.SHIFT,Keys.TAB);
					Sleeper.sleepTightInSeconds(1);
					String CCEmail=ConfirmEmail.getAttribute("value");
					ConfirmEmail.clear();
					Sleeper.sleepTightInSeconds(1);
					ConfirmEmail.sendKeys(CCEmail);
					Sleeper.sleepTightInSeconds(1);
					PhNumber.click();
					PhNumber.sendKeys(NewPh);
					Pword.click();
				}
			}
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
			Pop_VerifyNo.click();
		}
		else
		{
			Log.info("Popup not displayed");
		}
	}

	public void enterGenDetails(WebDriverWait wait) 
	{
		Pword.sendKeys("password");
		Sleeper.sleepTightInSeconds(1);
		ConfirmPword.click();
		Sleeper.sleepTightInSeconds(1);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		ConfirmPword.sendKeys("password");
		Sleeper.sleepTightInSeconds(1);
		DateOfBirth.click();
		DateOfBirth.sendKeys("12121975");
		FName.click();
		Sleeper.sleepTightInSeconds(1);
		String CxEmail=Email.getAttribute("value");
		String[] NamePart=CxEmail.split("_");
		String Fname=NamePart[0];
		String Lname=pageCon.PartName();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(3);
		FName.sendKeys(Fname);
		Sleeper.sleepTightInSeconds(1);
		LName.sendKeys(Lname);
		Sleeper.sleepTightInSeconds(1);
	}
	
	public void enterGenDetailsForEmails(WebDriverWait wait) 
	{
		Pword.sendKeys("password");
		Sleeper.sleepTightInSeconds(1);
		ConfirmPword.click();
		Sleeper.sleepTightInSeconds(1);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		ConfirmPword.sendKeys("password");
		Sleeper.sleepTightInSeconds(1);
		DateOfBirth.click();
		DateOfBirth.sendKeys("12121975");
		FName.click();
		Sleeper.sleepTightInSeconds(1);
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(3);
		FName.sendKeys("Flex");
		Sleeper.sleepTightInSeconds(1);
		LName.sendKeys("seven");
		Sleeper.sleepTightInSeconds(1);
	}
	
	public void GenDetailsForEmails() 
	{
		Pword.sendKeys("password");
		ConfirmPword.click();
		Sleeper.sleepTightInSeconds(3);
		ConfirmPword.sendKeys("password");
		Sleeper.sleepTightInSeconds(1);
		DateOfBirth.click();
		DateOfBirth.sendKeys("12121975");
		DateOfBirth.sendKeys(Keys.TAB);
		Sleeper.sleepTightInSeconds(5);

		String Fname=pageCon.PartName();
		String Lname=pageCon.PartName();
		
		FName.click();
		FName.sendKeys(Fname);
		Sleeper.sleepTightInSeconds(1);
		LName.sendKeys(Lname);
		Sleeper.sleepTightInSeconds(1);
	}

	public void enterSSN() throws InterruptedException
	{
		SSNumber.click();
		SSNumber.sendKeys(pageCon.onlineSSN());
		SecAnswer.click();
	}
	
	public void enterInvalidSSN(WebDriverWait wait, WebDriver driver) throws InterruptedException
	{
		Online_Login_Page FinLogin=PageFactory.initElements(driver, Online_Login_Page.class);
		
		SSNumber.click();
		SSNumber.sendKeys(pageCon.onlineSSN());
		SecAnswer.click();
		String ssEmail=Email.getAttribute("value");
		String ssPhone=PhNumber.getAttribute("value");
		
		Sleeper.sleepTightInSeconds(3);
		if(driver.findElements(By.xpath("//*[@id='globalPopup']/div[2]/div/div[1]/a")).size()>0)
		{
			String SSNPopText=PopUp.getText();
			if(SSNPopText.contains("Welcome Back"))
			{
				Pop_SSNLogin.click();
				Log.info("Email already Registered");
				FinLogin.ToRegister(wait);
				Email.sendKeys(ssEmail);
				ConfirmEmail.sendKeys(ssEmail);
				PhNumber.click();
				PhNumber.sendKeys(ssPhone);
				Pword.click();
				Sleeper.sleepTightInSeconds(2);
				if(driver.findElements(By.id("globalPopup")).size()!=0)
				{
					String PopupText=PopUp.getText();
					if(PopupText.contains("verify this phone"))
					{
						Pop_VerifyNo.click();
					}
				}	
				Sleeper.sleepTightInSeconds(1);
				Pword.sendKeys("password");
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
				Sleeper.sleepTightInSeconds(1);
				ConfirmPword.sendKeys("password");
				DateOfBirth.clear();
				DateOfBirth.sendKeys("12121975");
				Sleeper.sleepTightInSeconds(1);
				DateOfBirth.sendKeys(Keys.TAB);
				FName.sendKeys(pageCon.PartName());
				String lname=pageCon.PartName();
				LName.sendKeys(lname);
				SSNumber.sendKeys(Keys.SHIFT,Keys.TAB);
				Sleeper.sleepTightInSeconds(1);
				LName.clear();
				Sleeper.sleepTightInSeconds(1);
				LName.sendKeys(lname);
				Sleeper.sleepTightInSeconds(1);
				SSNumber.click();
				SSNumber.sendKeys(pageCon.onlineSSN());
				SecAnswer.click();
			}
		}	
	}
	
	public void FirstPageState(Row r, WebDriverWait wait)
	{
		String State=r.getCell(0).getStringCellValue();
		int MStatus=(int) r.getCell(2).getNumericCellValue();
		
		new Select (StateOne).selectByVisibleText(State);
		if(State.equals("Wisconsin"))
		{
			if(MStatus<1)
			{
				Sleeper.sleepTightInSeconds(5);
				new Select(MaritalStatus).selectByIndex(2);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
				TermsAndCon.click();
				Sleeper.sleepTightInSeconds(2);
				/*wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
				TermsAndCon.click();*/
			}
			else
			{
				//MaritalStatus.click();
				Sleeper.sleepTightInSeconds(5);
				new Select(MaritalStatus).selectByIndex(1);
				Sleeper.sleepTightInSeconds(1);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
				Sleeper.sleepTightInSeconds(1);
				SpouseFirstName.sendKeys(pageCon.PartName());
				Sleeper.sleepTightInSeconds(1);
				SpouseLastName.sendKeys(pageCon.PartName());
				Sleeper.sleepTightInSeconds(1);
				//SpouseEmail.sendKeys(pageCon.spouseEmail());
				Sleeper.sleepTightInSeconds(2);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
				//TermsAndCon.click();
				Sleeper.sleepTightInSeconds(2);
				/*wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
				TermsAndCon.click();*/
			}
		}
	}
	
	public void enterSecurityDetails()
	{
		new Select(SecQuestion).selectByIndex(1);
		SecAnswer.sendKeys("01254");
	}

	public void checkMilitary(WebDriverWait wait)
	{
			MilitaryNo.click();
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
			Sleeper.sleepTightInSeconds(1);
			MilitaryNo.click();		
	}

	public void checkConsent()
	{
		ConsentCheck.click();
	}
	public void communucationPolicyCheck()
	{
		ComPolicyCheck.click();
	}
	public void electronicSignCheck()
	{
		ElectronicDisclosureCheck.click();
	}
	
	public void clickAccCreateNXTBtn()
	{
		AccCreationNextBTN.click();
	}
	
	public void LNPopUp()
	{
		PopupOKBTN.click();
	}
	
	public void hadleChatBox(WebDriver driver)
	{
		boolean chatboxDisplay=driver.findElements(By.id("chatboxContainer")).size()>0;
		if(chatboxDisplay)
		{
			initialChatBox.click();
		}
			Sleeper.sleepTightInSeconds(2);
		try
		{	
			initialChatBox.click();
		}
		catch(Exception e)
		{
			
		}
	}	
	
	public String Zip(Row r)
	{
		String CxState=r.getCell(0).getStringCellValue();
		String Zip="37777";
		if(CxState.equals("Missouri"))
		{
			Zip="38880";
		}
		if(CxState.equals("Alabama"))
		{
			Zip="36660";
		}
		if(CxState.equals("California"))
		{
			Zip="90001";
		}
		if(CxState.equals("Utah"))
		{
			Zip="84404";
		}
		if(CxState.equals("Idaho"))
		{
			Zip="83333";
		}
		if(CxState.equals("Kansas"))
		{
			Zip="66606";
		}
		if(CxState.equals("Virginia"))
		{
			Zip="22622";
		}
		if(CxState.equals("North Dakota"))
		{
			Zip="58008";
		}
		if(CxState.equals("Delaware"))
		{
			Zip="19966";
		}
		if(CxState.equals("Wisconsin"))
		{
			Zip="72200";
		}
		
		return Zip;
	}
	
	public void NewAddressDetails(Row r, WebDriverWait wait) throws InterruptedException
	{
		Address1.sendKeys("1106 St John Circle");
		//SuiteNumber.sendKeys("Suite No 504");
		SuiteNumber.sendKeys("1-877-565");
		String CxState=r.getCell(0).getStringCellValue();
		new Select(StateDD).selectByVisibleText(CxState);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		SuiteNumber.click();
		
		By byCity = By.cssSelector("#CityID option");
		wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(byCity,1));
		Sleeper.sleepTightInSeconds(2);
		new Select(CityDD).selectByIndex(11);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		SuiteNumber.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(2);
		new Select(CityDD).selectByIndex(11);
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		SuiteNumber.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		
		String ZipC=Zip(r);
		NewZip.sendKeys(ZipC);
		SuiteNumber.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
	}
	
	public void IdentityDetails(WebDriverWait wait, WebDriver driver) throws InterruptedException
	{	
		new Select(PhotoIDType).selectByIndex(2);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		PhotoIDNumber.sendKeys(pageCon.onlinePhotoID());
		Sleeper.sleepTightInSeconds(1);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		SuiteNumber.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		new Select(IDissuedState).selectByIndex(12);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		JavascriptExecutor jse=(JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,500)", "");
		EmpPhone.click();
	}
	
	public void enterNewEmpDetails(Row r,WebDriverWait wait)		
	{	
		EmpName.clear();
		EmpName.sendKeys("ABC Systems");
		EmpAdd1.clear();
		EmpAdd1.sendKeys("242 Amanda St");
		EmpPhone.click();
		EmpPhone.sendKeys(pageCon.EmpPhNum());
		new Select(EmpStateID).selectByVisibleText(r.getCell(0).getStringCellValue());
		EmpName.click();
		By byEmpCity = By.cssSelector("#IncomeCityID option");
		wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(byEmpCity,1));
		new Select(EmpCity).selectByIndex(11);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		EmpName.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		new Select(EmpCity).selectByIndex(11);
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		EmpName.click();
		String ZipC=Zip(r);
		NewEmpZip.sendKeys(ZipC);
		GrossIncome.click();
	}
	
	public void enterEmpPhone()
	{
		EmpPhone.click();
		EmpPhone.sendKeys(pageCon.EmpPhNum());
	}
	
	public void enterIncomeDetails(Row r, WebDriverWait wait, WebDriver driver) throws InterruptedException
	{
		int empType=(int)r.getCell(1).getNumericCellValue();
		
		new Select(EmpTypeDD).selectByIndex(empType);
		
		if(empType<3)
		{
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
			Sleeper.sleepTightInSeconds(1);
			enterNewEmpDetails(r, wait);
		}
		else
		{
			enterEmpPhone();
		}
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(2);
		selectFrequency(r, wait);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		enterIncome(r, wait);
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		
		JavascriptExecutor jse=(JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,100)", "");
		
		enterHireDate(wait);
		Sleeper.sleepTightInSeconds(2);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		BankName.click();
	}
	
	
	public void selectFrequency(Row r, WebDriverWait wait)
	{
		int payFreq=(int)r.getCell(2).getNumericCellValue();
		String PayFreq=String.valueOf(payFreq);
		
		String NextPayDay=r.getCell(3).getStringCellValue();
		
		int WBSMandMday1=(int) r.getCell(5).getNumericCellValue();
		int WBSMandMday2=(int) r.getCell(7).getNumericCellValue();
		int SMandMweek1=(int) r.getCell(6).getNumericCellValue();
		int SMandMweek2=(int) r.getCell(8).getNumericCellValue();
		int SMandMDate1=(int) r.getCell(9).getNumericCellValue();
		int SMandMDate2=(int) r.getCell(10).getNumericCellValue();
		
		Sleeper.sleepTightInSeconds(2);
		if(PayFreq.equals("1"))
		{
			new Select(PayFrequency).selectByIndex(1);
			Sleeper.sleepTightInSeconds(1);
			new Select(Week_Bi_DayOfWeek).selectByIndex(WBSMandMday1);
		}
		else
		{
			if(PayFreq.equals("2"))
			{
				new Select(PayFrequency).selectByIndex(2);
				Sleeper.sleepTightInSeconds(1);
				new Select(Week_Bi_DayOfWeek).selectByIndex(WBSMandMday1);
				Sleeper.sleepTightInSeconds(1);
				if(NextPayDay.equals("0"))
				{
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
					Bi_NextPayDay1.click();
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
					Bi_NextPayDay1.click();
				}
				else
				{
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
					Bi_NextPayDay2.click();
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
					Bi_NextPayDay2.click();
				}
			}
			else
			{
				String DayOrDate=r.getCell(4).getStringCellValue();
				if(PayFreq.equals("3"))
				{
					new Select(PayFrequency).selectByIndex(3);
					Sleeper.sleepTightInSeconds(1);
					if(DayOrDate.equals("Day"))
					{
						wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
						SemiM_SpecDays.click();
						wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
						SemiM_SpecDays.click();
						Sleeper.sleepTightInSeconds(1);
						new Select(SemiM_WeekofMonth1).selectByIndex(SMandMweek1);
						new Select(SemiM_DayofWeek1).selectByIndex(WBSMandMday1);
						new Select(SemiM_WeekofMonth2).selectByIndex(SMandMweek2);
						new Select(SemiM_DayofWeek2).selectByIndex(WBSMandMday2);
					}
					else
					{
						wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
						SemiM_SpecDates.click();
						wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
						SemiM_SpecDates.click();
						Sleeper.sleepTightInSeconds(1);
						new Select(SemiM_DateofMonth1).selectByIndex(SMandMDate1);
						new Select(SemiM_DateofMonth2).selectByIndex(SMandMDate2);
					}
				}
				else
				{
					if(PayFreq.equals("4"))
					{
						new Select(PayFrequency).selectByIndex(4);
						Sleeper.sleepTightInSeconds(1);
						if(DayOrDate.equals("Day"))
						{
							wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
							Monthly_SpecDay.click();
							wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
							Monthly_SpecDay.click();
							Sleeper.sleepTightInSeconds(1);
							new Select(Weekly_WeekOfMonth).selectByIndex(SMandMweek1);
							new Select(Weekly_MonthDayOfWeek).selectByIndex(WBSMandMday1);						
						}
						else
						{
							wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
							Monthly_SpecDate.click();
							wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
							Monthly_SpecDate.click();
							Sleeper.sleepTightInSeconds(1);
							new Select(Monthly_DateOfMonth).selectByIndex(SMandMDate1);
						}
					}
				}
			}
		}
	}
	
	public void enterIncome(Row r, WebDriverWait wait)
	{
		String Income=r.getCell(11).getStringCellValue();
		GrossIncome.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(1);
		GrossIncome.sendKeys(Income);
		NetIncome.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(1);
		NetIncome.sendKeys(Income);
	}
	
	public void enterHireDate(WebDriverWait wait)
	{
		HireDate.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		HireDate.sendKeys("12122012");
		HireDate.sendKeys(Keys.TAB);
	}
	
	public void selectCheckingACType()
	{
		new Select(AccountType).selectByIndex(1);
	}
	
	public void selectSavingACType()
	{
		new Select(AccountType).selectByIndex(3);
		
		try
		{
			PopupOKBTN.click();
		}
		catch(Exception e){}
		Sleeper.sleepTightInSeconds(2);
	}

	public void selectPrepaidACType()
	{
		new Select(AccountType).selectByIndex(2);
		
		try
		{
			PopupOKBTN.click();
		}
		catch(Exception e){}
		Sleeper.sleepTightInSeconds(2);
	}

	public void BankDetails(Row r, WebDriverWait wait) throws InterruptedException	
	{
		BankName.sendKeys("Bank of America");
		Sleeper.sleepTightInSeconds(1);
		try
		{
			RoutingNumber.click();
			RoutingNumber.sendKeys("999988181");
		}
		catch(Exception e)
		{
			initialChatBox.click();
			Sleeper.sleepTightInSeconds(2);
			RoutingNumber.click();
			RoutingNumber.sendKeys("999988181");
		}
		
		Sleeper.sleepTightInSeconds(5);
		ConfirmRouting.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(5);
		ConfirmRouting.sendKeys("999988181");
		Sleeper.sleepTightInSeconds(1);
		String AccNbr=pageCon.AccountNum();

		//initialChatBox.click();
		
		AccountNumber.sendKeys(AccNbr);
		ConfirmAccountNum.click();
		Sleeper.sleepTightInSeconds(2);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(1);
		ConfirmAccountNum.sendKeys(AccNbr);

		String isDirectDeposit=r.getCell(12).getStringCellValue();
		if (isDirectDeposit.equals("Yes"))
		{
			new Select(DirectDepositCheck).selectByIndex(1);
		}
		else
		{
			new Select(DirectDepositCheck).selectByIndex(2);
		}
		
		Sleeper.sleepTightInSeconds(1);
	}
	
	//Yodlee Check - Index 1 for Instant Electronic Account verification, Index 2 for Manual Upload
	public void VerifyAccountYodlee()
	{
		new Select(AllowDecisionLogic).selectByIndex(0);			
		Log.info("Yodlee perform");
	}
	
	public void VerifyAccountUploadDoc()
	{
		new Select(AllowDecisionLogic).selectByIndex(1);
		Log.info("Manual Upload");
	}

	public void CardDetails(WebDriverWait wait, WebDriver driver, Row r) throws InterruptedException
	{
		//String cardNum=pageCon.cardNumber();
		CardNumber.click();
		Sleeper.sleepTightInSeconds(6);
		//CardNumber.sendKeys(cardNum);
		CardNumber.sendKeys("5105105105105100");
		ConfirmCardNum.sendKeys("5105105105105100");
		CardCVV.sendKeys("123");
		NameOnCard.sendKeys("Test");
		Sleeper.sleepTightInSeconds(8);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		CardExpiryDate.click();
		Sleeper.sleepTightInSeconds(8);
		CardExpiryDate.sendKeys("1220");
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(1);
		CardExpiryDate.sendKeys(Keys.TAB);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(3);
		try
		{
			cardAddSameAsMailAdd.click();
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
			Sleeper.sleepTightInSeconds(1);	
			
		}
		catch(Exception e) {
			CardBillAddress1.sendKeys("Suite Bill One");
			CardBillAddress2.sendKeys("Suite Bill Two");
			
			String BillState1=r.getCell(0).getStringCellValue();
			Log.info("BillState: "+BillState1);
			new Select(BillState).selectByVisibleText(BillState1);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
			CardBillAddress1.click();
			Sleeper.sleepTightInSeconds(2);
			
			By byBillCity = By.cssSelector("#BillingCity option");
			wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(byBillCity,1));
			Sleeper.sleepTightInSeconds(2);
			new Select(BillCity).selectByIndex(11);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
			Sleeper.sleepTightInSeconds(1);	
		}
		
		cardZip.sendKeys("35252");
	}
	
	public void cardContinueBtn()
	{
		continueBtn.click();
	}
	
	public void PersonalPageNextBtn()
	{
		NxtBTN.click();
	}
	
	public void DocsUploadPopUp()
	{
		EBVPopUpNo1.click();
		Sleeper.sleepTightInSeconds(3);
		EBVPopUpNo2.click();
		Sleeper.sleepTightInSeconds(5);
	}
	
	public void DocsPageYodleeYes(WebDriverWait wait)
	{
		EBVPopUpNo1.click();
		Sleeper.sleepTightInSeconds(5);		
		EBVPopUpNo2.click();
	}	
}