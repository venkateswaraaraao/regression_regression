package com.afqa.page;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Store_CxTransaction_Page {

	@FindBy(className="lOCPaymentMonetaryCodeClass")
	WebElement payBy;
	
	@FindBy(id="otherradio")
	WebElement payOtherRButton;
	
	@FindBy(id="Other")
	WebElement OtherAmount;

	@FindBy(className="lOCPaymentMonetaryCodeClass")
	WebElement selectPayType;
	
	@FindBy(className="tenderAmt")
	WebElement TenderAmount;
	
	@FindBy(id="customerPaymentButton")
	WebElement processPaymentBtn;
	
	@FindBy(className="popupDialogHdr")
	WebElement PopUpBox;

	@FindBy(className="globalPasswordBox")
	WebElement PWDPopUp;
	
	@FindBy(linkText="Ok")
	WebElement OkBtn;

	@FindBy(linkText="Verify")
	WebElement VerifyBTN;
	
	@FindBy(linkText="Email Receipt")
	WebElement emailReceiptBtn;
	
	@FindBy(linkText="No Receipt")
	WebElement NoReceiptBtn;
	
	@FindBy(linkText="OK")
	WebElement okBtn;
	
	@FindBy(xpath="//*[@id=\'divActivity\']/table/tbody/tr[2]/td[4]")
	WebElement Paymentid;
	
	@FindBy(id="SafePassword")
	WebElement safePassword;
	
	@FindBy(id="TellerPassword")
	WebElement TellerPassword;
	
	@FindBy(id="clsVerifyButton")
	WebElement verify_Btn;
	
	@FindBy(xpath="//*[@id='globalPopup']/div[2]/div/a")
	WebElement PopUPOKBtn; 
	
	@FindBy(xpath="//*[@id='globalPopup']/div[2]/ol/li")
	WebElement VoidText;

	

	public void payOtherAmount(WebDriverWait wait)
	{
		payOtherRButton.click();
		Sleeper.sleepTightInSeconds(1);
		OtherAmount.sendKeys("10");

		new Select(selectPayType).selectByIndex(1);
		
		Sleeper.sleepTightInSeconds(2);
		
		TenderAmount.sendKeys("10");
		
		Sleeper.sleepTightInSeconds(1);
		processPaymentBtn.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(1);
		processPaymentBtn.click();

		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
		Sleeper.sleepTightInSeconds(2);
		OkBtn.click();
		Sleeper.sleepTightInSeconds(2);
		PWDPopUp.sendKeys("FiNext@123");
		VerifyBTN.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("No Receipt")));
		Sleeper.sleepTightInSeconds(2);
		NoReceiptBtn.click();
		Sleeper.sleepTightInSeconds(2);
		//okBtn.click();
		
		
	}
	public void PaymentVoid(WebDriver driver,String LOCTrasactionid,WebDriverWait wait) throws Exception
	{                                                   
		WebElement voidBtn=driver.findElement(By.xpath("//*[@id=\'divActivity\']/table/tbody/tr[2]/td[12]/a[1]/img"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", voidBtn);
		
		String xpath1="//*[@id='divActivity']/table/tbody/tr[";
		String xpath2="]/td[4]";
		String xpath3="]/td[12]/a[1]/img";
		String paymentID=LOCTrasactionid;
		
		for(int m=2;m<=10;m++)
		{
			String getTranDetails=driver.findElement(By.xpath(xpath1+m+xpath2)).getText();
			
			if(getTranDetails.contains(paymentID))
			{
				driver.findElement(By.xpath(xpath1+m+xpath3)).click();
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupDialogMask")));
				Sleeper.sleepTightInSeconds(1);
				safePassword.click();
				Sleeper.sleepTightInSeconds(1);
				safePassword.sendKeys("password");
				Sleeper.sleepTightInSeconds(1);
				TellerPassword.click();
				Sleeper.sleepTightInSeconds(1);
				TellerPassword.sendKeys("FiNext@123");
				Sleeper.sleepTightInSeconds(2);      
				verify_Btn.click();
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
				Sleeper.sleepTightInSeconds(2);
				okBtn.click();
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
				Sleeper.sleepTightInSeconds(4);
				okBtn.click();
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("popupProgressDialogMask")));
				Sleeper.sleepTightInSeconds(3);
				break;
			   
			}
			
		}
	}
	
}
